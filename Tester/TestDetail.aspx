﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="TestDetail.aspx.cs" Inherits="Tester.TestDetail" ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="contTitle" ContentPlaceHolderID="contTitle" runat="server">
    <asp:Literal ID="litTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="contMenu" ContentPlaceHolderID="contMenu" runat="server">
</asp:Content>
<asp:Content ID="contMain" ContentPlaceHolderID="contMain" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:Label ID="lblTheme" runat="server" Text="Тема" ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="lblTestItemCount" runat="server" Text="Количество заданий" ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <br />
    <asp:Repeater ID="rptTestList" runat="server">
        <HeaderTemplate>
            <table border="1">
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:Label ID="lblTestItem" runat="server"
                        Text='<%# "Задание "+DataBinder.Eval(Container.DataItem, "OrderNumber")  %>'
                        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    <br />
                    <asp:Label ID="lblTestItemName" runat="server"
                        Text='<%# "ID Задения "+DataBinder.Eval(Container.DataItem, "Name")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    <asp:Label ID="lblDifficuty" runat="server"
                        Text='<%# "Сложность "+DataBinder.Eval(Container.DataItem, "Difficulty")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    <asp:Label ID="lblYear" runat="server"
                        Text='<%# "Год "+DataBinder.Eval(Container.DataItem, "Year")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    <div style="width: 1000px; background-color: white; border: 5px double #2E9AFE">
                        <asp:Literal ID="ltrlTestQuestion" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "QuestionHtml")  %>'></asp:Literal>
                    </div>
                    <br />
                    <asp:Label ID="lblAnswerType" runat="server"
                        Text='<%# "Тип ответа " + DataBinder.Eval(Container.DataItem, "AnswerType")%>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    <br />
                    <asp:Label ID="lblAnswer" runat="server"
                        Text='<%# "Ответ " + DataBinder.Eval(Container.DataItem, "Answer")%>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    <br />
                    <asp:Label ID="lblComment1" runat="server"
                        Text='<%# "Комментарий 1 " + DataBinder.Eval(Container.DataItem, "Comment1")%>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    <br />
                    <asp:Label ID="lblComment2" runat="server"
                        Text='<%# "Комментарий 2 "+DataBinder.Eval(Container.DataItem, "Comment1")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    <br />
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <hr />
    <table>
        <tr>
            <td>
                <asp:ImageButton ID="btnFirst" runat="server" OnClick="btnFirst_Click" ImageUrl="~/Buttons/First.png" />
            </td>
            <td>
                <asp:ImageButton ID="btnPrev" runat="server" OnClick="btnPrev_Click" ImageUrl="~/Buttons/Prev.png" />
            </td>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Задание "
                    ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
                <asp:DropDownList ID="ddlTaskNo" runat="server" AutoPostBack="True" 
                    OnSelectedIndexChanged="ddlTaskNo_SelectedIndexChanged" Width="50px"
                    BackColor="#58FAF4" ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px" BorderColor="#58FAF4"></asp:DropDownList>
                <asp:Label ID="lblTestItemCount2" runat="server" Text="" 
                    ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
            </td>
            <td>
                <asp:ImageButton ID="btnNext" runat="server" OnClick="btnNext_Click" ImageUrl="~/Buttons/Next.png" />
            </td>
            <td>
                <asp:ImageButton ID="btnLast" runat="server" OnClick="btnLast_Click" ImageUrl="~/Buttons/Last.png" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="contButtons" ContentPlaceHolderID="contButtons" runat="server">
    <hr />
    <asp:ImageButton ID="btnBack" runat="server" OnClick="btnBack_Click" ImageUrl="~/Buttons/Back.png" />
</asp:Content>
