﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="AssignTask.aspx.cs" Inherits="Tester.AssignTask" %>

<asp:Content ID="contTitle" ContentPlaceHolderID="contTitle" runat="server">
    <asp:Literal ID="litTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="contScript" ContentPlaceHolderID="contScript" runat="server">
    <script type="text/javascript">
        function Confirm(el) {
            Message = $(el).attr("confirmmessage");
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm(Message)) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>
<asp:Content ID="contMenu" ContentPlaceHolderID="contMenu" runat="server">
</asp:Content>
<asp:Content ID="contMain" ContentPlaceHolderID="contMain" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Назначить задание"
        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="lblTaskName" runat="server" Text=""
        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="Label3" runat="server" Text="("
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:CheckBox ID="chkContinuous" runat="server" Text="Непрерывное"
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px" />
    <asp:CheckBox ID="chkShowAnswer" runat="server" Text="Показывать ответ"
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px" />
    <asp:CheckBox ID="chkRepeatable" runat="server" Text="Повторяемое"
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px" />
    <asp:Label ID="Label4" runat="server" Text=")"
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <br />
    <br />
    <asp:Label ID="Label2" runat="server" Text="Пользователям"
        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <br />
    <asp:CheckBoxList ID="lstUsers" runat="server"
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px">
    </asp:CheckBoxList>
</asp:Content>
<asp:Content ID="contButtons" ContentPlaceHolderID="contButtons" runat="server">
    <hr />
    <asp:ImageButton ID="btnOK" runat="server" OnClick="btnOK_Click" ImageUrl="~/Buttons/OK.png" />
    <asp:ImageButton ID="btnCancel" runat="server" OnClick="btnCancel_Click" ImageUrl="~/Buttons/Cancel.png"
        OnClientClick="Confirm(this);" />
</asp:Content>
