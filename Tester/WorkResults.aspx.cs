﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Tester
{
    [Serializable]
    public class WorkShowResult
    {
        public Work WorkToShow
        {
            get;
            set;
        }
        public string TaskName
        {
            get;
            set;
        }
        public string UserName
        {
            get;
            set;
        }
        public int UserCreatorID
        {
            get;
            set;
        }
        public string UserCreatorName
        {
            get;
            set;
        }
        public string UserAssignerName
        {
            get;
            set;
        }
    }

    public partial class WorkResults : System.Web.UI.Page
    {
        WorkShowResult[] WorkShowResultArray;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SessionToViewState();
                FillDDLs();
                ShowFilter();
            }
            if (ViewState["IsAdmin"] != null)
            {
                if ((bool)ViewState["IsAdmin"])
                {
                    btnTests.Visible = true;
                    btnTasks.Visible = true;
                    btnUsers.Visible = true;
                }
                else
                {
                    btnTests.Visible = false;
                    btnTasks.Visible = false;
                    btnUsers.Visible = false;
                }
            }
            
            WorkShowResultArray = GetWorkShowResults();
            rptWorkShowResult.DataSource = WorkShowResultArray;
            OrderWorkShowResults(WorkShowResultArray);
            rptWorkShowResult.DataBind();
            if (ViewState["CurUser"] != null)
            {
                UserDB usr = (UserDB)ViewState["CurUser"];
                if (!usr.IsAdmin)
                {
                    rptWorkShowResult.Controls[0].FindControl("thDelete").Visible = false;
                    foreach (RepeaterItem ri in rptWorkShowResult.Items)
                    {
                        ri.FindControl("tdDelete").Visible = false;
                    }
                }
            }
            SetButtons();
        }
        private void SetButtons()
        {
            btnMyWork.Attributes.Add("onmouseout", "this.src='Buttons/MyWork.png'");
            btnMyWork.Attributes.Add("onmouseover", "this.src='Buttons/MyWorkMouseOver.png'");
            btnTests.Attributes.Add("onmouseout", "this.src='Buttons/Tests.png'");
            btnTests.Attributes.Add("onmouseover", "this.src='Buttons/TestsMouseOver.png'");
            btnTasks.Attributes.Add("onmouseout", "this.src='Buttons/Tasks.png'");
            btnTasks.Attributes.Add("onmouseover", "this.src='Buttons/TasksMouseOver.png'");
            btnUsers.Attributes.Add("onmouseout", "this.src='Buttons/Users.png'");
            btnUsers.Attributes.Add("onmouseover", "this.src='Buttons/UsersMouseOver.png'");
            btnApplyFilter.Attributes.Add("onmouseout", "this.src='Buttons/ApplyFilter.png'");
            btnApplyFilter.Attributes.Add("onmouseover", "this.src='Buttons/ApplyFilterMouseOver.png'");
            bool IsAdmin = false;
            if (ViewState["IsAdmin"] != null)
            {
                if ((bool)ViewState["IsAdmin"])
                {
                    IsAdmin = true;
                }
            }
            for (int i = 0; i < rptWorkShowResult.Items.Count; i++)
            {
                rptWorkShowResult.Items[i].FindControl("btnDelete").Visible = IsAdmin;
            }
        }
        private void FillDDLs()
        {
            ddlUserAssigner.Items.Clear();
            ddlUserDoer.Items.Clear();
            ddlState.Items.Clear();
            ddlUserAssigner.Items.Add(new ListItem("Все", "-1"));
            ddlUserDoer.Items.Add(new ListItem("Все", "-1"));
            ddlState.Items.Add(new ListItem("Все", "-1"));
            UserDB[] allUsers = AccessDB.GetUsers();
            foreach(UserDB usr in allUsers)
            {
                ddlUserAssigner.Items.Add(new ListItem(usr.ToString(), usr.UserID.ToString()));
                ddlUserDoer.Items.Add(new ListItem(usr.ToString(), usr.UserID.ToString()));
            }
            WorkState[] allWorkState = AccessDB.GetWorkStates();
            foreach(WorkState ws in allWorkState)
            {
                ddlState.Items.Add(new ListItem(ws.WorkStateString, ws.WorkStateID.ToString()));
            }
        }
        private int CompareWorkShowResult(WorkShowResult wr1, WorkShowResult wr2)
        {
            if (WorkResTaskNameSort == "ASC")
            {
                return wr1.TaskName.CompareTo(wr2.TaskName);
            }
            if (WorkResTaskNameSort == "DESC")
            {
                return wr2.TaskName.CompareTo(wr1.TaskName);
            }
            if (WorkResUserAssignerNameSort == "ASC")
            {
                return wr1.UserAssignerName.CompareTo(wr2.UserAssignerName);
            }
            if (WorkResUserAssignerNameSort == "DESC")
            {
                return wr2.UserAssignerName.CompareTo(wr1.UserAssignerName);
            }
            if (WorkResUserNameSort == "ASC")
            {
                return wr1.UserName.CompareTo(wr2.UserName);
            }
            if (WorkResUserNameSort == "DESC")
            {
                return wr2.UserName.CompareTo(wr1.UserName);
            }
            if (WorkResAssignDateSort == "ASC")
            {
                return wr1.WorkToShow.AssignDate.CompareTo(wr2.WorkToShow.AssignDate);
            }
            if (WorkResAssignDateSort == "DESC")
            {
                return wr2.WorkToShow.AssignDate.CompareTo(wr1.WorkToShow.AssignDate);
            }
            if (WorkResWorkStateSort == "ASC")
            {
                return wr1.WorkToShow.WorkStateID.CompareTo(wr2.WorkToShow.WorkStateID);
            }
            if (WorkResWorkStateSort == "DESC")
            {
                return wr2.WorkToShow.WorkStateID.CompareTo(wr1.WorkToShow.WorkStateID);
            }
            return 0;
        }
        private void OrderWorkShowResults(WorkShowResult[] WorkShowResults)
        {
            if (WorkResTaskNameSort != "" || WorkResUserAssignerNameSort != "" || WorkResUserNameSort != "" || 
                WorkResAssignDateSort != "" || WorkResWorkStateSort != "")
            {
                Array.Sort(WorkShowResults, CompareWorkShowResult);
            }
        }
        private string NextSortDir(string SortDir)
        {
            switch (SortDir)
            {
                case "":
                    return "ASC";
                case "ASC":
                    return "DESC";
                case "DESC":
                    return "";
                default:
                    return "";
            }
        }
        private DateTime FromDate
        {
            get
            {
                if (ViewState["WorkResultsFromDate"] != null)
                {
                    return (DateTime)ViewState["WorkResultsFromDate"];
                }
                else
                {
                    return DateTime.Now.AddMonths(-3);
                }
            }
            set
            {
                ViewState["WorkResultsFromDate"] = value;
            }
        }
        private DateTime ToDate
        {
            get
            {
                if (ViewState["WorkResultsToDate"] != null)
                {
                    return (DateTime)ViewState["WorkResultsToDate"];
                }
                else
                {
                    return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                }
            }
            set
            {
                ViewState["WorkResultsToDate"] = value;
            }
        }
        private int UserDoerID
        {
            get
            {
                if (ViewState["WorkResultsUserDoerID"] != null)
                {
                    return (int)ViewState["WorkResultsUserDoerID"];
                }
                else
                {
                    if (ViewState["IsAdmin"] != null)
                    {
                        if ((bool)ViewState["IsAdmin"])
                        {
                            return -1;
                        }
                    }
                    if (ViewState["CurUser"] != null)
                    {
                        return ((UserDB)ViewState["CurUser"]).UserID;
                    }
                    return -1;
                }
            }
            set
            {
                ViewState["WorkResultsUserDoerID"] = value;
            }
        }
        private int UserAssignerID
        {
            get
            {
                if (ViewState["WorkResultsUserAssignerID"] != null)
                {
                    return (int)ViewState["WorkResultsUserAssignerID"];
                }
                else
                {
                    if (ViewState["IsAdmin"] != null)
                    {
                        if ((bool)ViewState["IsAdmin"])
                        {
                            if (ViewState["CurUser"] != null)
                            {
                                return ((UserDB)ViewState["CurUser"]).UserID;
                            }
                        }
                    }
                    return -1;
                }
            }
            set
            {
                ViewState["WorkResultsUserAssignerID"] = value;
            }
        }
        private int WorkStateID
        {
            get
            {
                if (ViewState["WorkResultsWorkStateID"] != null)
                {
                    return (int)ViewState["WorkResultsWorkStateID"];
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                ViewState["WorkResultsWorkStateID"] = value;
            }
        }
        private string WorkResTaskNameSort
        {
            get
            {
                if (ViewState["WorkResTaskNameSort"] != null)
                {
                    return (string)ViewState["WorkResTaskNameSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["WorkResTaskNameSort"] = value;
            }
        }
        private string WorkResUserAssignerNameSort
        {
            get
            {
                if (ViewState["WorkResUserAssignerNameSort"] != null)
                {
                    return (string)ViewState["WorkResUserAssignerNameSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["WorkResUserAssignerNameSort"] = value;
            }
        }
        private string WorkResUserNameSort
        {
            get
            {
                if (ViewState["WorkResUserNameSort"] != null)
                {
                    return (string)ViewState["WorkResUserNameSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["WorkResUserNameSort"] = value;
            }
        }
        private string WorkResAssignDateSort
        {
            get
            {
                if (ViewState["WorkResAssignDateSort"] != null)
                {
                    return (string)ViewState["WorkResAssignDateSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["WorkResAssignDateSort"] = value;
            }
        }
        private string WorkResWorkStateSort
        {
            get
            {
                if (ViewState["WorkResWorkStateSort"] != null)
                {
                    return (string)ViewState["WorkResWorkStateSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["WorkResWorkStateSort"] = value;
            }
        }
        private void ShowFilter()
        {
            ddlUserAssigner.SelectedValue = UserAssignerID.ToString();
            ddlUserDoer.SelectedValue = UserDoerID.ToString();
            ddlState.SelectedValue = WorkStateID.ToString();
            if (ViewState["IsAdmin"] != null)
            {
                if (!(bool)ViewState["IsAdmin"])
                {
                    ddlUserAssigner.Enabled = false;
                    ddlUserDoer.Enabled = false;
                }
            }
            txtFromDate.Text = FromDate.ToString("dd.MM.yyyy");
            txtToDate.Text = ToDate.ToString("dd.MM.yyyy");
        }
        protected void btnTaskNameSort_Click(object sender, EventArgs e)
        {
            if (WorkResUserAssignerNameSort != "" || WorkResUserNameSort != "" || 
                WorkResAssignDateSort != "" || WorkResWorkStateSort!="")
            {
                WorkResTaskNameSort = "ASC";
                WorkResUserAssignerNameSort = "";
                WorkResUserNameSort = "";
                WorkResAssignDateSort = "";
                WorkResWorkStateSort = "";
            }
            else
            {
                WorkResTaskNameSort = NextSortDir(WorkResTaskNameSort);
            }
            ViewStateToSession();
            Response.Redirect("~/WorkResults.aspx");
        }
        protected void btnUserAssignerSort_Click(object sender, EventArgs e)
        {
            if (WorkResTaskNameSort != "" || WorkResUserNameSort != "" ||
                WorkResAssignDateSort != "" || WorkResWorkStateSort != "")
            {
                WorkResTaskNameSort = "";
                WorkResUserAssignerNameSort = "ASC";
                WorkResUserNameSort = "";
                WorkResAssignDateSort = "";
                WorkResWorkStateSort = "";
            }
            else
            {
                WorkResUserAssignerNameSort = NextSortDir(WorkResUserAssignerNameSort);
            }
            ViewStateToSession();
            Response.Redirect("~/WorkResults.aspx");
        }
        protected void btnUserSort_Click(object sender, EventArgs e)
        {
            if (WorkResTaskNameSort != "" || WorkResUserAssignerNameSort != "" ||
                WorkResAssignDateSort != "" || WorkResWorkStateSort != "")
            {
                WorkResTaskNameSort = "";
                WorkResUserAssignerNameSort = "";
                WorkResUserNameSort = "ASC";
                WorkResAssignDateSort = "";
                WorkResWorkStateSort = "";
            }
            else
            {
                WorkResUserNameSort = NextSortDir(WorkResUserNameSort);
            }
            ViewStateToSession();
            Response.Redirect("~/WorkResults.aspx");
        }
        protected void btnAssignDateSort_Click(object sender, EventArgs e)
        {
            if (WorkResTaskNameSort != "" || WorkResUserAssignerNameSort != "" || WorkResUserNameSort != "" ||
                WorkResWorkStateSort != "")
            {
                WorkResTaskNameSort = "";
                WorkResUserAssignerNameSort = "";
                WorkResUserNameSort = "";
                WorkResAssignDateSort = "ASC";
                WorkResWorkStateSort = "";
            }
            else
            {
                WorkResAssignDateSort = NextSortDir(WorkResAssignDateSort);
            }
            ViewStateToSession();
            Response.Redirect("~/WorkResults.aspx");
        }
        protected void btnStateSort_Click(object sender, EventArgs e)
        {
            if (WorkResTaskNameSort != "" || WorkResUserAssignerNameSort != "" || WorkResUserNameSort != "" ||
                WorkResAssignDateSort != "")
            {
                WorkResTaskNameSort = "";
                WorkResUserAssignerNameSort = "";
                WorkResUserNameSort = "";
                WorkResAssignDateSort = "";
                WorkResWorkStateSort = "ASC";
            }
            else
            {
                WorkResWorkStateSort = NextSortDir(WorkResWorkStateSort);
            }
            ViewStateToSession();
            Response.Redirect("~/WorkResults.aspx");
        }
        protected void btnApplyFilter_Click(object sender, EventArgs e)
        {
            UserDoerID = int.Parse(ddlUserDoer.SelectedValue);
            UserAssignerID = int.Parse(ddlUserAssigner.SelectedValue);
            WorkStateID = int.Parse(ddlState.SelectedValue);
            string[] sFrom = txtFromDate.Text.Split(new char[] { '.' });
            string[] sTo = txtToDate.Text.Split(new char[] { '.' });
            FromDate = new DateTime(int.Parse(sFrom[2]), int.Parse(sFrom[1]), int.Parse(sFrom[0]));
            ToDate = new DateTime(int.Parse(sTo[2]), int.Parse(sTo[1]), int.Parse(sTo[0]));
            ViewStateToSession();
            Response.Redirect("~/WorkResults.aspx");
        }
        private WorkShowResult[] GetWorkShowResults()
        {
            List<WorkShowResult> res = new List<WorkShowResult>();

            string filter = "AssignDate BETWEEN '" + FromDate.Year + "-" + FromDate.Month + "-" + FromDate.Day +
                " 00:00:00' AND '" + ToDate.Year + "-" + ToDate.Month + "-" + ToDate.Day + " 23:59:59'";
            if (UserDoerID != -1)
            {
                filter += " AND User_ID=" + UserDoerID.ToString();
            }
            if (UserAssignerID != -1)
            {
                filter += " AND UserAssigner_ID=" + UserAssignerID.ToString();
            }
            if (WorkStateID != -1)
            {
                filter += " AND WorkState_ID=" + WorkStateID.ToString();
            }
            filter += " ORDER BY EndDateTime DESC,BeginDateTime DESC";
            Work[] WorkArray = AccessDB.GetWorks(filter);

            foreach (Work wrk in WorkArray)
            {
                WorkShowResult wsr = new WorkShowResult();
                wsr.WorkToShow = wrk;
                wsr.UserName = AccessDB.GetUser(wrk.UserID).ToString();
                wsr.TaskName = wrk.TaskName;
                wsr.UserCreatorID = AccessDB.GetTask(wrk.TaskID).CreatorUserID;
                wsr.UserCreatorName = AccessDB.GetUser(wsr.UserCreatorID).ToString();
                wsr.UserAssignerName = AccessDB.GetUser(wrk.UserAssignerID).ToString();
                res.Add(wsr);
            }

            return res.ToArray();
        }
        protected void btnShowResult_Click(object sender, EventArgs e)
        {
            int num = int.Parse(((LinkButton)sender).CommandArgument);
            ViewState["CurrentWorkToShowResult"] = WorkShowResultArray[num].WorkToShow;
            ViewStateToSession();
            Response.Redirect("~/ShowWorkResult.aspx");
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int num = int.Parse(((ImageButton)sender).CommandArgument);
                AccessDB.DeleteWork(WorkShowResultArray[num].WorkToShow.WorkID);
                ViewStateToSession();
                Response.Redirect("~/WorkResults.aspx");
            }
        }
        protected void btnMyWork_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/MyWork.aspx");
        }
        protected void btnTests_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/Tests.aspx");
        }
        protected void btnTasks_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/Tasks.aspx");
        }
        protected void btnUsers_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/Users.aspx");
        }
        private void SessionToViewState()
        {
            for (int i = 0; i < Session.Count; i++)
            {
                ViewState[Session.Keys[i].ToString()] = Session[Session.Keys[i].ToString()];
            }
        }
        private void ViewStateToSession()
        {
            string[] Keys = new string[ViewState.Count];
            ViewState.Keys.CopyTo(Keys, 0);
            for (int i = 0; i < Keys.Count(); i++)
            {
                Session[Keys[i]] = ViewState[Keys[i]];
            }
        }
    }
}
