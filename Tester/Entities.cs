﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tester
{
    [Serializable]
    public class UserDB
    {
        public int UserID
        {
            get;
            set;
        }
        public string Login
        {
            get;
            set;
        }
        public string Password
        {
            get;
            set;
        }
        public bool IsAdmin
        {
            get;
            set;
        }
        public string FirstName
        {
            get;
            set;
        }
        public string SecondName
        {
            get;
            set;
        }
        public string EMail
        {
            get;
            set;
        }
        public override string ToString()
        {
            return FirstName + " " + SecondName;
        }
    }
    [Serializable]
    public class Test
    {
        public int TestID
        {
            get;
            set;
        }
        public string Theme
        {
            get;
            set;
        }
        public int OrderNumber
        {
            get;
            set;
        }
        public override string ToString()
        {
            return Theme;
        }
    }
    [Serializable]
    public class TestItem
    {
        public int TestItemID
        {
            get;
            set;
        }
        public int TestID
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
        public string Difficulty
        {
            get;
            set;
        }
        public string QuestionRtf
        {
            get;
            set;
        }
        public string QuestionHtml
        {
            get;
            set;
        }
        public int AnswerTypeID
        {
            get;
            set;
        }
        public string AnswerType
        {
            get;
            set;
        }
        public string Answer
        {
            get;
            set;
        }
        public string Year
        {
            get;
            set;
        }
        public string Comment1
        {
            get;
            set;
        }
        public string Comment2
        {
            get;
            set;
        }
        public int OrderNumber
        {
            get;
            set;
        }
        public override string ToString()
        {
            string Res = "Задание " + OrderNumber.ToString();
            if (Name != null)
            {
                if (Name != "")
                {
                    Res += "(" + Name + ")";
                }
            }
            return Res;
        }
    }
    [Serializable]
    public class AnswerType
    {
        public int AnswerTypeID
        {
            get;
            set;
        }
        public string AnswerTypeVal
        {
            get;
            set;
        }
        public override string ToString()
        {
            return AnswerTypeVal;
        }
    }
    [Serializable]
    public class Task
    {
        public int TaskID
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
        public int CreatorUserID
        {
            get;
            set;
        }
        public string CreatorUserString
        {
            get;
            set;
        }
        public DateTime CreateDate
        {
            get;
            set;
        }
        public DateTime LastModifyDate
        {
            get;
            set;
        }
        public int[] TaskTestItemIDs
        {
            get;
            set;
        }
        public override string ToString()
        {
            return Name + " (" + CreatorUserString + " от " + CreateDate.ToShortDateString() + ")";
        }
    }
    [Serializable]
    public class Work
    {
        public int WorkID
        {
            get;
            set;
        }
        public int UserID
        {
            get;
            set;
        }
        public int UserAssignerID
        {
            get;
            set;
        }
        public int TaskID
        {
            get;
            set;
        }
        public string TaskName
        {
            get;
            set;
        }
        public bool Continuous
        {
            get;
            set;
        }
        public bool ShowAnswer
        {
            get;
            set;
        }
        public bool Repeatable
        {
            get;
            set;
        }
        public int WorkStateID
        {
            get;
            set;
        }
        public string WorkState
        {
            get;
            set;
        }
        public DateTime AssignDate
        {
            get;
            set;
        }
        public DateTime? LastChangeStateDate
        {
            get;
            set;
        }
        public DateTime? BeginDateTime
        {
            get;
            set;
        }
        public DateTime? EndDateTime
        {
            get;
            set;
        }
    }
    [Serializable]
    public class WorkState
    {
        public int WorkStateID
        {
            get;
            set;
        }
        public string WorkStateString
        {
            get;
            set;
        }
    }
    [Serializable]
    public class WorkResult
    {
        public int WorkResultID
        {
            get;
            set;
        }
        public int WorkID
        {
            get;
            set;
        }
        public int TestItemID
        {
            get;
            set;
        }
        public int OrderNumber
        {
            get;
            set;
        }
        public string Answer
        {
            get;
            set;
        }
        public bool Correct
        {
            get;
            set;
        }
        public DateTime AnswerTimeStamp
        {
            get;
            set;
        }
    }
}