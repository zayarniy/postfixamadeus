﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Tester
{
    public partial class Tasks : System.Web.UI.Page
    {
        Task[] TaskArr;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                SessionToViewState();
                ShowFilter();
            }
            if (ViewState["CurUser"] != null)
            {
                UserDB usr = (UserDB)ViewState["CurUser"];
                string filter = "CreateDate BETWEEN '" + FromDate.Year + "-" + FromDate.Month + "-" + FromDate.Day +
                    " 00:00:00' AND '" + ToDate.Year + "-" + ToDate.Month + "-" + ToDate.Day + " 23:59:59'";
                if (OnlyMine)
                {
                    filter += " AND CreatorUser_ID=" + usr.UserID.ToString();
                }
                TaskArr = AccessDB.GetTasks(filter);
                OrderTasks(TaskArr);
                rptTaskList.DataSource = TaskArr;
                rptTaskList.DataBind();
            }
            SetButtons();
        }
        private void SetButtons()
        {
            btnMyWork.Attributes.Add("onmouseout", "this.src='Buttons/MyWork.png'");
            btnMyWork.Attributes.Add("onmouseover", "this.src='Buttons/MyWorkMouseOver.png'");
            btnTests.Attributes.Add("onmouseout", "this.src='Buttons/Tests.png'");
            btnTests.Attributes.Add("onmouseover", "this.src='Buttons/TestsMouseOver.png'");
            btnWorkResults.Attributes.Add("onmouseout", "this.src='Buttons/WorkResults.png'");
            btnWorkResults.Attributes.Add("onmouseover", "this.src='Buttons/WorkResultsMouseOver.png'");
            btnUsers.Attributes.Add("onmouseout", "this.src='Buttons/Users.png'");
            btnUsers.Attributes.Add("onmouseover", "this.src='Buttons/UsersMouseOver.png'");
            btnApplyFilter.Attributes.Add("onmouseout", "this.src='Buttons/ApplyFilter.png'");
            btnApplyFilter.Attributes.Add("onmouseover", "this.src='Buttons/ApplyFilterMouseOver.png'");
            btnCreateTask.Attributes.Add("onmouseout", "this.src='Buttons/CreateTask.png'");
            btnCreateTask.Attributes.Add("onmouseover", "this.src='Buttons/CreateTaskMouseOver.png'");
        }
        private int CompareTask(Task t1, Task t2)
        {
            if (TasksNameSort == "ASC")
            {
                return t1.Name.CompareTo(t2.Name);
            }
            if (TasksNameSort == "DESC")
            {
                return t2.Name.CompareTo(t1.Name);
            }
            if (TasksCreatorSort == "ASC")
            {
                return t1.CreatorUserString.CompareTo(t2.CreatorUserString);
            }
            if (TasksCreatorSort == "DESC")
            {
                return t2.CreatorUserString.CompareTo(t1.CreatorUserString);
            }
            if (TasksCreateDateSort == "ASC")
            {
                return t1.CreateDate.CompareTo(t2.CreateDate);
            }
            if (TasksCreateDateSort == "DESC")
            {
                return t2.CreateDate.CompareTo(t1.CreateDate);
            }
            if (TasksLastModifyDateSort == "ASC")
            {
                return t1.LastModifyDate.CompareTo(t2.LastModifyDate);
            }
            if (TasksLastModifyDateSort == "DESC")
            {
                return t2.LastModifyDate.CompareTo(t1.LastModifyDate);
            }
            return 0;
        }
        private void OrderTasks(Task[] Tasks)
        {
            if (TasksNameSort != "" || TasksCreatorSort != "" || TasksCreateDateSort != "" || TasksLastModifyDateSort != "")
            {
                Array.Sort(Tasks, CompareTask);
            }
        }
        private string NextSortDir(string SortDir)
        {
            switch (SortDir)
            {
                case "":
                    return "ASC";
                case "ASC":
                    return "DESC";
                case "DESC":
                    return "";
                default:
                    return "";
            }
        }
        private DateTime FromDate
        {
            get
            {
                if (ViewState["TasksFromDate"] != null)
                {
                    return (DateTime)ViewState["TasksFromDate"];
                }
                else
                {
                    return DateTime.Now.AddMonths(-3);
                }
            }
            set
            {
                ViewState["TasksFromDate"] = value;
            }
        }
        private DateTime ToDate
        {
            get
            {
                if (ViewState["TasksToDate"] != null)
                {
                    return (DateTime)ViewState["TasksToDate"];
                }
                else
                {
                    return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                }
            }
            set
            {
                ViewState["TasksToDate"] = value;
            }
        }
        private bool OnlyMine
        {
            get
            {
                if (ViewState["TasksOnlyMine"] != null)
                {
                    return (bool)ViewState["TasksOnlyMine"];
                }
                else
                {
                    return true;
                }
            }
            set
            {
                ViewState["TasksOnlyMine"] = value;
            }
        }
        private string TasksNameSort
        {
            get
            {
                if (ViewState["TasksNameSort"] != null)
                {
                    return (string)ViewState["TasksNameSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["TasksNameSort"] = value;
            }
        }
        private string TasksCreatorSort
        {
            get
            {
                if (ViewState["TasksCreatorSort"] != null)
                {
                    return (string)ViewState["TasksCreatorSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["TasksCreatorSort"] = value;
            }
        }
        private string TasksCreateDateSort
        {
            get
            {
                if (ViewState["TasksCreateDateSort"] != null)
                {
                    return (string)ViewState["TasksCreateDateSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["TasksCreateDateSort"] = value;
            }
        }
        private string TasksLastModifyDateSort
        {
            get
            {
                if (ViewState["TasksLastModifyDateSort"] != null)
                {
                    return (string)ViewState["TasksLastModifyDateSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["TasksLastModifyDateSort"] = value;
            }
        }
        private void ShowFilter()
        {
            chkOnlyMine.Checked = OnlyMine;
            txtFromDate.Text = FromDate.ToString("dd.MM.yyyy");
            txtToDate.Text = ToDate.ToString("dd.MM.yyyy");
        }
        protected void btnTaskNameSort_Click(object sender, EventArgs e)
        {
            if (TasksCreatorSort != "" || TasksCreateDateSort != "" || TasksLastModifyDateSort != "")
            {
                TasksNameSort = "ASC";
                TasksCreatorSort = "";
                TasksCreateDateSort = "";
                TasksLastModifyDateSort = "";
            }
            else
            {
                TasksNameSort = NextSortDir(TasksNameSort);
            }
            ViewStateToSession();
            Response.Redirect("~/Tasks.aspx");
        }
        protected void btnUserCreatorSort_Click(object sender, EventArgs e)
        {
            if (TasksNameSort != "" || TasksCreateDateSort != "" || TasksLastModifyDateSort != "")
            {
                TasksNameSort = "";
                TasksCreatorSort = "ASC";
                TasksCreateDateSort = "";
                TasksLastModifyDateSort = "";
            }
            else
            {
                TasksCreatorSort = NextSortDir(TasksCreatorSort);
            }
            ViewStateToSession();
            Response.Redirect("~/Tasks.aspx");
        }
        protected void btnCreateDateSort_Click(object sender, EventArgs e)
        {
            if (TasksNameSort != "" || TasksCreatorSort != "" || TasksLastModifyDateSort != "")
            {
                TasksNameSort = "";
                TasksCreatorSort = "";
                TasksCreateDateSort = "ASC";
                TasksLastModifyDateSort = "";
            }
            else
            {
                TasksCreateDateSort = NextSortDir(TasksCreateDateSort);
            }
            ViewStateToSession();
            Response.Redirect("~/Tasks.aspx");
        }
        protected void btnLastChangeDateSort_Click(object sender, EventArgs e)
        {
            if (TasksNameSort != "" || TasksCreatorSort != "" || TasksCreateDateSort != "")
            {
                TasksNameSort = "";
                TasksCreatorSort = "";
                TasksCreateDateSort = "";
                TasksLastModifyDateSort = "ASC";
            }
            else
            {
                TasksLastModifyDateSort = NextSortDir(TasksLastModifyDateSort);
            }
            ViewStateToSession();
            Response.Redirect("~/Tasks.aspx");
        }
        protected void btnApplyFilter_Click(object sender, EventArgs e)
        {
            OnlyMine = chkOnlyMine.Checked;
            string[] sFrom = txtFromDate.Text.Split(new char[] { '.' });
            string[] sTo = txtToDate.Text.Split(new char[] { '.' });
            FromDate = new DateTime(int.Parse(sFrom[2]), int.Parse(sFrom[1]), int.Parse(sFrom[0]));
            ToDate = new DateTime(int.Parse(sTo[2]), int.Parse(sTo[1]), int.Parse(sTo[0]));
            ViewStateToSession();
            Response.Redirect("~/Tasks.aspx");
        }
        
        protected void btnMyWork_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/MyWork.aspx");
        }
        protected void btnTests_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/Tests.aspx");
        }
        protected void btnWorkResults_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/WorkResults.aspx");
        }
        protected void btnUsers_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/Users.aspx");
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            int TaskID = int.Parse(((LinkButton)sender).CommandArgument);
            Task tsk = AccessDB.GetTask(TaskID);
            ViewState["CurrentTask"] = tsk;
            ViewStateToSession();
            Response.Redirect("~/EditTask.aspx");
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int TaskID = int.Parse(((ImageButton)sender).CommandArgument);
                AccessDB.DeleteTask(TaskID);
                ViewStateToSession();
                Response.Redirect("~/Tasks.aspx");
            }
        }
        protected void btnAssign_Click(object sender, EventArgs e)
        {
            int TaskID = int.Parse(((ImageButton)sender).CommandArgument);
            Task tsk = AccessDB.GetTask(TaskID);
            ViewState["CurrentTask"] = tsk;
            ViewStateToSession();
            Response.Redirect("~/AssignTask.aspx");
        }
        protected void btnCreateTask_Click(object sender, EventArgs e)
        {
            Task tsk = new Task();
            tsk.Name = "Без названия";
            if (ViewState["CurUser"] != null)
            {
                UserDB usr = (UserDB)ViewState["CurUser"];
                tsk.CreatorUserID = usr.UserID;
                tsk.CreatorUserString = usr.ToString();
            }
            else
            {
                tsk.CreatorUserID = 0;
                tsk.CreatorUserString = "";
            }
            tsk.CreateDate = DateTime.Now;
            tsk.LastModifyDate = DateTime.Now;
            tsk.TaskTestItemIDs = new int[0];
            ViewState["CurrentTask"] = tsk;
            ViewStateToSession();
            Response.Redirect("~/EditTask.aspx");
        }
        private void SessionToViewState()
        {
            for (int i = 0; i < Session.Count; i++)
            {
                ViewState[Session.Keys[i].ToString()] = Session[Session.Keys[i].ToString()];
            }
        }
        private void ViewStateToSession()
        {
            string[] Keys = new string[ViewState.Count];
            ViewState.Keys.CopyTo(Keys, 0);
            for (int i = 0; i < Keys.Count(); i++)
            {
                Session[Keys[i]] = ViewState[Keys[i]];
            }
        }
    }
}