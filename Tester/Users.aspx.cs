﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Tester
{
    public partial class Users : System.Web.UI.Page
    {
        UserDB[] UserArr;
        bool AddFlag;
        bool EditFlag;
        int UserIDForEdit;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SessionToViewState();
            }
            if (ViewState["AddFlag"] != null)
            {
                AddFlag = (bool)ViewState["AddFlag"];
            }
            else
            {
                AddFlag = false;
            }
            if (ViewState["EditFlag"] != null)
            {
                EditFlag = (bool)ViewState["EditFlag"];
            }
            else
            {
                EditFlag = false;
            }
            if (ViewState["UserIDForEdit"] != null)
            {
                UserIDForEdit = (int)ViewState["UserIDForEdit"];
            }
            else
            {
                UserIDForEdit = 0;
            }
            ShowParts();
            if (!IsPostBack)
            {
                if (EditFlag)
                {
                    FillPNUserFields();
                }
            }
            UserArr = AccessDB.GetUsers();
            rptUserList.DataSource = UserArr;
            OrderUsers(UserArr);
            rptUserList.DataBind();
            SetButtons();
        }
        private void SetButtons()
        {
            btnMyWork.Attributes.Add("onmouseout", "this.src='Buttons/MyWork.png'");
            btnMyWork.Attributes.Add("onmouseover", "this.src='Buttons/MyWorkMouseOver.png'");
            btnTests.Attributes.Add("onmouseout", "this.src='Buttons/Tests.png'");
            btnTests.Attributes.Add("onmouseover", "this.src='Buttons/TestsMouseOver.png'");
            btnTasks.Attributes.Add("onmouseout", "this.src='Buttons/Tasks.png'");
            btnTasks.Attributes.Add("onmouseover", "this.src='Buttons/TasksMouseOver.png'");
            btnWorkResults.Attributes.Add("onmouseout", "this.src='Buttons/WorkResults.png'");
            btnWorkResults.Attributes.Add("onmouseover", "this.src='Buttons/WorkResultsMouseOver.png'");
            btnAddUser.Attributes.Add("onmouseout", "this.src='Buttons/AddUser.png'");
            btnAddUser.Attributes.Add("onmouseover", "this.src='Buttons/AddUserMouseOver.png'");
            btnOK.Attributes.Add("onmouseout", "this.src='Buttons/OK.png'");
            btnOK.Attributes.Add("onmouseover", "this.src='Buttons/OKMouseOver.png'");
            btnCancel.Attributes.Add("onmouseout", "this.src='Buttons/Cancel.png'");
            btnCancel.Attributes.Add("onmouseover", "this.src='Buttons/CancelMouseOver.png'");
        }
        private int CompareUser(UserDB u1, UserDB u2)
        {
            if (UsersLoginSort == "ASC")
            {
                return u1.Login.CompareTo(u2.Login);
            }
            if (UsersLoginSort == "DESC")
            {
                return u2.Login.CompareTo(u1.Login);
            }
            if (UsersFirstNameSort == "ASC")
            {
                return u1.FirstName.CompareTo(u2.FirstName);
            }
            if (UsersFirstNameSort == "DESC")
            {
                return u2.FirstName.CompareTo(u1.FirstName);
            }
            if (UsersSecondNameSort == "ASC")
            {
                return u1.SecondName.CompareTo(u2.SecondName);
            }
            if (UsersSecondNameSort == "DESC")
            {
                return u2.SecondName.CompareTo(u1.SecondName);
            }
            if (UsersIsAdminSort == "ASC")
            {
                return u1.IsAdmin.CompareTo(u2.IsAdmin);
            }
            if (UsersIsAdminSort == "DESC")
            {
                return u2.IsAdmin.CompareTo(u1.IsAdmin);
            }
            if (UsersEMailSort == "ASC")
            {
                return u1.EMail.CompareTo(u2.EMail);
            }
            if (UsersEMailSort == "DESC")
            {
                return u2.EMail.CompareTo(u1.EMail);
            }
            return 0;
        }
        private void OrderUsers(UserDB[] Users)
        {
            if (UsersLoginSort != "" || UsersFirstNameSort != "" || UsersSecondNameSort != "" || 
                UsersIsAdminSort != "" || UsersEMailSort != "")
            {
                Array.Sort(Users, CompareUser);
            }
        }
        private string NextSortDir(string SortDir)
        {
            switch (SortDir)
            {
                case "":
                    return "ASC";
                case "ASC":
                    return "DESC";
                case "DESC":
                    return "";
                default:
                    return "";
            }
        }
        private string UsersLoginSort
        {
            get
            {
                if (ViewState["UsersLoginSort"] != null)
                {
                    return (string)ViewState["UsersLoginSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["UsersLoginSort"] = value;
            }
        }
        private string UsersFirstNameSort
        {
            get
            {
                if (ViewState["UsersFirstNameSort"] != null)
                {
                    return (string)ViewState["UsersFirstNameSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["UsersFirstNameSort"] = value;
            }
        }
        private string UsersSecondNameSort
        {
            get
            {
                if (ViewState["UsersSecondNameSort"] != null)
                {
                    return (string)ViewState["UsersSecondNameSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["UsersSecondNameSort"] = value;
            }
        }
        private string UsersIsAdminSort
        {
            get
            {
                if (ViewState["UsersIsAdminSort"] != null)
                {
                    return (string)ViewState["UsersIsAdminSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["UsersIsAdminSort"] = value;
            }
        }
        private string UsersEMailSort
        {
            get
            {
                if (ViewState["UsersEMailSort"] != null)
                {
                    return (string)ViewState["UsersEMailSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["UsersEMailSort"] = value;
            }
        }
        protected void btnLoginSort_Click(object sender, EventArgs e)
        {
            if (UsersFirstNameSort != "" || UsersSecondNameSort != "" ||
                UsersIsAdminSort != "" || UsersEMailSort != "")
            {
                UsersLoginSort = "ASC";
                UsersFirstNameSort = "";
                UsersSecondNameSort = "";
                UsersIsAdminSort = "";
                UsersEMailSort = "";
            }
            else
            {
                UsersLoginSort = NextSortDir(UsersLoginSort);
            }
            ViewStateToSession();
            Response.Redirect("~/Users.aspx");
        }
        protected void btnFirstNameSort_Click(object sender, EventArgs e)
        {
            if (UsersLoginSort != "" || UsersSecondNameSort != "" ||
                UsersIsAdminSort != "" || UsersEMailSort != "")
            {
                UsersLoginSort = "";
                UsersFirstNameSort = "ASC";
                UsersSecondNameSort = "";
                UsersIsAdminSort = "";
                UsersEMailSort = "";
            }
            else
            {
                UsersFirstNameSort = NextSortDir(UsersFirstNameSort);
            }
            ViewStateToSession();
            Response.Redirect("~/Users.aspx");
        }
        protected void btnSecondNameSort_Click(object sender, EventArgs e)
        {
            if (UsersLoginSort != "" || UsersFirstNameSort != "" || 
                UsersIsAdminSort != "" || UsersEMailSort != "")
            {
                UsersLoginSort = "";
                UsersFirstNameSort = "";
                UsersSecondNameSort = "ASC";
                UsersIsAdminSort = "";
                UsersEMailSort = "";
            }
            else
            {
                UsersSecondNameSort = NextSortDir(UsersSecondNameSort);
            }
            ViewStateToSession();
            Response.Redirect("~/Users.aspx");
        }
        protected void btnIsAdminSort_Click(object sender, EventArgs e)
        {
            if (UsersLoginSort != "" || UsersFirstNameSort != "" || UsersSecondNameSort != "" ||
                UsersEMailSort != "")
            {
                UsersLoginSort = "";
                UsersFirstNameSort = "";
                UsersSecondNameSort = "";
                UsersIsAdminSort = "ASC";
                UsersEMailSort = "";
            }
            else
            {
                UsersIsAdminSort = NextSortDir(UsersIsAdminSort);
            }
            ViewStateToSession();
            Response.Redirect("~/Users.aspx");
        }
        protected void btnEMailSort_Click(object sender, EventArgs e)
        {
            if (UsersLoginSort != "" || UsersFirstNameSort != "" || UsersSecondNameSort != "" ||
                UsersIsAdminSort != "")
            {
                UsersLoginSort = "";
                UsersFirstNameSort = "";
                UsersSecondNameSort = "";
                UsersIsAdminSort = "";
                UsersEMailSort = "ASC";
            }
            else
            {
                UsersEMailSort = NextSortDir(UsersEMailSort);
            }
            ViewStateToSession();
            Response.Redirect("~/Users.aspx");
        }
        private void ShowParts()
        {
            pnUser.Visible = AddFlag || EditFlag;
            btnAddUser.Visible = !(AddFlag || EditFlag);
            btnOK.Visible = AddFlag || EditFlag;
            btnCancel.Visible = AddFlag || EditFlag;
        }
        private void FillPNUserFields()
        {
            UserDB usr = AccessDB.GetUser(UserIDForEdit);
            if (usr != null)
            {
                txtLogin.Text = usr.Login;
                chkIsAdmin.Checked = usr.IsAdmin;
                txtFirstName.Text = usr.FirstName;
                txtSecondName.Text = usr.SecondName;
                txtEMail.Text = usr.EMail;
            }            
        }
        private void ClearState()
        {
            ViewState["AddFlag"] = null;
            ViewState["EditFlag"] = null;
        }
        protected void btnMyWork_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/MyWork.aspx");
        }
        protected void btnTests_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/Tests.aspx");
        }
        protected void btnTasks_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/Tasks.aspx");
        }
        protected void btnWorkResults_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/WorkResults.aspx");
        }
        protected void btnChangeUser_Click(object sender, EventArgs e)
        {
            ViewState["UserIDForEdit"] = int.Parse(((ImageButton)sender).CommandArgument);
            ViewState["EditFlag"] = true;
            ViewStateToSession();
            Response.Redirect("~/Users.aspx");
        }
        protected void btnClearPassword_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = int.Parse(((ImageButton)sender).CommandArgument);
                AccessDB.ClearPassword(userID);
                ViewStateToSession();
                Response.Redirect("~/Users.aspx");
            }
        }
        protected void btnDeleteUser_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                int userID = int.Parse(((ImageButton)sender).CommandArgument);
                AccessDB.DeleteUser(userID);
                ViewStateToSession();
                Response.Redirect("~/Users.aspx");
            }
        }
        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            ViewState["AddFlag"] = true;
            ViewStateToSession();
            Response.Redirect("~/Users.aspx");
        }
        protected void btnOK_Click(object sender, EventArgs e)
        {
            UserDB user;
            if (AddFlag)
            {
                user = new UserDB();
                user.UserID = 0;
                user.Login = txtLogin.Text;
                user.Password = "";
                user.FirstName = txtFirstName.Text;
                user.SecondName = txtSecondName.Text;
                user.IsAdmin = chkIsAdmin.Checked;
                user.EMail = txtEMail.Text;
                AccessDB.AddUser(user);
            }
            if (EditFlag)
            {
                user = AccessDB.GetUser(UserIDForEdit);
                if (user!=null)
                {
                    user.Login = txtLogin.Text;
                    user.FirstName = txtFirstName.Text;
                    user.SecondName = txtSecondName.Text;
                    user.IsAdmin = chkIsAdmin.Checked;
                    user.EMail = txtEMail.Text;
                    AccessDB.ModifyUser(user);
                }
            }
            ClearState();
            ViewStateToSession();
            Response.Redirect("~/Users.aspx");
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearState();
            ViewStateToSession();
            Response.Redirect("~/Users.aspx");
        }
        private void SessionToViewState()
        {
            for (int i = 0; i < Session.Count; i++)
            {
                ViewState[Session.Keys[i].ToString()] = Session[Session.Keys[i].ToString()];
            }
        }
        private void ViewStateToSession()
        {
            string[] Keys = new string[ViewState.Count];
            ViewState.Keys.CopyTo(Keys, 0);
            for (int i = 0; i < Keys.Count(); i++)
            {
                Session[Keys[i]] = ViewState[Keys[i]];
            }
        }
    }
}