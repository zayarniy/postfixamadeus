﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Tester
{
    [Serializable]
    public class AddTestItem
    {
        public int TestID
        {
            get;
            set;
        }
        public int OrderNumber
        {
            get;
            set;
        }
        public string Theme
        {
            get;
            set;
        }
        public int ItemCount
        {
            get;
            set;
        }
        public int[] ItemsForList
        {
            get;
            set;
        }
        public int AllItemCount
        {
            get;
            set;
        }
    }
    public class TaskTreeTestItem
    {
        public int TestID
        {
            get;
            set;
        }
        public int[] TestItemsIDs
        {
            get;
            set;
        }
    }
    public partial class EditTask : System.Web.UI.Page
    {        
        Task CurrTask;
        TaskTreeTestItem[] TaskItems;
        AddTestItem[] AddTests;
        private Dictionary<string, bool> TVWTaskItemsExpState;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SessionToViewState();
            }
            if (ViewState["CurrentTask"] != null)
            {
                CurrTask = (Task)ViewState["CurrentTask"];
                TaskItems = GetTaskTreeTestItems(CurrTask);
                if (!IsPostBack)
                {   
                    UpdateTaskAttr();
                    UpdateTaskTree();
                    UpdateTestLisl();
                }
                SetButtons();
            }
            if (ViewState["TVWTaskItemsExpState"]!=null)
            {
                TVWTaskItemsExpState = (Dictionary<string, bool>)ViewState["TVWTaskItemsExpState"];
                RestoreTVWTaskItemsState(TVWTaskItemsExpState);
            }
            
        }
        private void SetButtons()
        {
            trvwTaskItems.Attributes.Add("onclick", "postBackByObject()");
            btnOK.Attributes.Add("onmouseout", "this.src='Buttons/OK.png'");
            btnOK.Attributes.Add("onmouseover", "this.src='Buttons/OKMouseOver.png'");
            btnCancel.Attributes.Add("onmouseout", "this.src='Buttons/Cancel.png'");
            btnCancel.Attributes.Add("onmouseover", "this.src='Buttons/CancelMouseOver.png'");
            if (CurrTask.TaskID == 0)
            {
                btnCancel.Attributes.Add("confirmmessage", "Отменить создание нового задания?");
            }
            else
            {
                btnCancel.Attributes.Add("confirmmessage", "Отменить редактирование задания " + CurrTask.ToString() + "?");
            }
        }
        private void UpdateTaskAttr()
        {
            if (CurrTask.TaskID == 0)
            {
                litTitle.Text = "Создание нового задания";
            }
            else
            {
                litTitle.Text = "Редактирование задания" + CurrTask.ToString();
            }
            txtName.Text = CurrTask.Name;
            lblCreator.Text = CurrTask.CreatorUserString;
            lblCreateDate.Text = CurrTask.CreateDate.ToString();
            lblLastModifyDate.Text = CurrTask.LastModifyDate.ToString();
        }
        private void UpdateTestLisl()
        {
            Test[] AllTests = AccessDB.GetTests();
            List<AddTestItem> atiList = new List<AddTestItem>();
            foreach(Test t in AllTests)
            {
                AddTestItem ati = new AddTestItem();
                ati.TestID = t.TestID;
                ati.OrderNumber = t.OrderNumber;
                ati.Theme = t.Theme;
                ati.AllItemCount = AccessDB.GetTestItemCount(t.TestID);
                ati.ItemsForList = new int[ati.AllItemCount + 1];
                for (int i = 0; i <= ati.AllItemCount; i++)
                {
                    ati.ItemsForList[i] = i;
                }

                var res = from ti in TaskItems where ti.TestID == t.TestID select ti.TestItemsIDs.Count();
                if (res.Count()!=0)
                {
                    ati.ItemCount = res.First();
                }
                else
                {
                    ati.ItemCount = 0;
                }
                atiList.Add(ati);
            }
            AddTests = atiList.ToArray();
            rptTests.DataSource = AddTests;
            rptTests.DataBind();
            for (int i = 0; i < rptTests.Items.Count; i++)
            {
                DropDownList ddl = (DropDownList)rptTests.Items[i].FindControl("ddlTestItemCount");
                ddl.SelectedIndex = AddTests[i].ItemCount;
            }

        }
        private void UpdateTaskTree()
        {
            trvwTaskItems.Nodes.Clear();
            foreach(TaskTreeTestItem ttti in TaskItems)
            {
                Test tst = AccessDB.GetTest(ttti.TestID);
                int TestItemsCount = AccessDB.GetTestItemCount(tst.TestID);
                TreeNode ndTop = new TreeNode(tst.ToString() + " (" + ttti.TestItemsIDs.Count() + " из " +
                    TestItemsCount.ToString() + ")", tst.TestID.ToString());
                ndTop.ShowCheckBox = true;
                ndTop.Checked = true;
                TestItem[] tstItems = AccessDB.GetTestItems(ttti.TestID);
                foreach(TestItem tstItem in tstItems)
                {
                    TreeNode ndChild = new TreeNode(tstItem.ToString(), tstItem.TestItemID.ToString());
                    ndChild.ShowCheckBox = true;
                    bool bChecked = false;
                    for (int i = 0; i < ttti.TestItemsIDs.Count(); i++)
                    {
                        if (ttti.TestItemsIDs.Contains(tstItem.TestItemID))
                        {
                            bChecked = true;
                            break;
                        }
                    }
                    ndChild.Checked = bChecked;
                    ndTop.ChildNodes.Add(ndChild);
                }
                trvwTaskItems.Nodes.Add(ndTop);
            }
            trvwTaskItems.CollapseAll();
        }
        private TaskTreeTestItem[] GetTaskTreeTestItems(Task pTask)
        {
            Dictionary<int, List<int>> dictTask = new Dictionary<int, List<int>>();
            foreach (int testItemID in pTask.TaskTestItemIDs)
            {
                TestItem ti = AccessDB.GetTestItem(testItemID);
                if (dictTask.ContainsKey(ti.TestID))
                {
                    List<int> lstItemIDs = dictTask[ti.TestID];
                    lstItemIDs.Add(ti.TestItemID);
                }
                else
                {
                    List<int> lstItemIDs = new List<int>();
                    lstItemIDs.Add(ti.TestItemID);
                    dictTask.Add(ti.TestID, lstItemIDs);
                }
            }
            List<TaskTreeTestItem> res = new List<TaskTreeTestItem>();
            foreach(KeyValuePair<int,List<int>> kvp in dictTask)
            {
                TaskTreeTestItem ttti = new TaskTreeTestItem();
                Test tst = AccessDB.GetTest(kvp.Key);
                ttti.TestID = tst.TestID;
                List<int> TaskIDs = new List<int>();
                List<string> TaskStrings = new List<string>();
                foreach(int testItemID in kvp.Value)
                {
                    TestItem tsk = AccessDB.GetTestItem(testItemID);
                    TaskIDs.Add(testItemID);
                    TaskStrings.Add(tsk.ToString());
                }
                ttti.TestItemsIDs = TaskIDs.ToArray();
                res.Add(ttti);
            }
            return res.ToArray();
        }
        private void UpdateTaskItems(int RPTRowNum)
        {
            int tstID = int.Parse(((HiddenField)rptTests.Items[RPTRowNum].FindControl("hdnTestID")).Value);
            int nCnt = ((DropDownList)rptTests.Items[RPTRowNum].FindControl("ddlTestItemCount")).SelectedIndex;
            int nMax = AccessDB.GetTestItemCount(tstID);

            nCnt = nCnt > 0 ? nCnt : 0;
            nCnt = nCnt <= nMax ? nCnt : nMax;
            List<int> res = new List<int>();
            List<int> vals = new List<int>(nMax);
            for (int i = 0; i <= nMax; i++)
            {
                vals.Add(i);
            }
            Random rnd = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < nCnt; i++)
            {
                int rndi = rnd.Next(0, vals.Count() - 1);
                res.Add(vals[rndi]);
                vals.RemoveAt(rndi);
            }
            TestItem[] tstItems = AccessDB.GetTestItems(tstID);
            List<int> tstItemsIDs = CurrTask.TaskTestItemIDs.ToList();
            foreach (TestItem ti in tstItems)
            {
                tstItemsIDs.Remove(ti.TestItemID);
            }
            foreach (int tstnum in res)
            {
                tstItemsIDs.Add(tstItems[tstnum].TestItemID);
            }
            CurrTask.TaskTestItemIDs = tstItemsIDs.ToArray();
            CurrTask.Name = txtName.Text;
            ViewState["CurrentTask"] = CurrTask;
        }
        protected void ddlTestItemCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlTestItemCount = (DropDownList)sender;
            int num = int.Parse(((HiddenField)ddlTestItemCount.Parent.FindControl("hfNum")).Value);
           
            UpdateTaskItems(num);
            ViewStateToSession();
            Response.Redirect("~/EditTask.aspx");
        }
        protected void btnMinusAll_Click(object sender, EventArgs e)
        {
            foreach (RepeaterItem ri in rptTests.Items)
            {
                int tstID = int.Parse(((HiddenField)ri.FindControl("hdnTestID")).Value);
                int nCnt = ((DropDownList)ri.FindControl("ddlTestItemCount")).SelectedIndex;
                int nMax = AccessDB.GetTestItemCount(tstID);

                nCnt--;
                nCnt = nCnt > 0 ? nCnt : 0;
                nCnt = nCnt <= nMax ? nCnt : nMax;

                DropDownList ddlTestItemCount = (DropDownList)ri.FindControl("ddlTestItemCount");
                ddlTestItemCount.SelectedIndex = nCnt;
                int num = int.Parse(((HiddenField)ddlTestItemCount.Parent.FindControl("hfNum")).Value);
                UpdateTaskItems(num);
            }
            ViewStateToSession();
            Response.Redirect("~/EditTask.aspx");
        }
        protected void btnPlusAll_Click(object sender, EventArgs e)
        {
            foreach (RepeaterItem ri in rptTests.Items)
            {
                int tstID = int.Parse(((HiddenField)ri.FindControl("hdnTestID")).Value);
                int nCnt = ((DropDownList)ri.FindControl("ddlTestItemCount")).SelectedIndex;
                int nMax = AccessDB.GetTestItemCount(tstID);

                nCnt++;
                nCnt = nCnt > 0 ? nCnt : 0;
                nCnt = nCnt <= nMax ? nCnt : nMax;

                DropDownList ddlTestItemCount = (DropDownList)ri.FindControl("ddlTestItemCount");
                ddlTestItemCount.SelectedIndex = nCnt;
                int num = int.Parse(((HiddenField)ddlTestItemCount.Parent.FindControl("hfNum")).Value);
                UpdateTaskItems(num);
            }
            ViewStateToSession();
            Response.Redirect("~/EditTask.aspx");
        }
        protected void btnMinusCount_Click(object sender, EventArgs e)
        {
            int num = int.Parse(((ImageButton)sender).CommandArgument);
            DropDownList ddlTestItemCount = (DropDownList)rptTests.Items[num].FindControl("ddlTestItemCount");
            ddlTestItemCount.SelectedIndex--;
            UpdateTaskItems(num);
            ViewStateToSession();
            Response.Redirect("~/EditTask.aspx");
        }
        protected void btnPlusCount_Click(object sender, EventArgs e)
        {
            int num = int.Parse(((ImageButton)sender).CommandArgument);
            DropDownList ddlTestItemCount = (DropDownList)rptTests.Items[num].FindControl("ddlTestItemCount");
            ddlTestItemCount.SelectedIndex++;
            UpdateTaskItems(num);
            ViewStateToSession();
            Response.Redirect("~/EditTask.aspx");
        }
        private void SaveTask()
        {
            if (CurrTask.TaskID == 0)
            {
                CurrTask.Name = txtName.Text;
                CurrTask.LastModifyDate = DateTime.Now;
                AccessDB.AddTask(CurrTask);
            }
            else
            {
                CurrTask.Name = txtName.Text;
                CurrTask.LastModifyDate = DateTime.Now;
                AccessDB.EditTask(CurrTask);
            }
        }
        protected void trvwTaskItems_SelectedNodeChanged(object sender, EventArgs e)
        {
            TreeNode tn = trvwTaskItems.SelectedNode;
            if (tn.Depth == 1)
            {
                int TestItemID = int.Parse(tn.Value);
                TestItem TestItemToShow = AccessDB.GetTestItem(TestItemID);
                string QuestToShow = "<div style='text-align:center'><h3>Тема: " + AccessDB.GetTest(TestItemToShow.TestID).ToString() + "</h3>";
                QuestToShow += "<h4>" + TestItemToShow.ToString() + "</h4></div>";
                QuestToShow += TestItemToShow.QuestionHtml;
                ltrlQuestion.Text = QuestToShow;
            }
        }
        protected void trvwTaskItems_TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            List<int> TestItemIDs = new List<int>();
            foreach (TreeNode ndTop in trvwTaskItems.Nodes)
            {
                if (ndTop.Checked)
                {
                    foreach (TreeNode ndChild in ndTop.ChildNodes)
                    {
                        if (ndChild.Checked)
                        {
                            TestItemIDs.Add(int.Parse(ndChild.Value));
                        }
                    }
                }
            }
            CurrTask.TaskTestItemIDs = TestItemIDs.ToArray();
            CurrTask.Name = txtName.Text;
            ViewState["CurrentTask"] = CurrTask;
            ltrlQuestion.Text = "";
            ViewStateToSession();
            Response.Redirect("~/EditTask.aspx");
        }
        private Dictionary<string, bool> SaveTVWTaskItemsState()
        {
            Dictionary<string, bool> nodeStates = new Dictionary<string, bool>();
            for (int i = 0; i < trvwTaskItems.Nodes.Count; i++)
            {
                if (trvwTaskItems.Nodes[i].ChildNodes.Count > 0)
                {
                    nodeStates.Add(trvwTaskItems.Nodes[i].Value, trvwTaskItems.Nodes[i].Expanded??false);
                }
            }

            return nodeStates;
        }
        private void RestoreTVWTaskItemsState(Dictionary<string, bool> treeState)
        {
            for (int i = 0; i < trvwTaskItems.Nodes.Count; i++)
            {
                if (treeState.ContainsKey(trvwTaskItems.Nodes[i].Value))
                {
                    if (treeState[trvwTaskItems.Nodes[i].Value])
                        trvwTaskItems.Nodes[i].Expand();
                    else
                        trvwTaskItems.Nodes[i].Collapse();
                }
            }
        }
        protected void trvwTaskItems_TreeNodeCollapsed(object sender, TreeNodeEventArgs e)
        {
            TVWTaskItemsExpState = SaveTVWTaskItemsState();
            ViewState["TVWTaskItemsExpState"] = TVWTaskItemsExpState;
            ltrlQuestion.Text = "";
        }

        protected void trvwTaskItems_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
        {
            TVWTaskItemsExpState = SaveTVWTaskItemsState();
            ViewState["TVWTaskItemsExpState"] = TVWTaskItemsExpState;
            ltrlQuestion.Text = "";
        }
        protected void btnOK_Click(object sender, EventArgs e)
        {
            SaveTask();
            ViewStateToSession();
            Response.Redirect("~/Tasks.aspx");
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                ViewStateToSession();
                Response.Redirect("~/Tasks.aspx");
            }
        }
        private void SessionToViewState()
        {
            for (int i = 0; i < Session.Count; i++)
            {
                ViewState[Session.Keys[i].ToString()] = Session[Session.Keys[i].ToString()];
            }
        }
        private void ViewStateToSession()
        {
            string[] Keys = new string[ViewState.Count];
            ViewState.Keys.CopyTo(Keys, 0);
            for (int i = 0; i < Keys.Count(); i++)
            {
                Session[Keys[i]] = ViewState[Keys[i]];
            }
        }
    }
}