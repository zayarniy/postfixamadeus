﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Tester.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Вход в систему</title>
</head>
<body style="background-color: #2E9AFE">
    <form id="form1" runat="server">
        <div style="text-align: center">
            <asp:Label ID="Label1" runat="server" Text="Вход в систему" Font-Names="Times New Roman" Font-Size="36px" ForeColor="#0B0B3B"></asp:Label>
            <br />
            <br />
            <table style="width: 300px;margin: 0 auto;">
                <tr>
                    <td style="width: 100px;text-align:right">
                        <asp:Label ID="lblLogin" runat="server" Text="Логин:" ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
                    </td>
                    <td style="width: 200px;text-align:left">
                        <asp:TextBox ID="txtLogin" runat="server" Width="200px" BackColor="#58FAF4" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px" BorderColor="#58FAF4"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px;text-align:right">
                        <asp:Label ID="lblPassword" runat="server" Text="Пароль:" ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
                    </td>
                    <td style="width: 200px;text-align:left">
                        <asp:TextBox ID="txtPassword" runat="server" Width="200px" TextMode="Password" BackColor="#58FAF4" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px" BorderColor="#58FAF4"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:ImageButton ID="btnEnter" runat="server" OnClick="btnEnter_Click" ImageUrl="~/Buttons/Login.png"/>
                    </td>
                </tr>
            </table>
            <br />
            <asp:Label ID="lblIncorrect" runat="server" Text="" ForeColor="#FE2E2E" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
        </div>
    </form>
</body>
</html>
