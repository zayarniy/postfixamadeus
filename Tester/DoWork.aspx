﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="DoWork.aspx.cs" Inherits="Tester.DoWork" %>

<asp:Content ID="contTitle" ContentPlaceHolderID="contTitle" runat="server">
    <asp:Literal ID="litTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="contScript" ContentPlaceHolderID="contScript" runat="server">
    <script type="text/javascript">
        function ImageMouseOver(el) {
            $(el).attr("src", "Buttons/AnswerMouseOver.png");
        }
        function ImageMouseOut(el) {
            $(el).attr("src", "Buttons/Answer.png");
        }
        function Confirm(el) {
            Message = $(el).attr("confirmmessage");
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm(Message)) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>
<asp:Content ID="contMain" ContentPlaceHolderID="contMain" runat="server">
    <asp:Label ID="Label2" runat="server" Text="Задание"
        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="lblTaskName" runat="server" Text=""
        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="Label5" runat="server" Text="Количество вопросов"
        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="lblTestItemCount" runat="server" Text=""
        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <br />
    <asp:Label ID="Label6" runat="server" Text="Дата-время начала тестирования:"
        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="lblBeginDateTime" runat="server" Text=""
        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <br />
    <asp:Label ID="Label4" runat="server" Text="Время тестирования:" 
        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="lblTestingTime" runat="server" Text="" 
        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <hr />
    <asp:Repeater ID="rptWorkItems" runat="server">
        <HeaderTemplate>
            <table style="text-align: center">
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:Label ID="lblQuestNum" runat="server" Text='<%# "Вопрос "+DataBinder.Eval(Container.DataItem, "OrderNumber")  %>'
                        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width: 1000px; background-color: white; border: 5px double #2E9AFE">
                        <asp:Literal ID="ltrlTestQuestion" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "QuestionHtml")  %>'></asp:Literal>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Ответ" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    <asp:TextBox ID="txtAnswer" runat="server" Width="80px"
                        Text='<%# DataBinder.Eval(Container.DataItem, "Answer") %>'
                        Enabled='<%# DataBinder.Eval(Container.DataItem, "Answer")==""  %>'
                        BackColor='<%# DataBinder.Eval(Container.DataItem, "Answer")=="" ?System.Drawing.Color.FromName("#58FAF4"):System.Drawing.Color.FromName("#2E9AFE") %>'
                        BorderColor='<%# DataBinder.Eval(Container.DataItem, "Answer")=="" ?System.Drawing.Color.FromName("#58FAF4"):System.Drawing.Color.FromName("#2E9AFE") %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="btnAnswer" runat="server"
                        Visible='<%# DataBinder.Eval(Container.DataItem, "Answer")=="" %>'
                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "OrderNumber")  %>'
                        OnClick="btnAnswer_Click"
                        ImageUrl="~/Buttons/Answer.png"
                        onmouseover="ImageMouseOver(this);"
                        onmouseout="ImageMouseOut(this);" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnShowCorrectAnswer" runat="server"
                        Visible='<%# (bool)DataBinder.Eval(Container.DataItem, "ShowCorrectAnswer") && DataBinder.Eval(Container.DataItem, "Answer")!="" && !(bool)DataBinder.Eval(Container.DataItem, "Correct")  %>'>
                        <asp:Label ID="Label1" runat="server" Text="Правильный ответ:"
                            ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                        <asp:Label ID="lblCorrectAnswer" runat="server"
                            Text='<%# DataBinder.Eval(Container.DataItem, "CorrectAnswer")  %>'
                            ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <hr />
    <table>
        <tr>
            <td>
                <asp:ImageButton ID="btnFirst" runat="server" OnClick="btnFirst_Click" ImageUrl="~/Buttons/First.png" />
            </td>
            <td>
                <asp:ImageButton ID="btnPrev" runat="server" OnClick="btnPrev_Click" ImageUrl="~/Buttons/Prev.png" />
            </td>
            <td>
                <asp:ImageButton ID="btnNext" runat="server" OnClick="btnNext_Click" ImageUrl="~/Buttons/Next.png" />
            </td>
            <td>
                <asp:ImageButton ID="btnLast" runat="server" OnClick="btnLast_Click" ImageUrl="~/Buttons/Last.png" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="contButtons" ContentPlaceHolderID="contButtons" runat="server">
    <hr />
    <asp:ImageButton ID="btnOK" runat="server" OnClick="btnOK_Click" ImageUrl="~/Buttons/OK.png" />
    <asp:ImageButton ID="btnCancel" runat="server" OnClick="btnCancel_Click" ImageUrl="~/Buttons/Cancel.png"
        OnClientClick="Confirm(this);" />
</asp:Content>
