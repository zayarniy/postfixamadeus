﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.IO;

namespace Tester
{
    [Serializable]
    public class WorkResultItem
    {
        public int WorkID
        {
            get;
            set;
        }
        public int TaskID
        {
            get;
            set;
        }
        public int TestItemID
        {
            get;
            set;
        }
        public int OrderNumber
        {
            get;
            set;
        }
        public string QuestionHtml
        {
            get;
            set;
        }
        public string Answer
        {
            get;
            set;
        }
        public bool ShowCorrectAnswer
        {
            get;
            set;
        }
        public string CorrectAnswer
        {
            get;
            set;
        }
        public bool Correct
        {
            get;
            set;
        }
        public DateTime AnswerTimeStamp
        {
            get;
            set;
        }
    }
    public partial class DoWork : System.Web.UI.Page
    {
        Work CurrWork;
        WorkResultItem[] WorkResult;
        string ConfirmMessage;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SessionToViewState();
            }
            if (ViewState["CurrentWork"] != null && ViewState["WorkResult"] != null)
            {
                CurrWork = (Work)ViewState["CurrentWork"];
                WorkResult = (WorkResultItem[])ViewState["WorkResult"];
                string sTaskName = AccessDB.GetTask(CurrWork.TaskID).ToString();
                lblTaskName.Text = sTaskName;
                litTitle.Text = sTaskName;
                ConfirmMessage = "Отменить задание " + sTaskName + " без сохранения результатов?";
                lblTestItemCount.Text = WorkResult.Count().ToString();
                lblBeginDateTime.Text = CurrWork.BeginDateTime.ToString();
                if ( CurrWork.BeginDateTime!=null)
                {
                    DateTime dtNow = DateTime.Now;
                    dtNow = new DateTime(dtNow.Ticks - dtNow.Ticks % TimeSpan.TicksPerSecond);
                    lblTestingTime.Text = (dtNow - CurrWork.BeginDateTime).ToString();
                }
                if (!IsPostBack)
                {
                    SetButtonsState();
                    PagedDataSource pDS = new PagedDataSource();
                    pDS.DataSource = WorkResult;
                    pDS.AllowPaging = true;
                    pDS.PageSize = 1;
                    pDS.CurrentPageIndex = CurrentPage;
                    rptWorkItems.DataSource = pDS;
                    rptWorkItems.DataBind();
                }
                
            }
            SetButtons();
        }
        private void SetButtons()
        {
            btnFirst.Attributes.Add("onmouseout", "this.src='Buttons/First.png'");
            btnFirst.Attributes.Add("onmouseover", "this.src='Buttons/FirstMouseOver.png'");
            btnPrev.Attributes.Add("onmouseout", "this.src='Buttons/Prev.png'");
            btnPrev.Attributes.Add("onmouseover", "this.src='Buttons/PrevMouseOver.png'");
            btnNext.Attributes.Add("onmouseout", "this.src='Buttons/Next.png'");
            btnNext.Attributes.Add("onmouseover", "this.src='Buttons/NextMouseOver.png'");
            btnLast.Attributes.Add("onmouseout", "this.src='Buttons/Last.png'");
            btnLast.Attributes.Add("onmouseover", "this.src='Buttons/LastMouseOver.png'");
            if (btnOK.ImageUrl == "~/Buttons/OK.png")
            {
                btnOK.Attributes.Add("onmouseout", "this.src='Buttons/OK.png'");
                btnOK.Attributes.Add("onmouseover", "this.src='Buttons/OKMouseOver.png'");
            }
            else
            {
                btnOK.Attributes.Add("onmouseout", "this.src='Buttons/Stop.png'");
                btnOK.Attributes.Add("onmouseover", "this.src='Buttons/StopMouseOver.png'");   
            }
            btnCancel.Attributes.Add("confirmmessage", ConfirmMessage);
            btnCancel.Attributes.Add("onmouseout", "this.src='Buttons/Cancel.png'");
            btnCancel.Attributes.Add("onmouseover", "this.src='Buttons/CancelMouseOver.png'");
        }
        public int CurrentPage
        {
            get
            {
                object o = ViewState["CurrentPage"];
                if (o == null)
                    return 0;
                else
                    return (int)o;
            }
            set
            {
                ViewState["CurrentPage"] = value;
            }
        }
        public bool Complete
        {
            get
            {
                bool res = true;
                foreach (WorkResultItem wri in WorkResult)
                {
                    if (wri.Answer == "")
                    {
                        res = false;
                        break;
                    }
                }
                return res;
            }
        }
        private void SetButtonsState()
        {
            if (CurrentPage != 0)
            {
                btnFirst.Enabled = true;
                btnFirst.ImageUrl = "~/Buttons/First.png";
                btnFirst.Attributes.Add("onmouseout", "this.src='Buttons/First.png'");
                btnFirst.Attributes.Add("onmouseover", "this.src='Buttons/FirstMouseOver.png'");
                btnPrev.Enabled = true;
                btnPrev.ImageUrl = "~/Buttons/Prev.png";
                btnPrev.Attributes.Add("onmouseout", "this.src='Buttons/Prev.png'");
                btnPrev.Attributes.Add("onmouseover", "this.src='Buttons/PrevMouseOver.png'");
            }
            else
            {
                btnFirst.Enabled = false;
                btnFirst.ImageUrl = "~/Buttons/FirstInactive.png";
                btnFirst.Attributes.Add("onmouseout", "");
                btnFirst.Attributes.Add("onmouseover", "");
                btnPrev.Enabled = false;
                btnPrev.ImageUrl = "~/Buttons/PrevInactive.png";
                btnPrev.Attributes.Add("onmouseout", "");
                btnPrev.Attributes.Add("onmouseover", "");
            }
            if (CurrentPage != (WorkResult.Count() - 1))
            {
                btnNext.Enabled = true;
                btnNext.ImageUrl = "~/Buttons/Next.png";
                btnNext.Attributes.Add("onmouseout", "this.src='Buttons/Next.png'");
                btnNext.Attributes.Add("onmouseover", "this.src='Buttons/NextMouseOver.png'");
                btnLast.Enabled = true;
                btnLast.ImageUrl = "~/Buttons/Last.png";
                btnLast.Attributes.Add("onmouseout", "this.src='Buttons/Last.png'");
                btnLast.Attributes.Add("onmouseover", "this.src='Buttons/LastMouseOver.png'");
            }
            else
            {
                btnNext.Enabled = false;
                btnNext.ImageUrl = "~/Buttons/NextInactive.png";
                btnNext.Attributes.Add("onmouseout", "");
                btnNext.Attributes.Add("onmouseover", "");
                btnLast.Enabled = false;
                btnLast.ImageUrl = "~/Buttons/LastInactive.png";
                btnLast.Attributes.Add("onmouseout", "");
                btnLast.Attributes.Add("onmouseover", "");
            }
            if (CurrWork.Continuous)
            {
                if (Complete)
                {
                    btnOK.Enabled = true;
                    btnOK.ImageUrl = "~/Buttons/OK.png";
                    btnOK.Attributes.Add("onmouseout", "this.src='Buttons/OK.png'");
                    btnOK.Attributes.Add("onmouseover", "this.src='Buttons/OKMouseOver.png'");   
                }
                else
                {
                    btnOK.Enabled = false;
                    btnOK.ImageUrl = "~/Buttons/OKInactive.png";
                    btnOK.Attributes.Add("onmouseout", "");
                    btnOK.Attributes.Add("onmouseover", "");   
                }
            }
            else
            {
                if (Complete)
                {
                    btnOK.ImageUrl = "~/Buttons/OK.png";
                    btnOK.Attributes.Add("onmouseout", "this.src='Buttons/OK.png'");
                    btnOK.Attributes.Add("onmouseover", "this.src='Buttons/OKMouseOver.png'");   
                }
                else
                {
                    btnOK.ImageUrl = "~/Buttons/Stop.png";
                    btnOK.Attributes.Add("onmouseout", "this.src='Buttons/Stop.png'");
                    btnOK.Attributes.Add("onmouseover", "this.src='Buttons/StopMouseOver.png'");   
                }
                btnOK.Enabled = true;
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            CurrentPage = 0;
            ViewStateToSession();
            Response.Redirect("~/DoWork.aspx");
        }
        protected void btnPrev_Click(object sender, EventArgs e)
        {
            CurrentPage--;
            ViewStateToSession();
            Response.Redirect("~/DoWork.aspx");
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            CurrentPage++;
            ViewStateToSession();
            Response.Redirect("~/DoWork.aspx");
        }
        protected void btnLast_Click(object sender, EventArgs e)
        {
            CurrentPage = WorkResult.Count() - 1;
            ViewStateToSession();
            Response.Redirect("~/DoWork.aspx");
        }
        protected void btnAnswer_Click(object sender, EventArgs e)
        {
            int num = int.Parse(((ImageButton)sender).CommandArgument) - 1;
            string sAnswer = ((TextBox)rptWorkItems.Items[0].FindControl("txtAnswer")).Text.Trim();
            if (sAnswer != "")
            {
                WorkResult[num].Answer = ((TextBox)rptWorkItems.Items[0].FindControl("txtAnswer")).Text;
                if (AccessDB.GetTestItem(WorkResult[num].TestItemID).Answer.Trim().ToUpper() ==
                     WorkResult[num].Answer.Trim().ToUpper())
                {
                    WorkResult[num].Correct = true;
                }
                else
                {
                    WorkResult[num].Correct = false;
                }
                WorkResult[num].AnswerTimeStamp = DateTime.Now;
                ViewState["WorkResult"] = WorkResult;
                if (Complete)
                {
                    DateTime dtNow = DateTime.Now;
                    dtNow = new DateTime(dtNow.Ticks - dtNow.Ticks % TimeSpan.TicksPerSecond);
                    CurrWork.EndDateTime = dtNow;
                    ViewState["CurrentWork"] = null;
                    AccessDB.SaveWorkResults(WorkResult, CurrWork.WorkID);
                    if (CurrWork.Continuous)
                    {
                        CurrWork.WorkStateID = 3;
                        SendMail();
                    }
                    else
                    {
                        if (Complete)
                        {
                            CurrWork.WorkStateID = 3;
                            SendMail();
                        }
                        else
                        {
                            CurrWork.WorkStateID = 2;
                        }
                    }
                    AccessDB.UpdateWork(CurrWork);
                    ViewState["CurrentWorkToShowResult"] = CurrWork;
                    ViewState["CurrentPage"] = null;
                    ViewStateToSession();
                    Response.Redirect("~/ShowWorkResult.aspx");
                }
                if (CurrentPage < WorkResult.Count() - 1)
                {
                    if (!CurrWork.ShowAnswer || WorkResult[num].Correct)
                    {
                        CurrentPage++;
                    }
                }
            }
            ViewStateToSession();
            Response.Redirect("~/DoWork.aspx");
        }
        private void SendMail()
        {
            string EMailAddr = AccessDB.GetUser(CurrWork.UserAssignerID).EMail;
            if (EMailAddr != "")
            {
                string SMTPServer = ConfigurationManager.AppSettings["MailSMTPServer"].ToString();
                string Username = ConfigurationManager.AppSettings["MailUsername"].ToString();
                string Password = ConfigurationManager.AppSettings["MailPassword"].ToString();
                int Port = int.Parse(ConfigurationManager.AppSettings["MailPort"].ToString());

                SmtpClient scl = new SmtpClient();
                scl.Credentials = new NetworkCredential(Username, Password);
                scl.Host = SMTPServer;
                scl.EnableSsl = true;
                scl.Port = Port;
                MailMessage mm = new MailMessage(Username, EMailAddr);
                mm.Subject = "Результаты тестирования " + AccessDB.GetUser(CurrWork.UserID).ToString();
                mm.IsBodyHtml = true;
                string sBody = "<h2>Результаты тестирования " + AccessDB.GetUser(CurrWork.UserID).ToString() + "</h2>";
                sBody += "Задание: " + AccessDB.GetTask(CurrWork.TaskID).ToString() + "<br/>";
                sBody += "Назначил: " + AccessDB.GetUser(CurrWork.UserAssignerID).ToString() + " " + CurrWork.AssignDate.ToString() + "<br/>";
                sBody += "Дата-время начала теста: " + CurrWork.BeginDateTime.ToString() + "<br />";
                sBody += "Дата-время окончания теста: " + CurrWork.EndDateTime.ToString() + "<br />";
                sBody += "Время тестирования: " + (CurrWork.EndDateTime - CurrWork.BeginDateTime).ToString() + "<br />";
                int AllCount = WorkResult.Count();
                int RightCount = (from wa in WorkResult where wa.Correct select 1).Count();
                int RightPersent;
                if (AllCount > 0)
                {
                    RightPersent = RightCount * 100 / AllCount;
                }
                else
                {
                    RightPersent = 0;
                }
                
                sBody += "Правильных ответов: " + RightCount.ToString() + " из " + AllCount.ToString() + " (" +
                    RightPersent.ToString() + "%)<br/><br/>";
                sBody += "<h3>Подробно:</h3><table border='1'>";
                sBody += "<tr><th>№</th>";
                sBody += "<th>Задание</th>";
                sBody += "<th>Правильный<br/>ответ</th>";
                sBody += "<th>Данный<br/>ответ</th>";
                sBody += "<th>Верно</th>";
                sBody += "<th>Дата-время ответа</th></tr>";

                for (int i = 0; i < WorkResult.Count(); i++)
                {
                    sBody += "<tr><td>" + WorkResult[i].OrderNumber.ToString() + "</td>";
                    sBody += "<td>" + AccessDB.GetTestItem(WorkResult[i].TestItemID).ToString() + "</td>";
                    sBody += "<td>" + WorkResult[i].Answer + "</td>";
                    sBody += "<td>" + WorkResult[i].CorrectAnswer + "</td>";
                    sBody += "<td>" + (WorkResult[i].Correct ? "Да" : "Нет") + "</td>";
                    sBody += "<td>" + WorkResult[i].AnswerTimeStamp.ToString() + "</td></tr>";
                }
                sBody += "</table>";
                mm.Body = sBody;
                string sAttach = "";
                sAttach += "<html><head>";
                sAttach += "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>";
                sAttach += "<title>Задания</title></head><body><table border='1'>";
                sAttach += "<tr><th width='60px'>№</th><th width='200px'>Задание</th><th width='1000px'>Вопрос</th></tr>";
                for (int i = 0; i < WorkResult.Count(); i++)
                {
                    TestItem ti = AccessDB.GetTestItem(WorkResult[i].TestItemID);

                    sAttach += "<tr><td style='text-align:center'>" + (i + 1).ToString() + "</td>";
                    sAttach += "<td style='text-align:center'>";
                    sAttach += "Тема: " + AccessDB.GetTest(ti.TestID).ToString() + "<br />";
                    sAttach += ti.ToString() + "</td>";
                    sAttach += "<td>" + ti.QuestionHtml + "</td></tr>";
                }
                sAttach += "</table></body></html>";

                Attachment att = Attachment.CreateAttachmentFromString(sAttach, "Задание.html", System.Text.Encoding.UTF8, null);
                mm.Attachments.Add(att);
                scl.Send(mm);
            }
        }
        protected void btnOK_Click(object sender, EventArgs e)
        {
            AccessDB.SaveWorkResults(WorkResult, CurrWork.WorkID);
            if (CurrWork.Continuous)
            {
                CurrWork.WorkStateID = 3;
                SendMail();
            }
            else
            {
                if (Complete)
                {
                    CurrWork.WorkStateID = 3;
                    SendMail();
                }
                else
                {
                    CurrWork.WorkStateID = 2;
                }
            }
            AccessDB.UpdateWork(CurrWork);
            ViewState["CurrentPage"] = null;
            ViewStateToSession();
            Response.Redirect("~/MyWork.aspx");
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                ViewState["CurrentPage"] = null;
                ViewStateToSession();
                Response.Redirect("~/MyWork.aspx");
            }
        }
        private void SessionToViewState()
        {
            for (int i = 0; i < Session.Count; i++)
            {
                ViewState[Session.Keys[i].ToString()] = Session[Session.Keys[i].ToString()];
            }
        }
        private void ViewStateToSession()
        {
            string[] Keys = new string[ViewState.Count];
            ViewState.Keys.CopyTo(Keys, 0);
            for (int i = 0; i < Keys.Count(); i++)
            {
                Session[Keys[i]] = ViewState[Keys[i]];
            }
        }
    }
}