﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace Tester
{
    public partial class TestDetail : System.Web.UI.Page
    {
        TestItem[] CurrTestItems;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SessionToViewState();
            }
            if (ViewState["Test"] != null)
            {
                Test CurTest = (Test)ViewState["Test"];
                litTitle.Text = CurTest.Theme;
                lblTheme.Text = "Тема: " + CurTest.Theme;

                CurrTestItems = AccessDB.GetTestItems(CurTest.TestID);
                if (!IsPostBack)
                {
                    lblTestItemCount.Text = "Количество заданий: " + CurrTestItems.Count().ToString();
                    SetState();

                    PagedDataSource pDS = new PagedDataSource();
                    pDS.DataSource = CurrTestItems;
                    pDS.AllowPaging = true;
                    pDS.PageSize = 1;
                    pDS.CurrentPageIndex = CurrentPage;
                    rptTestList.DataSource = pDS;
                    rptTestList.DataBind();
                }
                SetButtons();
            }
        }
        private void SetButtons()
        {
            btnFirst.Attributes.Add("onmouseout", "this.src='Buttons/First.png'");
            btnFirst.Attributes.Add("onmouseover", "this.src='Buttons/FirstMouseOver.png'");
            btnPrev.Attributes.Add("onmouseout", "this.src='Buttons/Prev.png'");
            btnPrev.Attributes.Add("onmouseover", "this.src='Buttons/PrevMouseOver.png'");
            btnNext.Attributes.Add("onmouseout", "this.src='Buttons/Next.png'");
            btnNext.Attributes.Add("onmouseover", "this.src='Buttons/NextMouseOver.png'");
            btnLast.Attributes.Add("onmouseout", "this.src='Buttons/Last.png'");
            btnLast.Attributes.Add("onmouseover", "this.src='Buttons/LastMouseOver.png'");
            btnBack.Attributes.Add("onmouseout", "this.src='Buttons/Back.png'");
            btnBack.Attributes.Add("onmouseover", "this.src='Buttons/BackMouseOver.png'");
        }
        public int CurrentPage
        {
            get
            {
                object o = ViewState["CurrentPage"];
                if (o == null)
                    return 0;
                else
                    return (int)o;
            }
            set
            {
                ViewState["CurrentPage"] = value;
            }
        }
        private void SetState()
        {
            if (CurrentPage != 0)
            {
                btnFirst.Enabled = true;
                btnFirst.ImageUrl = "~/Buttons/First.png";
                btnFirst.Attributes.Add("onmouseout", "this.src='Buttons/First.png'");
                btnFirst.Attributes.Add("onmouseover", "this.src='Buttons/FirstMouseOver.png'");
                btnPrev.Enabled = true;
                btnPrev.ImageUrl = "~/Buttons/Prev.png";
                btnPrev.Attributes.Add("onmouseout", "this.src='Buttons/Prev.png'");
                btnPrev.Attributes.Add("onmouseover", "this.src='Buttons/PrevMouseOver.png'");
            }
            else
            {
                btnFirst.Enabled = false;
                btnFirst.ImageUrl = "~/Buttons/FirstInactive.png";
                btnFirst.Attributes.Add("onmouseout", "");
                btnFirst.Attributes.Add("onmouseover", "");
                btnPrev.Enabled = false;
                btnPrev.ImageUrl = "~/Buttons/PrevInactive.png";
                btnPrev.Attributes.Add("onmouseout", "");
                btnPrev.Attributes.Add("onmouseover", "");
            }
            if (CurrentPage != (CurrTestItems.Count() - 1))
            {
                btnNext.Enabled = true;
                btnNext.ImageUrl = "~/Buttons/Next.png";
                btnNext.Attributes.Add("onmouseout", "this.src='Buttons/Next.png'");
                btnNext.Attributes.Add("onmouseover", "this.src='Buttons/NextMouseOver.png'");
                btnLast.Enabled = true;
                btnLast.ImageUrl = "~/Buttons/Last.png";
                btnLast.Attributes.Add("onmouseout", "this.src='Buttons/Last.png'");
                btnLast.Attributes.Add("onmouseover", "this.src='Buttons/LastMouseOver.png'");
            }
            else
            {
                btnNext.Enabled = false;
                btnNext.ImageUrl = "~/Buttons/NextInactive.png";
                btnNext.Attributes.Add("onmouseout", "");
                btnNext.Attributes.Add("onmouseover", "");
                btnLast.Enabled = false;
                btnLast.ImageUrl = "~/Buttons/LastInactive.png";
                btnLast.Attributes.Add("onmouseout", "");
                btnLast.Attributes.Add("onmouseover", "");
            }
            ddlTaskNo.Items.Clear();
            for (int i = 1; i <= CurrTestItems.Count();i++ )
            {
                ddlTaskNo.Items.Add(i.ToString());
            }
            ddlTaskNo.SelectedIndex = CurrentPage;
            lblTestItemCount2.Text = " из " + CurrTestItems.Count();
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            ViewState["CurrentPage"] = null;
            ViewStateToSession();
            Response.Redirect("~/Tests.aspx");
        }

        protected void btnFirst_Click(object sender, EventArgs e)
        {
            CurrentPage = 0;
            ViewStateToSession();
            Response.Redirect("~/TestDetail.aspx");
        }

        protected void btnPrev_Click(object sender, EventArgs e)
        {
            CurrentPage--;
            ViewStateToSession();
            Response.Redirect("~/TestDetail.aspx");
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            CurrentPage++;
            ViewStateToSession();
            Response.Redirect("~/TestDetail.aspx");
        }

        protected void btnLast_Click(object sender, EventArgs e)
        {
            CurrentPage = CurrTestItems.Count() - 1;
            ViewStateToSession();
            Response.Redirect("~/TestDetail.aspx");
        }

        protected void ddlTaskNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            CurrentPage = ddlTaskNo.SelectedIndex;
            ViewStateToSession();
            Response.Redirect("~/TestDetail.aspx");
        }
        private void SessionToViewState()
        {
            for (int i = 0; i < Session.Count; i++)
            {
                ViewState[Session.Keys[i].ToString()] = Session[Session.Keys[i].ToString()];
            }
        }
        private void ViewStateToSession()
        {
            string[] Keys = new string[ViewState.Count];
            ViewState.Keys.CopyTo(Keys, 0);
            for (int i = 0; i < Keys.Count(); i++)
            {
                Session[Keys[i]] = ViewState[Keys[i]];
            }
        }
    }
}