﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="EditTask.aspx.cs" Inherits="Tester.EditTask" %>

<asp:Content ID="contTitle" ContentPlaceHolderID="contTitle" runat="server">
    <asp:Literal ID="litTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="contScript" ContentPlaceHolderID="contScript" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('#dlgQuestion').first().html().trim().length !== 0) {
                $('#dlgQuestion').dialog({
                    buttons: [{
                        text: "OK", click: function () {
                            $('#dlgQuestion').text("");
                            $(this).dialog("close");
                        }
                    }],
                    width: 1000,
                    modal: true
                })
            }
        });
        function MinusAllMouseOver(el) {
            $(el).attr("src", "Buttons/MinusMouseOver.png");
        }
        function MinusAllMouseOut(el) {
            $(el).attr("src", "Buttons/Minus.png");
        }
        function PlusAllMouseOver(el) {
            $(el).attr("src", "Buttons/PlusMouseOver.png");
        }
        function PlusAllMouseOut(el) {
            $(el).attr("src", "Buttons/Plus.png");
        }
        function MinusMouseOver(el) {
            ItemCount = Number($(el).attr("ItemCount"));
            if (ItemCount > 0) {
                $(el).attr("src", "Buttons/MinusMouseOver.png");
            }
            else {
                $(el).attr("src", "Buttons/MinusInactive.png");
            }
        }
        function MinusMouseOut(el) {
            ItemCount = Number($(el).attr("ItemCount"));
            if (ItemCount > 0) {
                $(el).attr("src", "Buttons/Minus.png");
            }
            else {
                $(el).attr("src", "Buttons/MinusInactive.png");
            }
        }
        function PlusMouseOver(el) {
            ItemCount = Number($(el).attr("ItemCount"));
            AllItemCount = Number($(el).attr("AllItemCount"));
            if (ItemCount < AllItemCount) {

                $(el).attr("src", "Buttons/PlusMouseOver.png");
            }
            else {
                $(el).attr("src", "Buttons/PlusInactive.png");
            }
        }
        function PlusMouseOut(el) {
            ItemCount = Number($(el).attr("ItemCount"));
            AllItemCount = Number($(el).attr("AllItemCount"));
            if (ItemCount < AllItemCount) {
                $(el).attr("src", "Buttons/Plus.png");
            }
            else {
                $(el).attr("src", "Buttons/PlusInactive.png");
            }
        }
        function postBackByObject() {
            var o = window.event.srcElement;
            if (o.tagName == "INPUT" && o.type == "checkbox") {
                __doPostBack("", "");
            }
        }
        function Confirm(el) {
            Message = $(el).attr("confirmmessage");
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm(Message)) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>
<asp:Content ID="contMenu" ContentPlaceHolderID="contMenu" runat="server">
</asp:Content>
<asp:Content ID="contMain" ContentPlaceHolderID="contMain" runat="server">
    <table>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Название задания:"
                    ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
                <asp:TextBox ID="txtName" runat="server"
                    BackColor="#58FAF4" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px" BorderColor="#58FAF4"></asp:TextBox>
                <br />
                <asp:Label ID="Label4" runat="server" Text="Создатель:"
                    ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
                <asp:Label ID="lblCreator" runat="server" Text=""
                    ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
                <br />
                <asp:Label ID="Label5" runat="server" Text="Дата-время создания:"
                    ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
                <asp:Label ID="lblCreateDate" runat="server" Text=""
                    ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
                <br />
                <asp:Label ID="Label9" runat="server" Text="Дата-время последнего изменения:"
                    ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
                <asp:Label ID="lblLastModifyDate" runat="server" Text=""
                    ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
            </td>
            <td>
                <div id="dlgQuestion" title="Вопрос">
                    <asp:Literal ID="ltrlQuestion" runat="server"></asp:Literal>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:ImageButton ID="btnOK" runat="server" OnClick="btnOK_Click" ImageUrl="~/Buttons/OK.png" />
                <asp:ImageButton ID="btnCancel" runat="server" OnClick="btnCancel_Click" ImageUrl="~/Buttons/Cancel.png"
                    OnClientClick="Confirm(this);" />
            </td>
        </tr>
    </table>
    <hr />
    <asp:Label ID="Label7" runat="server" Text="Добавление тестов"
        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Repeater ID="rptTests" runat="server">
        <HeaderTemplate>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="Label8" runat="server" Text="Все темы" ForeColor="#0B0B3B"
                            Font-Bold="True" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    </td>
                    <td></td>
                    <td>
                        <asp:ImageButton ID="btnMinusAll" runat="server"
                            OnClick="btnMinusAll_Click"
                            ImageUrl="~/Buttons/Minus.png"
                            onmouseover="MinusAllMouseOver(this);"
                            onmouseout="MinusAllMouseOut(this);" />
                        <asp:ImageButton ID="btnPlusAll" runat="server"
                            OnClick="btnPlusAll_Click"
                            ImageUrl="~/Buttons/Plus.png"
                            onmouseover="PlusAllMouseOver(this);"
                            onmouseout="PlusAllMouseOut(this);" />
                    </td>
                    <td></td>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:Label ID="Label8" runat="server" Text="Тема:" ForeColor="#0B0B3B"
                        Font-Bold="True" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    <asp:Label ID="lblTheme" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Theme")  %>'
                        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"></asp:Label>
                </td>
                <td>
                    <asp:HiddenField ID="hfNum" runat="server" Value="<%# Container.ItemIndex %>" />
                    <asp:DropDownList ID="ddlTestItemCount" runat="server" AutoPostBack="True" Width="50px"
                        OnSelectedIndexChanged="ddlTestItemCount_SelectedIndexChanged"
                        DataSource='<%# DataBinder.Eval(Container.DataItem, "ItemsForList") %>'
                        BackColor="#58FAF4" ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px" BorderColor="#58FAF4">
                    </asp:DropDownList>

                </td>
                <td>
                    <asp:HiddenField ID="hdnTestID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TestID")  %>' />
                    <asp:ImageButton ID="btnMinusCount" runat="server"
                        CommandArgument="<%# Container.ItemIndex %>"
                        OnClick="btnMinusCount_Click"
                        Enabled='<%# int.Parse(DataBinder.Eval(Container.DataItem, "ItemCount").ToString())>0 %>'
                        ItemCount='<%# DataBinder.Eval(Container.DataItem, "ItemCount") %>'
                        ImageUrl='<%# int.Parse(DataBinder.Eval(Container.DataItem, "ItemCount").ToString())>0?"~/Buttons/Minus.png":"~/Buttons/MinusInactive.png" %>'
                        onmouseover="MinusMouseOver(this);"
                        onmouseout="MinusMouseOut(this);" />
                    <asp:ImageButton ID="btnPlusCount" runat="server"
                        CommandArgument="<%# Container.ItemIndex %>"
                        OnClick="btnPlusCount_Click"
                        Enabled='<%# int.Parse(DataBinder.Eval(Container.DataItem, "ItemCount").ToString())<int.Parse(DataBinder.Eval(Container.DataItem, "AllItemCount").ToString()) %>'
                        ItemCount='<%# DataBinder.Eval(Container.DataItem, "ItemCount") %>'
                        AllItemCount='<%# DataBinder.Eval(Container.DataItem, "AllItemCount") %>'
                        ImageUrl='<%# int.Parse(DataBinder.Eval(Container.DataItem, "ItemCount").ToString())<int.Parse(DataBinder.Eval(Container.DataItem, "AllItemCount").ToString())?"~/Buttons/Plus.png":"~/Buttons/PlusInactive.png" %>'
                        onmouseover="PlusMouseOver(this);"
                        onmouseout="PlusMouseOut(this);" />
                </td>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="заданий из " ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    <asp:Label ID="lblAllCount" runat="server"
                        Text='<%# DataBinder.Eval(Container.DataItem, "AllItemCount") %>'
                        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"></asp:Label>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <hr />
    <table>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Выбранные тесты:" ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
                <br />
                <br />
                <asp:TreeView ID="trvwTaskItems" runat="server" OnSelectedNodeChanged="trvwTaskItems_SelectedNodeChanged"
                    BackColor="#2E9AFE" BorderColor="#2E9AFE" Font-Names="Arial" Font-Bold="True" Font-Size="14px"
                    ForeColor="#0B0B3B" Font-Overline="False" HoverNodeStyle-BackColor="#2E9AFE" HoverNodeStyle-BorderColor="#2E9AFE"
                    HoverNodeStyle-ForeColor="#0B0B3B" LeafNodeStyle-BackColor="#2E9AFE" LeafNodeStyle-BorderColor="#2E9AFE"
                    LeafNodeStyle-ForeColor="#0B0B3B" NodeStyle-BackColor="#2E9AFE" NodeStyle-BorderColor="#2E9AFE"
                    NodeStyle-ForeColor="#0B0B3B" ParentNodeStyle-BackColor="#2E9AFE" ParentNodeStyle-BorderColor="#2E9AFE"
                    ParentNodeStyle-ForeColor="#0B0B3B" RootNodeStyle-BackColor="#2E9AFE" RootNodeStyle-BorderColor="#2E9AFE"
                    RootNodeStyle-ForeColor="#0B0B3B" SelectedNodeStyle-BackColor="#2E9AFE" SelectedNodeStyle-BorderColor="#2E9AFE"
                    SelectedNodeStyle-ForeColor="#0B0B3B" ShowCheckBoxes="All"
                    OnTreeNodeCheckChanged="trvwTaskItems_TreeNodeCheckChanged" OnTreeNodeCollapsed="trvwTaskItems_TreeNodeCollapsed"
                    OnTreeNodeExpanded="trvwTaskItems_TreeNodeExpanded">
                </asp:TreeView>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="contButtons" ContentPlaceHolderID="contButtons" runat="server">
</asp:Content>
