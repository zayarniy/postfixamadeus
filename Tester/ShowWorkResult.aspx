﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ShowWorkResult.aspx.cs" Inherits="Tester.ShowWorkResult" %>

<asp:Content ID="contTitle" ContentPlaceHolderID="contTitle" runat="server">
    <asp:Literal ID="litTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="contScript" ContentPlaceHolderID="contScript" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('#dlgQuestion').first().html().trim().length !== 0) {
                $('#dlgQuestion').dialog({
                    buttons: [{
                        text: "OK", click: function () {
                            $('#dlgQuestion').text("");
                            $(this).dialog("close");
                        }
                    }],
                    width: 1000,
                    modal: true
                })
            }
        });
    </script>
</asp:Content>
<asp:Content ID="contMenu" ContentPlaceHolderID="contMenu" runat="server">
</asp:Content>
<asp:Content ID="contMain" ContentPlaceHolderID="contMain" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Задание:"
        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="lblTaskName" runat="server" Text=""
        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <br />
    <br />
    <asp:Label ID="Label8" runat="server" Text="Назначеное пользователем:"
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="lblUserAssignerName" runat="server" Text=""
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <br />
    <asp:Label ID="Label2" runat="server" Text="Назначеное пользователю:"
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="lblUserName" runat="server" Text=""
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <br />
    <asp:Label ID="Label3" runat="server" Text="Дата-время назначения:"
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="lblAssignDate" runat="server" Text=""
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <br />
    <asp:Label ID="Label7" runat="server" Text="Состояние:"
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="lblWorkState" runat="server" Text=""
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <br />
    <asp:Label ID="Label17" runat="server" Text="Дата-время начала выполнения:"
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="lblBeginDateTime" runat="server" Text=""
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <br />
    <asp:Label ID="Label18" runat="server" Text="Дата-время окончания выполнения:"
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="lblEndDateTime" runat="server" Text=""
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <br />
    <asp:Label ID="Label19" runat="server" Text="Время тестирования:"
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="lblTestDuration" runat="server" Text=""
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <br />
    <asp:Label ID="Label4" runat="server" Text="Отвечено вопросов:"
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="lblProgress" runat="server" Text=""
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <br />
    <asp:Label ID="Label5" runat="server" Text="Правильных ответов:"
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Label ID="lblRight" runat="server" Text=""
        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <br />
    <br />
    <asp:ImageButton ID="btnDeleteResult" runat="server" OnClick="btnDeleteResult_Click" ImageUrl="~/Buttons/DeleteResult.png" />
    <hr />
    <asp:Label ID="Label6" runat="server" Text="Подробно"
        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:Repeater ID="rptWorkAnswer" runat="server">
        <HeaderTemplate>
            <table border="1">
                <tr>
                    <th>
                        <asp:LinkButton ID="btnOrderNumberSort" runat="server" Text="№"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnOrderNumberSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnTaskNameSort" runat="server" Text="Задание"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnTaskNameSort_Click"></asp:LinkButton>
                    </th>
                    <th runat="server" id="thCorrectAnswer">
                        <asp:LinkButton ID="btnCorrectAnswerSort" runat="server" Text="Правильный"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnCorrectAnswerSort_Click"></asp:LinkButton>
                        <br />
                        <asp:LinkButton ID="btnCorrectAnswerSort2" runat="server" Text="ответ"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnCorrectAnswerSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnGivenAnswerSort" runat="server" Text="Данный"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnGivenAnswerSort_Click"></asp:LinkButton>
                        <br />
                        <asp:LinkButton ID="btnGivenAnswerSort2" runat="server" Text="ответ"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnGivenAnswerSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnCorrectSort" runat="server" Text="Верно?"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnCorrectSort_Click"></asp:LinkButton>
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:Label ID="lblOrderNumber" runat="server"
                        Text='<%# DataBinder.Eval(Container.DataItem, "OrderNumber")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                </td>
                <td>
                    <asp:LinkButton ID="btnTestItem" runat="server"
                        Text='<%# DataBinder.Eval(Container.DataItem, "TestTheme")+"<br />"+DataBinder.Eval(Container.DataItem, "TestItemName")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"
                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TestItemID")  %>'
                        OnClick="btnTestItem_Click"></asp:LinkButton>
                </td>
                <td runat="server" id="tdCorrectAnswer">
                    <asp:Label ID="lblCorrectAnswer" runat="server"
                        Text='<%# DataBinder.Eval(Container.DataItem, "CorrectAnswer")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblGivenAnswer" runat="server"
                        Text='<%# DataBinder.Eval(Container.DataItem, "GivenAnswer")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                </td>
                <td style="text-align: center">
                    <asp:Image ID="imgCorrect" runat="server"
                        ImageUrl='<%# ((bool)DataBinder.Eval(Container.DataItem, "Correct"))?"~/CheckBoxImage/Checked.png":"~/CheckBoxImage/UnChecked.png"  %>' />
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <div id="dlgQuestion" title="Вопрос">
        <asp:Literal ID="ltrlQuestion" runat="server"></asp:Literal>
    </div>
</asp:Content>
<asp:Content ID="contButtons" ContentPlaceHolderID="contButtons" runat="server">
    <hr />
    <asp:ImageButton ID="btnBack" runat="server" OnClick="btnBack_Click" ImageUrl="~/Buttons/Back.png" />
</asp:Content>

