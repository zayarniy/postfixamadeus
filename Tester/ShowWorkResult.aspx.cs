﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Tester
{
    [Serializable]
    public class WorkAnswer
    {
        public int OrderNumber
        {
            get;
            set;
        }
        public int TestItemID
        {
            get;
            set;
        }
        public string TestTheme
        {
            get;
            set;
        }
        public string TestItemName
        {
            get;
            set;
        }
        public string CorrectAnswer
        {
            get;
            set;
        }
        public string GivenAnswer
        {
            get;
            set;
        }
        public bool Correct
        {
            get
            {
                if (GivenAnswer.Trim() != "")
                {
                    return CorrectAnswer.Trim().ToUpper() == GivenAnswer.Trim().ToUpper();
                }
                else
                {
                    return false;
                }
            }
        }
        public string AnswerTimeStamp
        {
            get;
            set;
        }
    }
    public partial class ShowWorkResult : System.Web.UI.Page
    {
        Work CurrentWork;
        WorkAnswer[] WorkAnswerArray;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SessionToViewState();
            }
            if (ViewState["CurrentWorkToShowResult"] != null)
            {
                CurrentWork = (Work)ViewState["CurrentWorkToShowResult"];
                string sTaskName = AccessDB.GetTask(CurrentWork.TaskID).ToString();
                litTitle.Text = "Результаты " + sTaskName;
                lblTaskName.Text = sTaskName;
                lblUserAssignerName.Text = AccessDB.GetUser(CurrentWork.UserAssignerID).ToString();
                lblUserName.Text = AccessDB.GetUser(CurrentWork.UserID).ToString();
                lblAssignDate.Text = CurrentWork.AssignDate.ToString();
                lblWorkState.Text = CurrentWork.WorkState;
                lblBeginDateTime.Text = CurrentWork.BeginDateTime.ToString();
                if (CurrentWork.EndDateTime!=null)
                {
                    lblEndDateTime.Text = CurrentWork.EndDateTime.ToString();
                    lblTestDuration.Text = (CurrentWork.EndDateTime - CurrentWork.BeginDateTime).ToString();
                }
                WorkAnswerArray = GetWorkAnswer(CurrentWork.WorkID);
                int AllCount = WorkAnswerArray.Count();
                int AnsweredCount = (from wa in WorkAnswerArray where wa.GivenAnswer.Trim() != "" select 1).Count();
                int RightCount = (from wa in WorkAnswerArray where wa.Correct select 1).Count();
                int AnsweredPersent;
                if (AllCount > 0)
                {
                    AnsweredPersent = AnsweredCount * 100 / AllCount;
                }
                else
                {
                    AnsweredPersent = 0;
                }
                int RightPersent;
                if (AnsweredCount > 0)
                {
                    RightPersent = RightCount * 100 / AnsweredCount;
                }
                else
                {
                    RightPersent = 0;
                }
                lblProgress.Text = AnsweredCount.ToString() + " из " + AllCount.ToString() + " (" + AnsweredPersent.ToString() + "%)";
                lblRight.Text = RightCount.ToString() + " из " + AnsweredCount.ToString() + " (" + RightPersent.ToString() + "%)";
                rptWorkAnswer.DataSource = WorkAnswerArray;
                OrderWorkShowResults(WorkAnswerArray);
                rptWorkAnswer.DataBind();
                if (ViewState["CurUser"] != null)
                {
                    UserDB usr = (UserDB)ViewState["CurUser"];
                    if (!usr.IsAdmin)
                    {
                        btnDeleteResult.Visible = false;
                        if (CurrentWork.WorkStateID != 3 || !CurrentWork.ShowAnswer)
                        {
                            rptWorkAnswer.Controls[0].FindControl("thCorrectAnswer").Visible = false;
                            foreach (RepeaterItem ri in rptWorkAnswer.Items)
                            {
                                ri.FindControl("tdCorrectAnswer").Visible = false;
                            }
                        }
                    }
                }
            }
            SetButtons();
        }
        private void SetButtons()
        {
            btnBack.Attributes.Add("onmouseout", "this.src='Buttons/Back.png'");
            btnBack.Attributes.Add("onmouseover", "this.src='Buttons/BackMouseOver.png'");
            btnDeleteResult.Attributes.Add("onmouseout", "this.src='Buttons/DeleteResult.png'");
            btnDeleteResult.Attributes.Add("onmouseover", "this.src='Buttons/DeleteResultMouseOver.png'");
        }
        private int CompareWorkAnswer(WorkAnswer wa1, WorkAnswer wa2)
        {
            if (ShowWorkResOrderNumberSort == "ASC")
            {
                return wa1.OrderNumber.CompareTo(wa2.OrderNumber);
            }
            if (ShowWorkResOrderNumberSort == "DESC")
            {
                return wa2.OrderNumber.CompareTo(wa1.OrderNumber);
            }
            if (ShowWorkResTestItemNameSort == "ASC")
            {
                return (wa1.TestTheme + wa1.TestItemName).CompareTo(wa2.TestTheme + wa2.TestItemName);
            }
            if (ShowWorkResTestItemNameSort == "DESC")
            {
                return (wa2.TestTheme + wa2.TestItemName).CompareTo(wa1.TestTheme + wa1.TestItemName);
            }
            if (ShowWorkResCorrectAnswerSort == "ASC")
            {
                return wa1.CorrectAnswer.CompareTo(wa2.CorrectAnswer);
            }
            if (ShowWorkResCorrectAnswerSort == "DESC")
            {
                return wa2.CorrectAnswer.CompareTo(wa1.CorrectAnswer);
            }
            if (ShowWorkResGivenAnswerSort == "ASC")
            {
                return wa2.GivenAnswer.CompareTo(wa1.GivenAnswer);
            }
            if (ShowWorkResGivenAnswerSort == "DESC")
            {
                return wa2.GivenAnswer.CompareTo(wa1.GivenAnswer);
            }
            if (ShowWorkResCorrectSort == "ASC")
            {
                return wa1.Correct.CompareTo(wa2.Correct);
            }
            if (ShowWorkResCorrectSort == "DESC")
            {
                return wa2.Correct.CompareTo(wa1.Correct);
            }
            return 0;
        }
        private void OrderWorkShowResults(WorkAnswer[] WorkAnswers)
        {
            if (ShowWorkResOrderNumberSort != "" || ShowWorkResTestItemNameSort != "" || ShowWorkResCorrectAnswerSort != "" ||
                ShowWorkResGivenAnswerSort != "" || ShowWorkResCorrectSort != "")
            {
                Array.Sort(WorkAnswers, CompareWorkAnswer);
            }
        }
        private string NextSortDir(string SortDir)
        {
            switch (SortDir)
            {
                case "":
                    return "ASC";
                case "ASC":
                    return "DESC";
                case "DESC":
                    return "";
                default:
                    return "";
            }
        }
        private string ShowWorkResOrderNumberSort
        {
            get
            {
                if (ViewState["ShowWorkResOrderNumberSort"] != null)
                {
                    return (string)ViewState["ShowWorkResOrderNumberSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["ShowWorkResOrderNumberSort"] = value;
            }
        }
        private string ShowWorkResTestItemNameSort
        {
            get
            {
                if (ViewState["ShowWorkResTestItemNameSort"] != null)
                {
                    return (string)ViewState["ShowWorkResTestItemNameSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["ShowWorkResTestItemNameSort"] = value;
            }
        }
        private string ShowWorkResCorrectAnswerSort
        {
            get
            {
                if (ViewState["ShowWorkResCorrectAnswerSort"] != null)
                {
                    return (string)ViewState["ShowWorkResCorrectAnswerSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["ShowWorkResCorrectAnswerSort"] = value;
            }
        }
        private string ShowWorkResGivenAnswerSort
        {
            get
            {
                if (ViewState["ShowWorkResGivenAnswerSort"] != null)
                {
                    return (string)ViewState["ShowWorkResGivenAnswerSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["ShowWorkResGivenAnswerSort"] = value;
            }
        }
        private string ShowWorkResCorrectSort
        {
            get
            {
                if (ViewState["ShowWorkResCorrectSort"] != null)
                {
                    return (string)ViewState["ShowWorkResCorrectSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["ShowWorkResCorrectSort"] = value;
            }
        }
        protected void btnOrderNumberSort_Click(object sender, EventArgs e)
        {
            if (ShowWorkResTestItemNameSort != "" || ShowWorkResCorrectAnswerSort != "" ||
                ShowWorkResGivenAnswerSort != "" || ShowWorkResCorrectSort != "")
            {
                ShowWorkResOrderNumberSort = "ASC";
                ShowWorkResTestItemNameSort = "";
                ShowWorkResCorrectAnswerSort = "";
                ShowWorkResGivenAnswerSort = "";
                ShowWorkResCorrectSort = "";
            }
            else
            {
                ShowWorkResOrderNumberSort = NextSortDir(ShowWorkResOrderNumberSort);
            }
            ViewStateToSession();
            Response.Redirect("~/ShowWorkResult.aspx");
        }
        protected void btnTaskNameSort_Click(object sender, EventArgs e)
        {
            if (ShowWorkResOrderNumberSort != "" || ShowWorkResCorrectAnswerSort != "" ||
                ShowWorkResGivenAnswerSort != "" || ShowWorkResCorrectSort != "")
            {
                ShowWorkResOrderNumberSort = "";
                ShowWorkResTestItemNameSort = "ASC";
                ShowWorkResCorrectAnswerSort = "";
                ShowWorkResGivenAnswerSort = "";
                ShowWorkResCorrectSort = "";
            }
            else
            {
                ShowWorkResTestItemNameSort = NextSortDir(ShowWorkResTestItemNameSort);
            }
            ViewStateToSession();
            Response.Redirect("~/ShowWorkResult.aspx");
        }
        protected void btnCorrectAnswerSort_Click(object sender, EventArgs e)
        {
            if (ShowWorkResOrderNumberSort != "" || ShowWorkResTestItemNameSort != "" ||
                ShowWorkResGivenAnswerSort != "" || ShowWorkResCorrectSort != "")
            {
                ShowWorkResOrderNumberSort = "";
                ShowWorkResTestItemNameSort = "";
                ShowWorkResCorrectAnswerSort = "ASC";
                ShowWorkResGivenAnswerSort = "";
                ShowWorkResCorrectSort = "";
            }
            else
            {
                ShowWorkResCorrectAnswerSort = NextSortDir(ShowWorkResCorrectAnswerSort);
            }
            ViewStateToSession();
            Response.Redirect("~/ShowWorkResult.aspx");
        }
        protected void btnGivenAnswerSort_Click(object sender, EventArgs e)
        {
            if (ShowWorkResOrderNumberSort != "" || ShowWorkResTestItemNameSort != "" || ShowWorkResCorrectAnswerSort != "" ||
                ShowWorkResCorrectSort != "")
            {
                ShowWorkResOrderNumberSort = "";
                ShowWorkResTestItemNameSort = "";
                ShowWorkResCorrectAnswerSort = "";
                ShowWorkResGivenAnswerSort = "ASC";
                ShowWorkResCorrectSort = "";
            }
            else
            {
                ShowWorkResGivenAnswerSort = NextSortDir(ShowWorkResGivenAnswerSort);
            }
            ViewStateToSession();
            Response.Redirect("~/ShowWorkResult.aspx");
        }
        protected void btnCorrectSort_Click(object sender, EventArgs e)
        {
            if (ShowWorkResOrderNumberSort != "" || ShowWorkResTestItemNameSort != "" || ShowWorkResCorrectAnswerSort != "" ||
                ShowWorkResGivenAnswerSort != "")
            {
                ShowWorkResOrderNumberSort = "";
                ShowWorkResTestItemNameSort = "";
                ShowWorkResCorrectAnswerSort = "";
                ShowWorkResGivenAnswerSort = "";
                ShowWorkResCorrectSort = "ASC";
            }
            else
            {
                ShowWorkResCorrectSort = NextSortDir(ShowWorkResCorrectSort);
            }
            ViewStateToSession();
            Response.Redirect("~/ShowWorkResult.aspx");
        }
        private WorkAnswer[] GetWorkAnswer(int WorkID)
        {
            WorkResult[] wrs = AccessDB.GetWorkResults(WorkID);
            List<WorkAnswer> res = new List<WorkAnswer>();
            foreach(WorkResult wr in wrs)
            {
                WorkAnswer wa = new WorkAnswer();
                TestItem ti = AccessDB.GetTestItem(wr.TestItemID);
                wa.OrderNumber = wr.OrderNumber;
                wa.TestItemID = wr.TestItemID;
                wa.TestTheme = AccessDB.GetTest(ti.TestID).Theme;
                wa.TestItemName = ti.ToString();
                wa.CorrectAnswer = ti.Answer;
                wa.GivenAnswer = wr.Answer;
                if (wa.GivenAnswer.Trim() != "")
                {
                    wa.AnswerTimeStamp = wr.AnswerTimeStamp.ToString();
                }
                else
                {
                    wa.AnswerTimeStamp = "";
                }
                res.Add(wa);
            }
            return res.ToArray();
        }
        protected void btnDeleteResult_Click(object sender, EventArgs e)
        {
            AccessDB.DeleteWork(CurrentWork.WorkID);
            ViewState["CurrentWorkToShowResult"] = null;
            ViewStateToSession();
            Response.Redirect("~/WorkResults.aspx");
        }
        protected void btnTestItem_Click(object sender, EventArgs e)
        {
            int TestItemID = int.Parse(((LinkButton)sender).CommandArgument);
            TestItem TestItemToShow = AccessDB.GetTestItem(TestItemID);
            string QuestToShow = "<div style='text-align:center'><h3>Тема: " + AccessDB.GetTest(TestItemToShow.TestID).ToString() + "</h3>";
            QuestToShow += "<h4>" + TestItemToShow.ToString() + "</h4></div>";
            QuestToShow += TestItemToShow.QuestionHtml;
            ltrlQuestion.Text = QuestToShow;
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            ViewState["CurrentWorkToShowResult"] = null;
            ViewStateToSession();
            Response.Redirect("~/WorkResults.aspx");
        }
        private void SessionToViewState()
        {
            for (int i = 0; i < Session.Count; i++)
            {
                ViewState[Session.Keys[i].ToString()] = Session[Session.Keys[i].ToString()];
            }
        }
        private void ViewStateToSession()
        {
            string[] Keys = new string[ViewState.Count];
            ViewState.Keys.CopyTo(Keys, 0);
            for (int i = 0; i < Keys.Count(); i++)
            {
                Session[Keys[i]] = ViewState[Keys[i]];
            }
        }
    }
}