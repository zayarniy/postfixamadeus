﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Tasks.aspx.cs"
    Inherits="Tester.Tasks" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="contTitle" ContentPlaceHolderID="contTitle" runat="server">
    Задания
</asp:Content>
<asp:Content ID="contScript" ContentPlaceHolderID="contScript" runat="server">
    <script type="text/javascript">
        function DeleteMouseOver(el) {
            $(el).attr("src", "Buttons/DeleteMouseOver.png");
        }
        function DeleteMouseOut(el) {
            $(el).attr("src", "Buttons/Delete.png");
        }
        function AssignMouseOver(el) {
            $(el).attr("src", "Buttons/AssignMouseOver.png");
        }
        function AssignMouseOut(el) {
            $(el).attr("src", "Buttons/Assign.png");
        }
        function Confirm(el) {
            Message = $(el).attr("confirmmessage");
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm(Message)) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>
<asp:Content ID="contMenu" ContentPlaceHolderID="contMenu" runat="server">
    <table>
        <tr>
            <td>
                <asp:ImageButton ID="btnMyWork" runat="server" OnClick="btnMyWork_Click" ImageUrl="~/Buttons/MyWork.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnTests" runat="server" OnClick="btnTests_Click" ImageUrl="~/Buttons/Tests.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnTasks" runat="server" Enabled="false" ImageUrl="~/Buttons/TasksActive.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnWorkResults" runat="server" OnClick="btnWorkResults_Click" ImageUrl="~/Buttons/WorkResults.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnUsers" runat="server" OnClick="btnUsers_Click" ImageUrl="~/Buttons/Users.png" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="contMain" ContentPlaceHolderID="contMain" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"></asp:ScriptManager>
    <asp:CheckBox ID="chkOnlyMine" runat="server" Text="Только мои" Checked="true" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px" />
    <asp:Label ID="Label2" runat="server" Text="с" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:TextBox ID="txtFromDate" runat="server" ReadOnly="false" Width="100px" BackColor="#58FAF4" ForeColor="#0B0B3B"
        Font-Names="Arial" Font-Size="18px" BorderColor="#58FAF4"></asp:TextBox>
    <cc1:CalendarExtender ID="ce_txtFromDate" runat="server" TargetControlID="txtFromDate" Format="dd.MM.yyyy" CssClass="cal" />
    <asp:Label ID="Label3" runat="server" Text="по" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:TextBox ID="txtToDate" runat="server" ReadOnly="false" Width="100px" BackColor="#58FAF4" ForeColor="#0B0B3B"
        Font-Names="Arial" Font-Size="18px" BorderColor="#58FAF4"></asp:TextBox>
    <cc1:CalendarExtender ID="ce_txtToDate" runat="server" TargetControlID="txtToDate" Format="dd.MM.yyyy" CssClass="cal" />
    <br />
    <asp:ImageButton ID="btnApplyFilter" runat="server" OnClick="btnApplyFilter_Click" ImageUrl="~/Buttons/ApplyFilter.png" />
    <asp:Repeater ID="rptTaskList" runat="server">
        <HeaderTemplate>
            <table border="1">
                <tr>
                    <th>
                        <asp:LinkButton ID="btnTaskNameSort" runat="server" Text="Имя"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnTaskNameSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnUserCreatorSort" runat="server" Text="Создатель"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnUserCreatorSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnCreateDateSort" runat="server" Text="Дата-время создания"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnCreateDateSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnLastChangeDateSort" runat="server" Text="Дата-время последнего изменения"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnCreateDateSort_Click"></asp:LinkButton>
                    </th>
                    <th style="width: 110px">&nbsp;
                    </th>
                    <th style="width: 110px">&nbsp;
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:LinkButton ID="btnName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"
                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TaskID")  %>' OnClick="btnEdit_Click"></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="btnCreator" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CreatorUserString")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"
                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TaskID")  %>' OnClick="btnEdit_Click"></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="btnCreateDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CreateDate")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"
                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TaskID")  %>' OnClick="btnEdit_Click"></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="btnLastModifyDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LastModifyDate")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"
                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TaskID")  %>' OnClick="btnEdit_Click"></asp:LinkButton>
                </td>
                <td>
                    <asp:ImageButton ID="btnDelete" runat="server"
                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TaskID")  %>'
                        OnClick="btnDelete_Click"
                        ImageUrl="~/Buttons/Delete.png"
                        onmouseover="DeleteMouseOver(this);"
                        onmouseout="DeleteMouseOut(this);"
                        confirmmessage='<%# "Удалить задание "+DataBinder.Eval(Container.DataItem, "Name")+"?"  %>'
                        OnClientClick="Confirm(this);" />
                </td>
                <td>
                    <asp:ImageButton ID="btnAssign" runat="server"
                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TaskID")  %>'
                        OnClick="btnAssign_Click"
                        ImageUrl="~/Buttons/Assign.png"
                        onmouseover="AssignMouseOver(this);"
                        onmouseout="AssignMouseOut(this);" />
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="contButtons" ContentPlaceHolderID="contButtons" runat="server">
    <hr />
    <asp:ImageButton ID="btnCreateTask" runat="server" OnClick="btnCreateTask_Click" ImageUrl="~/Buttons/CreateTask.png" />
</asp:Content>
