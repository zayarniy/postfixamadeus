﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Tester
{
    public partial class Tests : System.Web.UI.Page
    {
        Test[] TestArr;
        bool EditFlag;
        int TestIDForEdit;
        bool IsAdmin;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SessionToViewState();
            }
            if (ViewState["EditFlag"] != null)
            {
                EditFlag = (bool)ViewState["EditFlag"];
            }
            else
            {
                EditFlag = false;
            }
            if (ViewState["TestIDForEdit"] != null)
            {
                TestIDForEdit = (int)ViewState["TestIDForEdit"];
            }
            else
            {
                TestIDForEdit = 0;
            }
            ShowParts();
            if (!IsPostBack)
            {
                if (EditFlag)
                {
                    FillPNTestEdit();
                }
            }
            if (ViewState["IsAdmin"]!=null)
            {
                IsAdmin=(bool)ViewState["IsAdmin"];
            }
            else
            {
                IsAdmin = false;
            }
            btnUsers.Visible = IsAdmin;

            TestArr = AccessDB.GetTests();

            OrderTests(TestArr);
            rptTestList.DataSource = TestArr;
            rptTestList.DataBind();
            SetButtons();
        }
        private void SetButtons()
        {
            btnMyWork.Attributes.Add("onmouseout", "this.src='Buttons/MyWork.png'");
            btnMyWork.Attributes.Add("onmouseover", "this.src='Buttons/MyWorkMouseOver.png'");
            btnTasks.Attributes.Add("onmouseout", "this.src='Buttons/Tasks.png'");
            btnTasks.Attributes.Add("onmouseover", "this.src='Buttons/TasksMouseOver.png'");
            btnWorkResults.Attributes.Add("onmouseout", "this.src='Buttons/WorkResults.png'");
            btnWorkResults.Attributes.Add("onmouseover", "this.src='Buttons/WorkResultsMouseOver.png'");
            btnUsers.Attributes.Add("onmouseout", "this.src='Buttons/Users.png'");
            btnUsers.Attributes.Add("onmouseover", "this.src='Buttons/UsersMouseOver.png'");
            btnOK.Attributes.Add("onmouseout", "this.src='Buttons/OK.png'");
            btnOK.Attributes.Add("onmouseover", "this.src='Buttons/OKMouseOver.png'");
            btnCancel.Attributes.Add("onmouseout", "this.src='Buttons/Cancel.png'");
            btnCancel.Attributes.Add("onmouseover", "this.src='Buttons/CancelMouseOver.png'");
        }
        private int CompareTest(Test t1, Test t2)
        {
            if (TestsThemeSort == "ASC")
            {
                return t1.Theme.CompareTo(t2.Theme);
            }
            if (TestsThemeSort == "DESC")
            {
                return t2.Theme.CompareTo(t1.Theme);
            }
            return 0;
        }
        private void OrderTests(Test[] Tests)
        {
            if (TestsThemeSort != "")
            {
                Array.Sort(Tests, CompareTest);
            }
        }
        private string NextSortDir(string SortDir)
        {
            switch (SortDir)
            {
                case "":
                    return "ASC";
                case "ASC":
                    return "DESC";
                case "DESC":
                    return "";
                default:
                    return "";
            }
        }
        private string TestsThemeSort
        {
            get
            {
                if (ViewState["TestsThemeSort"] != null)
                {
                    return (string)ViewState["TestsThemeSort"];
                }
                else
                {
                    return "ASC";
                }
            }
            set
            {
                ViewState["TestsThemeSort"] = value;
            }
        }
        protected void btnThemeSort_Click(object sender, EventArgs e)
        {
            TestsThemeSort = NextSortDir(TestsThemeSort);
            ViewStateToSession();
            Response.Redirect("~/Tests.aspx");
        }
        protected void lbTest_Click(object sender, EventArgs e)
        {
            ViewState["Test"] = TestArr[int.Parse(((LinkButton)sender).CommandArgument)];
            ViewStateToSession();
            Response.Redirect("~/TestDetail.aspx");
        }
        protected void btnMyWork_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/MyWork.aspx");
        }
        protected void btnTasks_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/Tasks.aspx");
        }
        protected void btnWorkResults_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/WorkResults.aspx");
        }
        protected void btnUsers_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/Users.aspx");
        }
        protected void btnRename_Click(object sender, EventArgs e)
        {
            ViewState["TestIDForEdit"] = int.Parse(((ImageButton)sender).CommandArgument);
            ViewState["EditFlag"] = true;
            ViewStateToSession();
            Response.Redirect("~/Tests.aspx");
        }
        private void ShowParts()
        {
            pnTestEdit.Visible = EditFlag;
            bottomHR.Visible = EditFlag;
            btnOK.Visible = EditFlag;
            btnCancel.Visible = EditFlag;
        }
        private void FillPNTestEdit()
        {
            Test tst = AccessDB.GetTest(TestIDForEdit);
            if (tst != null)
            {
                txtTheme.Text = tst.Theme;
            }
        }
        private void ClearState()
        {
            ViewState["EditFlag"] = null;
            ViewState["TestIDForEdit"] = null;
        }
        protected void btnOK_Click(object sender, EventArgs e)
        {
            if (EditFlag)
            {
                string sTheme = txtTheme.Text.Trim();
                if (sTheme != "")
                {
                    AccessDB.UpdateTestTheme(TestIDForEdit, sTheme);
                }
            }
            ClearState();
            ViewStateToSession();
            Response.Redirect("~/Tests.aspx");
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearState();
            ViewStateToSession();
            Response.Redirect("~/Tests.aspx");
        }
        private void SessionToViewState()
        {
            for (int i = 0; i < Session.Count; i++)
            {
                ViewState[Session.Keys[i].ToString()] = Session[Session.Keys[i].ToString()];
            }
        }
        private void ViewStateToSession()
        {
            string[] Keys = new string[ViewState.Count];
            ViewState.Keys.CopyTo(Keys, 0);
            for (int i = 0; i < Keys.Count(); i++)
            {
                Session[Keys[i]] = ViewState[Keys[i]];
            }
        }
    }
}