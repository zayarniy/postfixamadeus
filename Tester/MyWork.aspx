﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="MyWork.aspx.cs"
    Inherits="Tester.MyWork" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="contTitle" ContentPlaceHolderID="contTitle" runat="server">
    Мои задания
</asp:Content>
<asp:Content ID="contScript" ContentPlaceHolderID="contScript" runat="server">
    <script type="text/javascript">
        function ImageMouseOver(el) {
            WorkStateID = $(el).attr("WorkStateID");
            switch (WorkStateID) {
                case "1":
                    $(el).attr("src", "Buttons/StartMouseOver.png");
                    break;
                case "2":
                    $(el).attr("src", "Buttons/ContinueMouseOver.png");
                    break;
                case "3":
                    $(el).attr("src", "Buttons/RepeatMouseOver.png");
                    break;
            }
        }
        function ImageMouseOut(el) {
            WorkStateID = $(el).attr("WorkStateID");
            switch (WorkStateID) {
                case "1":
                    $(el).attr("src", "Buttons/Start.png");
                    break;
                case "2":
                    $(el).attr("src", "Buttons/Continue.png");
                    break;
                case "3":
                    $(el).attr("src", "Buttons/Repeat.png");
                    break;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="contMenu" ContentPlaceHolderID="contMenu" runat="server">
    <table>
        <tr>
            <td>
                <asp:ImageButton ID="btnMyWork" runat="server" Enabled="false" ImageUrl="~/Buttons/MyWorkActive.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnTests" runat="server" OnClick="btnTests_Click" ImageUrl="~/Buttons/Tests.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnTasks" runat="server" OnClick="btnTasks_Click" ImageUrl="~/Buttons/Tasks.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnWorkResults" runat="server" OnClick="btnWorkResults_Click" ImageUrl="~/Buttons/WorkResults.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnUsers" runat="server" OnClick="btnUsers_Click" ImageUrl="~/Buttons/Users.png" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="contMain" ContentPlaceHolderID="contMain" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"></asp:ScriptManager>
    <asp:CheckBox ID="chkNoComplete" runat="server" Text="Только незавершенные" Checked="true" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px" />
    <asp:Label ID="LabelC" runat="server" Text="с" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:TextBox ID="txtFromDate" runat="server" ReadOnly="false" Width="100px" BackColor="#58FAF4" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px" BorderColor="#58FAF4"></asp:TextBox>
    <cc1:CalendarExtender ID="ce_txtFromDate" runat="server" TargetControlID="txtFromDate" Format="dd.MM.yyyy" CssClass="cal" />
    <asp:Label ID="Label3" runat="server" Text="по" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:TextBox ID="txtToDate" runat="server" ReadOnly="false" Width="100px" BackColor="#58FAF4" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px" BorderColor="#58FAF4"></asp:TextBox>
    <cc1:CalendarExtender ID="ce_txtToDate" runat="server" TargetControlID="txtToDate" Format="dd.MM.yyyy" CssClass="cal" />
    <br />
    <asp:ImageButton ID="btnApplyFilter" runat="server" OnClick="btnApplyFilter_Click" ImageUrl="~/Buttons/ApplyFilter.png" />
    <asp:Repeater ID="rptMyWork" runat="server">
        <HeaderTemplate>
            <table border="1">
                <tr>
                    <th>
                        <asp:LinkButton ID="btnAssignDateSort" runat="server" Text="Дата Назначения"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnAssignDateSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnTaskNameSort" runat="server" Text="Задание"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnTaskNameSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnStateSort" runat="server" Text="Состояние"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnStateSort_Click"></asp:LinkButton>
                    </th>
                    <th style="width: 110px">&nbsp;
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:Label ID="lblAssignDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AssignDate")  %>' ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblTaskName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TaskName")  %>' ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblState" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "WorkState")  %>' ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                </td>
                <td>
                    <asp:ImageButton ID="btnStart" runat="server"
                        Visible='<%# DataBinder.Eval(Container.DataItem, "WorkStateID").ToString()!="3" || (bool)DataBinder.Eval(Container.DataItem, "Repeatable")  %>'
                        OnClick="btnStart_Click"
                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "WorkID")  %>'
                        ImageUrl='<%# DataBinder.Eval(Container.DataItem, "WorkStateID").ToString()=="1"?"~/Buttons/Start.png":(DataBinder.Eval(Container.DataItem, "WorkStateID").ToString()=="2"?"~/Buttons/Continue.png":"~/Buttons/Repeat.png") %>'
                        WorkStateID='<%# DataBinder.Eval(Container.DataItem, "WorkStateID").ToString() %>'
                        onmouseover="ImageMouseOver(this);"
                        onmouseout="ImageMouseOut(this);" />
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="contButtons" ContentPlaceHolderID="contButtons" runat="server">
</asp:Content>
