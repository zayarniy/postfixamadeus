﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Tester
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SessionToViewState();
            }
            lblIncorrect.Text = "";
            if (ViewState["Login"] != null)
            {
                bool isLogin = (bool)ViewState["Login"];
                if (!isLogin)
                {
                    lblIncorrect.Text = "Неверные имя пользователя/пароль";
                }
            }
            SetButtons();
        }
        private void SetButtons()
        {
            btnEnter.Attributes.Add("onmouseover", "this.src='Buttons/LoginMouseOver.png'");
            btnEnter.Attributes.Add("onmouseout", "this.src='Buttons/Login.png'");
        }
        protected void btnEnter_Click(object sender, EventArgs e)
        {
            bool isLogin;
            UserDB CurUser;
            isLogin = AccessDB.LoginDB(txtLogin.Text, txtPassword.Text, out CurUser);
            ViewState["Login"] = isLogin;
            if (isLogin)
            {
                ViewState["CurUser"] = CurUser;
                ViewState["IsAdmin"] = CurUser.IsAdmin;
                ViewStateToSession();
                Response.Redirect("~/MyWork.aspx");
            }
            else
            {
                ViewStateToSession();
                Response.Redirect("~/Login.aspx");
            }
        }
        private void SessionToViewState()
        {
            for (int i = 0; i < Session.Count; i++)
            {
                ViewState[Session.Keys[i].ToString()] = Session[Session.Keys[i].ToString()];
            }
        }
        private void ViewStateToSession()
        {
            string[] Keys = new string[ViewState.Count];
            ViewState.Keys.CopyTo(Keys, 0);
            for (int i = 0; i < Keys.Count(); i++)
            {
                Session[Keys[i]] = ViewState[Keys[i]];
            }
        }
    }
}