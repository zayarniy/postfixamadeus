﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Tests.aspx.cs" Inherits="Tester.Tests" EnableEventValidation="false" %>

<asp:Content ID="contTitle" ContentPlaceHolderID="contTitle" runat="server">
    Банк вопросов
</asp:Content>
<asp:Content ID="contScript" ContentPlaceHolderID="contScript" runat="server">
    <script type="text/javascript">
        function RenameMouseOver(el) {
            $(el).attr("src", "Buttons/RenameMouseOver.png");
        }
        function RenameMouseOut(el) {
            $(el).attr("src", "Buttons/Rename.png");
        }
    </script>
</asp:Content>
<asp:Content ID="contMenu" ContentPlaceHolderID="contMenu" runat="server">
    <table>
        <tr>
            <td>
                <asp:ImageButton ID="btnMyWork" runat="server" OnClick="btnMyWork_Click" ImageUrl="~/Buttons/MyWork.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnTests" runat="server" Enabled="false" ImageUrl="~/Buttons/TestsActive.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnTasks" runat="server" OnClick="btnTasks_Click" ImageUrl="~/Buttons/Tasks.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnWorkResults" runat="server" OnClick="btnWorkResults_Click" ImageUrl="~/Buttons/WorkResults.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnUsers" runat="server" OnClick="btnUsers_Click" ImageUrl="~/Buttons/Users.png" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="contMain" ContentPlaceHolderID="contMain" runat="server">
    <asp:Repeater ID="rptTestList" runat="server">
        <HeaderTemplate>
            <table border="1">
                <tr>
                    <th>
                        <asp:LinkButton ID="btnThemeSort" runat="server" Text="Тема"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"
                            OnClick="btnThemeSort_Click"></asp:LinkButton>
                    </th>
                    <th>&nbsp;
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:LinkButton ID="lbTest" runat="server" OnClick="lbTest_Click" CommandArgument="<%# Container.ItemIndex %>"
                        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"
                        Text='<%# DataBinder.Eval(Container.DataItem, "Theme")  %>'>
                    </asp:LinkButton>
                </td>
                <td>
                    <asp:ImageButton ID="btnRename" runat="server"
                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TestID")  %>'
                        OnClick="btnRename_Click"
                        ImageUrl="~/Buttons/Rename.png"
                        onmouseover="RenameMouseOver(this);"
                        onmouseout="RenameMouseOut(this);" />
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <asp:Panel ID="pnTestEdit" runat="server">
        <br />
        <asp:Label ID="Label1" runat="server" Text="Название темы:"
            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="18px"></asp:Label>
        <asp:TextBox ID="txtTheme" runat="server" Width="500px" BackColor="#58FAF4" ForeColor="#0B0B3B"
            Font-Names="Arial" Font-Size="18px" BorderColor="#58FAF4"></asp:TextBox>
    </asp:Panel>
</asp:Content>
<asp:Content ID="contButtons" ContentPlaceHolderID="contButtons" runat="server">
    <hr runat="server" id="bottomHR" />
    <asp:ImageButton ID="btnOK" runat="server" OnClick="btnOK_Click" ImageUrl="~/Buttons/OK.png" />
    <asp:ImageButton ID="btnCancel" runat="server" OnClick="btnCancel_Click" ImageUrl="~/Buttons/Cancel.png" />
</asp:Content>

