﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Tester
{
    public partial class AssignTask : System.Web.UI.Page
    {
        Task CurrTask;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SessionToViewState();
            }
            if (ViewState["CurrentTask"] != null)
            {
                CurrTask = (Task)ViewState["CurrentTask"];
                lblTaskName.Text = CurrTask.ToString();
                if (!IsPostBack)
                {
                    UpdateListUsers();
                }
                SetState();
            }
        }
        private void SetState()
        {
            btnOK.Attributes.Add("onmouseout", "this.src='Buttons/OK.png'");
            btnOK.Attributes.Add("onmouseover", "this.src='Buttons/OKMouseOver.png'");
            btnCancel.Attributes.Add("onmouseout", "this.src='Buttons/Cancel.png'");
            btnCancel.Attributes.Add("onmouseover", "this.src='Buttons/CancelMouseOver.png'");
            btnCancel.Attributes.Add("confirmmessage", "Отменить назначение задания " + CurrTask.ToString() + "?");
            litTitle.Text = "Назначение задания" + CurrTask.ToString();
        }
        private void UpdateListUsers()
        {
            lstUsers.Items.Clear();
            UserDB[] AllUsers = AccessDB.GetUsers();
            foreach (UserDB usr in AllUsers)
            {
                lstUsers.Items.Add(new ListItem(usr.ToString(), usr.UserID.ToString()));
            }
        }
        protected void btnOK_Click(object sender, EventArgs e)
        {
            if (ViewState["CurUser"] != null)
            {
                UserDB usr = (UserDB)ViewState["CurUser"];
                foreach (ListItem li in lstUsers.Items)
                {
                    if (li.Selected)
                    {
                        Work wrk = new Work();
                        wrk.UserID = int.Parse(li.Value);
                        wrk.UserAssignerID = usr.UserID;
                        wrk.TaskID = CurrTask.TaskID;
                        wrk.Continuous = chkContinuous.Checked;
                        wrk.ShowAnswer = chkShowAnswer.Checked;
                        wrk.Repeatable = chkRepeatable.Checked;
                        wrk.WorkStateID = 1;
                        wrk.AssignDate = DateTime.Now;
                        wrk.LastChangeStateDate = null;
                        int WorkID = AccessDB.AddWork(wrk);
                        AccessDB.AddWorkResultsFromTaskID(WorkID, CurrTask.TaskID);
                    }
                }
            }
            ViewStateToSession();
            Response.Redirect("~/Tasks.aspx");
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                ViewStateToSession();
                Response.Redirect("~/Tasks.aspx");
            }
        }
        private void SessionToViewState()
        {
            for (int i = 0; i < Session.Count; i++)
            {
                ViewState[Session.Keys[i].ToString()] = Session[Session.Keys[i].ToString()];
            }
        }
        private void ViewStateToSession()
        {
            string[] Keys = new string[ViewState.Count];
            ViewState.Keys.CopyTo(Keys, 0);
            for (int i = 0; i < Keys.Count(); i++)
            {
                Session[Keys[i]] = ViewState[Keys[i]];
            }
        }
    }
}