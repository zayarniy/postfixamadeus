﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using MySql.Data.MySqlClient;

namespace Tester
{
    public static class AccessDB
    {
        private static string ConnectionString
        {
            get
            {
                return WebConfigurationManager.ConnectionStrings["TesterConnectionString"].ToString();
            }
        }
        public static bool LoginDB (string Login,string Password, out UserDB curUser)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comm = new MySqlCommand("SELECT * FROM tst_user WHERE Login=@Login AND Password=@Password", con);
            comm.Parameters.AddWithValue("@Login", Login);
            comm.Parameters.AddWithValue("@Password", Password);
            MySqlDataReader rdr = comm.ExecuteReader();
            if (rdr.Read())
            {
                curUser = new UserDB();
                curUser.UserID = rdr.GetInt32("user_id");
                curUser.Login = Login;
                curUser.Password = Password;
                curUser.IsAdmin = (rdr.GetInt32("isAdmin") != 0);
                curUser.FirstName = rdr.GetString("FirstName");
                curUser.SecondName = rdr.GetString("SecondName");
                if (!rdr.IsDBNull(rdr.GetOrdinal("EMail")))
                {
                    curUser.EMail = rdr.GetString("EMail");
                }
                else
                {
                    curUser.EMail = "";
                }
                con.Close();
                return true;
            }
            curUser = null;
            con.Close();
            return false;
        }
        public static UserDB[] GetUsers()
        {
            List<UserDB> res = new List<UserDB>();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comm = new MySqlCommand("SELECT User_ID,Login,Password,isAdmin,FirstName,SecondName,EMail FROM tst_user", con);
            MySqlDataReader rdr = comm.ExecuteReader();

            while (rdr.Read())
            {
                UserDB uDB = new UserDB();
                uDB.UserID = rdr.GetInt32("User_ID");
                uDB.Login = rdr.GetString("Login");
                uDB.Password = rdr.GetString("Password");
                uDB.IsAdmin = (rdr.GetInt32("isAdmin") != 0);
                uDB.FirstName = rdr.GetString("FirstName");
                uDB.SecondName = rdr.GetString("SecondName");
                if (!rdr.IsDBNull(rdr.GetOrdinal("EMail")))
                {
                    uDB.EMail = rdr.GetString("EMail");
                }
                else
                {
                    uDB.EMail = "";
                }
                res.Add(uDB);
            }
            con.Close();
            return res.ToArray();
        }
        public static UserDB GetUser(int UserID)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comm = new MySqlCommand("SELECT Login,Password,isAdmin,FirstName,SecondName,EMail FROM tst_user WHERE User_ID=" + UserID.ToString(), con);
            MySqlDataReader rdr = comm.ExecuteReader();
            if (rdr.Read())
            {
                UserDB uDB = new UserDB();
                uDB.UserID = UserID;
                uDB.Login = rdr.GetString("Login");
                uDB.Password = rdr.GetString("Password");
                uDB.IsAdmin = (rdr.GetInt32("isAdmin") != 0);
                uDB.FirstName = rdr.GetString("FirstName");
                uDB.SecondName = rdr.GetString("SecondName");
                if (!rdr.IsDBNull(rdr.GetOrdinal("EMail")))
                {
                    uDB.EMail = rdr.GetString("EMail");
                }
                else
                {
                    uDB.EMail = "";
                }
                con.Close();
                return uDB;
            }
            con.Close();
            return null;
        }
        private static int GetNextUserID()
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com = new MySqlCommand("SELECT count(1) FROM tst_user", con);
            int cnt;
            cnt = int.Parse(com.ExecuteScalar().ToString());
            if (cnt > 0)
            {
                com = new MySqlCommand("SELECT max(User_ID) FROM tst_user", con);
                int usrID;
                usrID = ((int)com.ExecuteScalar()) + 1;
                con.Close();
                return usrID;
            }
            else
            {
                con.Close();
                return 1;
            }
        }
        public static int AddUser(UserDB user)
        {
            int UserID;
            UserID = GetNextUserID();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com = new MySqlCommand("INSERT INTO tst_user(User_ID,Login,Password,isAdmin,FirstName,SecondName,EMail) VALUES"+
                " (@User_ID,@Login,@Password,@isAdmin,@FirstName,@SecondName,@EMail)", con);
            com.Parameters.AddWithValue("@User_ID", UserID);
            com.Parameters.AddWithValue("@Login", user.Login);
            com.Parameters.AddWithValue("@Password", user.Password);
            com.Parameters.AddWithValue("@isAdmin", user.IsAdmin);
            com.Parameters.AddWithValue("@FirstName", user.FirstName);
            com.Parameters.AddWithValue("@SecondName", user.SecondName);
            com.Parameters.AddWithValue("@EMail", user.EMail);
            com.ExecuteNonQuery();
            con.Close();
            return UserID;
        }
        public static void ModifyUser(UserDB user)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com = new MySqlCommand("UPDATE tst_user SET Login=@Login,Password=@Password,isAdmin=@isAdmin,"+
                "FirstName=@FirstName,SecondName=@SecondName,EMail=@EMail WHERE User_ID=@User_ID", con);
            com.Parameters.AddWithValue("@User_ID", user.UserID);
            com.Parameters.AddWithValue("@Login", user.Login);
            com.Parameters.AddWithValue("@Password", user.Password);
            com.Parameters.AddWithValue("@isAdmin", user.IsAdmin);
            com.Parameters.AddWithValue("@FirstName", user.FirstName);
            com.Parameters.AddWithValue("@SecondName", user.SecondName);
            com.Parameters.AddWithValue("@EMail", user.EMail);
            com.ExecuteNonQuery();
            con.Close();
        }
        public static void DeleteUser(int UserID)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com = new MySqlCommand("DELETE FROM tst_user WHERE User_ID=" + UserID.ToString(), con);
            com.ExecuteNonQuery();
            con.Close();
            return;
        }
        public static void ClearPassword(int UserID)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com = new MySqlCommand("UPDATE tst_user SET Password='' WHERE User_ID=" + UserID.ToString(), con);
            com.ExecuteNonQuery();
            con.Close();
            return;
        }
        public static void ChangePassword(int UserID, string Password)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com = new MySqlCommand("UPDATE tst_user SET Password=@Password WHERE User_ID=" + UserID.ToString(), con);
            com.Parameters.AddWithValue("@Password", Password);
            com.ExecuteNonQuery();
            con.Close();
            return;
        }
        public static Test[] GetTests()
        {
            List<Test> res = new List<Test>();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comm = new MySqlCommand("SELECT Test_ID,Theme,OrderNumber FROM test ORDER BY OrderNumber", con);
            MySqlDataReader rdr = comm.ExecuteReader();

            while (rdr.Read())
            {
                Test t = new Test();
                t.TestID = rdr.GetInt32("Test_ID");
                t.Theme = rdr.GetString("Theme");
                if (!rdr.IsDBNull(rdr.GetOrdinal("OrderNumber")))
                {
                    t.OrderNumber = rdr.GetInt32("OrderNumber");
                }
                else
                {
                    t.OrderNumber = 0;
                }
                res.Add(t);
            }
            con.Close();
            return res.ToArray();
        }
        public static Test GetTest(int TestID)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comm = new MySqlCommand("SELECT Theme,OrderNumber FROM test WHERE Test_ID=" + TestID.ToString(), con);
            MySqlDataReader rdr = comm.ExecuteReader();

            if (rdr.Read())
            {
                Test t = new Test();
                t.TestID = TestID;
                t.Theme = rdr.GetString("Theme");
                if (!rdr.IsDBNull(rdr.GetOrdinal("OrderNumber")))
                {
                    t.OrderNumber = rdr.GetInt32("OrderNumber");
                }
                else
                {
                    t.OrderNumber = 0;
                }
                con.Close();
                return t;
            }
            else
            {
                con.Close();
                return null;
            }
        }
        public static void UpdateTestTheme(int TestID,string Theme)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comm = new MySqlCommand("UPDATE test SET Theme=@Theme WHERE Test_ID=@TestID", con);
            comm.Parameters.AddWithValue("@Theme", Theme);
            comm.Parameters.AddWithValue("@TestID", TestID);
            comm.ExecuteNonQuery();
            con.Close();
        }
        public static AnswerType[] GetAnswerTypes()
        {
            List<AnswerType> res = new List<AnswerType>();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comm = new MySqlCommand("SELECT AnswerType_ID,AnswerTypeVal FROM answertype", con);
            MySqlDataReader rdr = comm.ExecuteReader();
            while (rdr.Read())
            {
                AnswerType at = new AnswerType();
                at.AnswerTypeID = rdr.GetInt32("AnswerType_ID");
                at.AnswerTypeVal = rdr.GetString("AnswerTypeVal");
                res.Add(at);
            }
            con.Close();
            return res.ToArray();
        }
        public static string GetAnswerTypeVal(int AnswerTypeID)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comm = new MySqlCommand("SELECT AnswerTypeVal FROM answertype WHERE AnswerType_ID=" + AnswerTypeID.ToString(), con);
            MySqlDataReader rdr = comm.ExecuteReader();
            if (rdr.Read())
            {
                string res = rdr.GetString("AnswerTypeVal");
                con.Close();
                return res;

            }
            else
            {
                con.Close();
                return "Unknown";
            }
        }
        public static int GetTestItemCount(int TestID)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comm = new MySqlCommand("SELECT COUNT(1) AS cnt FROM testitem WHERE Test_ID=" + TestID.ToString(), con);
            MySqlDataReader rdr = comm.ExecuteReader();
            if (rdr.Read())
            {
                int res = rdr.GetInt32("cnt");
                con.Close();
                return res;
            }
            else
            {
                con.Close();
                return 0;
            }
        }

        public static string UnZip(byte[] value)
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream(value);
            System.IO.Compression.GZipStream sr = new System.IO.Compression.GZipStream(ms,
                System.IO.Compression.CompressionMode.Decompress);

            List<byte> Res = new List<byte>();
            int b;
            while ((b = sr.ReadByte()) != -1)
            {
                Res.Add((byte)b);
            }

            return System.Text.Encoding.Unicode.GetString(Res.ToArray());
        }
        public static TestItem[] GetTestItems(int TestID)
        {
            List<TestItem> res = new List<TestItem>();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comm = new MySqlCommand("SELECT TestItem_ID,Name,Difficulty,QuestionRtf,length(QuestionRtf) as LenRtf,QuestionHtml," +
                "length(QuestionHtml) as LenHtml,AnswerType_ID,Answer," +
                "Year,Comment1,Comment2,OrderNumber " +
                "FROM testitem WHERE Test_ID=" + TestID.ToString() + " ORDER BY OrderNumber", con);
            MySqlDataReader rdr = comm.ExecuteReader();

            while (rdr.Read())
            {
                TestItem t = new TestItem();
                t.TestItemID = rdr.GetInt32("TestItem_ID");
                t.TestID = TestID;
                t.Name = rdr.GetString("Name");
                t.Difficulty = rdr.GetString("Difficulty");
                int lenRtf = rdr.GetInt32("LenRtf");
                byte[] QuestRtfComp = new byte[lenRtf];
                rdr.GetBytes(rdr.GetOrdinal("QuestionRtf"), 0, QuestRtfComp, 0, lenRtf);
                t.QuestionRtf = UnZip(QuestRtfComp);
                int lenHtml = rdr.GetInt32("LenHtml");
                byte[] QuestHtmlComp = new byte[lenHtml];
                rdr.GetBytes(rdr.GetOrdinal("QuestionHtml"), 0, QuestHtmlComp, 0, lenHtml);
                t.QuestionHtml = UnZip(QuestHtmlComp);
                t.AnswerTypeID = rdr.GetInt32("AnswerType_ID");
                t.AnswerType = GetAnswerTypeVal(t.AnswerTypeID);
                t.Answer = rdr.GetString("Answer");
                t.Year = rdr.GetString("Year");
                t.Comment1 = rdr.GetString("Comment1");
                t.Comment2 = rdr.GetString("Comment2");
                t.OrderNumber = rdr.GetInt32("OrderNumber");
                res.Add(t);
            }
            con.Close();
            return res.ToArray();
        }
        public static TestItem GetTestItem(int TestItemID)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comm = new MySqlCommand("SELECT Test_ID,Name,Difficulty,QuestionRtf,length(QuestionRtf) as LenRtf,QuestionHtml," +
                "length(QuestionHtml) as LenHtml,AnswerType_ID,Answer," +
                "Year,Comment1,Comment2,OrderNumber " +
                "FROM testitem WHERE TestItem_ID=" + TestItemID.ToString(), con);
            MySqlDataReader rdr = comm.ExecuteReader();

            if (rdr.Read())
            {
                TestItem t = new TestItem();
                t.TestItemID = TestItemID;
                t.TestID = rdr.GetInt32("Test_ID");
                t.Name = rdr.GetString("Name");
                t.Difficulty = rdr.GetString("Difficulty");
                int lenRtf = rdr.GetInt32("LenRtf");
                byte[] QuestRtfComp = new byte[lenRtf];
                rdr.GetBytes(rdr.GetOrdinal("QuestionRtf"), 0, QuestRtfComp, 0, lenRtf);
                t.QuestionRtf = UnZip(QuestRtfComp);
                int lenHtml = rdr.GetInt32("LenHtml");
                byte[] QuestHtmlComp = new byte[lenHtml];
                rdr.GetBytes(rdr.GetOrdinal("QuestionHtml"), 0, QuestHtmlComp, 0, lenHtml);
                t.QuestionHtml = UnZip(QuestHtmlComp);
                t.AnswerTypeID = rdr.GetInt32("AnswerType_ID");
                t.AnswerType = GetAnswerTypeVal(t.AnswerTypeID);
                t.Answer = rdr.GetString("Answer");
                t.Year = rdr.GetString("Year");
                t.Comment1 = rdr.GetString("Comment1");
                t.Comment2 = rdr.GetString("Comment2");
                t.OrderNumber = rdr.GetInt32("OrderNumber");
                con.Close();
                return t;
            }
            else
            {
                con.Close();
                return null;
            }
        }
        public static Task[] GetTasks(string WhereClause)
        {
            List<Task> res = new List<Task>();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            if (WhereClause.Trim() == "")
            {
                WhereClause = "1=1";
            }
            MySqlCommand comm = new MySqlCommand("SELECT Task_ID,Name,CreatorUser_ID,CreateDate,LastModifyDate " +
                "FROM task WHERE " + WhereClause, con);
            MySqlDataReader rdr = comm.ExecuteReader();

            while (rdr.Read())
            {
                Task t = new Task();
                t.TaskID = rdr.GetInt32("Task_ID");
                t.Name = rdr.GetString("Name");
                if (!rdr.IsDBNull(rdr.GetOrdinal("CreatorUser_ID")))
                {
                    t.CreatorUserID = rdr.GetInt32("CreatorUser_ID");
                    UserDB usr = GetUser(t.CreatorUserID);
                    if (usr!=null)
                    {
                        t.CreatorUserString = usr.ToString();
                    }
                    else
                    {
                        t.CreatorUserString = "";
                    }
                }
                else
                {
                    t.CreatorUserID = 0;
                    t.CreatorUserString = "";
                }
                t.CreateDate = rdr.GetDateTime("CreateDate");
                t.LastModifyDate = rdr.GetDateTime("LastModifyDate");
                t.TaskTestItemIDs = GetTaskTestItemIDs(t.TaskID);
                res.Add(t);
            }
            con.Close();
            return res.ToArray();
        }
        public static Task[] GetTasks()
        {
            return GetTasks("");
        }
        public static Task GetTask(int TaskID)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comm = new MySqlCommand("SELECT Name,CreatorUser_ID,CreateDate,LastModifyDate FROM task " +
                "WHERE Task_ID=" + TaskID.ToString(), con);
            MySqlDataReader rdr = comm.ExecuteReader();

            if (rdr.Read())
            {
                Task t = new Task();
                t.TaskID = TaskID;
                t.Name = rdr.GetString("Name");
                if (!rdr.IsDBNull(rdr.GetOrdinal("CreatorUser_ID")))
                {
                    t.CreatorUserID = rdr.GetInt32("CreatorUser_ID");
                    UserDB usr = GetUser(t.CreatorUserID);
                    if (usr != null)
                    {
                        t.CreatorUserString = usr.ToString();
                    }
                    else
                    {
                        t.CreatorUserString = "";
                    }
                }
                else
                {
                    t.CreatorUserID = 0;
                    t.CreatorUserString = "";
                }
                t.CreateDate = rdr.GetDateTime("CreateDate");
                t.LastModifyDate = rdr.GetDateTime("LastModifyDate");
                t.TaskTestItemIDs = GetTaskTestItemIDs(t.TaskID);
                con.Close();
                return t;
            }
            else
            {
                con.Close();
                return null;
            }
        }
        public static int[] GetTaskTestItemIDs(int TaskID)
        {
            List<int> res = new List<int>();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comm = new MySqlCommand("SELECT TestItem_ID FROM tasktestitem WHERE Task_ID=" + TaskID.ToString() +
                " ORDER BY OrderNumber", con);
            MySqlDataReader rdr = comm.ExecuteReader();
            while (rdr.Read())
            {
                res.Add(rdr.GetInt32("TestItem_ID"));
            }
            con.Close();
            return res.ToArray();
        }
        private static int GetNextTaskID()
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com = new MySqlCommand("SELECT count(1) FROM task", con);
            int cnt;
            cnt = int.Parse(com.ExecuteScalar().ToString());
            if (cnt > 0)
            {
                com = new MySqlCommand("SELECT max(Task_ID) FROM task", con);
                int tskID;
                tskID = ((int)com.ExecuteScalar()) + 1;
                con.Close();
                return tskID;
            }
            else
            {
                con.Close();
                return 1;
            }
        }
        private static int GetNextTaskTestItemID()
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com = new MySqlCommand("SELECT count(1) FROM tasktestitem", con);
            int cnt;
            cnt = int.Parse(com.ExecuteScalar().ToString());
            if (cnt > 0)
            {
                com = new MySqlCommand("SELECT max(TaskTestItem_ID) FROM tasktestitem", con);
                int tskTstItmID;
                tskTstItmID = ((int)com.ExecuteScalar()) + 1;
                con.Close();
                return tskTstItmID;
            }
            else
            {
                con.Close();
                return 1;
            }
        }
        public static int AddTask(Task pTask)
        {
            int TaskID = GetNextTaskID();
            int TaskTestItemID = GetNextTaskTestItemID();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlTransaction tx = con.BeginTransaction();
            try
            {
                MySqlCommand com = new MySqlCommand("INSERT INTO task(Task_ID,Name,CreatorUser_ID,CreateDate,LastModifyDate) " +
                    "VALUES (@Task_ID,@Name,@CreatorUser_ID,@CreateDate,@LastModifyDate)", con);
                com.Parameters.AddWithValue("@Task_ID", TaskID);
                com.Parameters.AddWithValue("@Name", pTask.Name);
                com.Parameters.AddWithValue("@CreatorUser_ID", pTask.CreatorUserID);
                com.Parameters.AddWithValue("@CreateDate", pTask.CreateDate);
                com.Parameters.AddWithValue("@LastModifyDate", pTask.LastModifyDate);
                com.ExecuteNonQuery();
                int OrderNumber = 0;
                foreach (int testItemID in pTask.TaskTestItemIDs)
                {
                    OrderNumber++;
                    MySqlCommand com2 = new MySqlCommand("INSERT INTO tasktestitem(TaskTestItem_ID,Task_ID,TestItem_ID,OrderNumber) " +
                        "VALUES (@TaskTestItem_ID,@Task_ID,@TestItem_ID,@OrderNumber)", con);
                    com2.Parameters.AddWithValue("@TaskTestItem_ID", TaskTestItemID);
                    com2.Parameters.AddWithValue("@Task_ID", TaskID);
                    com2.Parameters.AddWithValue("@TestItem_ID", testItemID);
                    com2.Parameters.AddWithValue("@OrderNumber", OrderNumber);
                    com2.ExecuteNonQuery();
                    TaskTestItemID++;
                }
                tx.Commit();
            }
            catch
            {
                tx.Rollback();
            }
            finally
            {
                con.Close();
            }
            return TaskID;
        }
        public static void EditTask(Task pTask)
        {
            int TaskTestItemID = GetNextTaskTestItemID();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlTransaction tx = con.BeginTransaction();
            try
            {
                MySqlCommand com = new MySqlCommand("UPDATE task SET Name=@Name,CreatorUser_ID=@CreatorUser_ID,"+
                    "CreateDate=@CreateDate,LastModifyDate=@LastModifyDate WHERE Task_ID=@Task_ID", con);
                com.Parameters.AddWithValue("@Task_ID", pTask.TaskID);
                com.Parameters.AddWithValue("@Name", pTask.Name);
                com.Parameters.AddWithValue("@CreatorUser_ID", pTask.CreatorUserID);
                com.Parameters.AddWithValue("@CreateDate", pTask.CreateDate);
                com.Parameters.AddWithValue("@LastModifyDate", pTask.LastModifyDate);
                com.ExecuteNonQuery();
                MySqlCommand comDel = new MySqlCommand("DELETE FROM tasktestitem WHERE Task_ID=" + pTask.TaskID.ToString(), con);
                comDel.ExecuteNonQuery();
                int OrderNumber = 0;
                foreach (int testItemID in pTask.TaskTestItemIDs)
                {
                    OrderNumber++;
                    MySqlCommand com2 = new MySqlCommand("INSERT INTO tasktestitem(TaskTestItem_ID,Task_ID,TestItem_ID,OrderNumber) " +
                        "VALUES (@TaskTestItem_ID,@Task_ID,@TestItem_ID,@OrderNumber)", con);
                    com2.Parameters.AddWithValue("@TaskTestItem_ID", TaskTestItemID);
                    com2.Parameters.AddWithValue("@Task_ID", pTask.TaskID);
                    com2.Parameters.AddWithValue("@TestItem_ID", testItemID);
                    com2.Parameters.AddWithValue("@OrderNumber", OrderNumber);
                    com2.ExecuteNonQuery();
                    TaskTestItemID++;
                }
                tx.Commit();
            }
            catch
            {
                tx.Rollback();
            }
            finally
            {
                con.Close();
            }
        }
        public static void DeleteTask(int TaskID)
        {
            int TaskTestItemID = GetNextTaskTestItemID();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com1 = new MySqlCommand("DELETE FROM task WHERE Task_ID=" + TaskID.ToString(), con);
            MySqlCommand com2 = new MySqlCommand("DELETE FROM tasktestitem WHERE Task_ID=" + TaskID.ToString(), con);
            MySqlTransaction tx = con.BeginTransaction();
            try
            {
                com1.ExecuteNonQuery();
                com2.ExecuteNonQuery();
                tx.Commit();
            }
            catch
            {
                tx.Rollback();
            }
            finally
            {
                con.Close();
            }
        }
        public static WorkState[] GetWorkStates()
        {
            List<WorkState> res = new List<WorkState>();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comm = new MySqlCommand("SELECT WorkState_ID,WorkStateString FROM workstate", con);
            MySqlDataReader rdr = comm.ExecuteReader();

            while (rdr.Read())
            {
                WorkState ws = new WorkState();
                ws.WorkStateID = rdr.GetInt32("WorkState_ID");
                ws.WorkStateString = rdr.GetString("WorkStateString");
                res.Add(ws);
            }
            con.Close();
            return res.ToArray();
        }
        public static string GetWorkStateVal(int WorkStateID)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comm = new MySqlCommand("SELECT WorkStateString FROM workstate WHERE WorkState_ID=" + WorkStateID.ToString(), con);
            MySqlDataReader rdr = comm.ExecuteReader();
            if (rdr.Read())
            {
                string res = rdr.GetString("WorkStateString");
                con.Close();
                return res;

            }
            else
            {
                con.Close();
                return "Unknown";
            }
        }
        public static Work[] GetWorks(string WhereClause)
        {
            List<Work> res = new List<Work>();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            if (WhereClause.Trim() == "")
            {
                WhereClause = "1=1";
            }
            MySqlCommand comm = new MySqlCommand("SELECT Work_ID,User_ID,UserAssigner_ID,Task_ID,Continuous,ShowAnswer,Repeatable," +
                "WorkState_ID,AssignDate,LastChangeStateDate,BeginDateTime,EndDateTime " +
                "FROM work WHERE " + WhereClause, con);
            MySqlDataReader rdr = comm.ExecuteReader();
            while (rdr.Read())
            {
                Work w = new Work();
                w.WorkID = rdr.GetInt32("Work_ID");
                w.UserID = rdr.GetInt32("User_ID");
                w.UserAssignerID = rdr.GetInt32("UserAssigner_ID");
                if (!rdr.IsDBNull(rdr.GetOrdinal("Task_ID")))
                {
                    w.TaskID = rdr.GetInt32("Task_ID");
                }
                else
                {
                    w.TaskID = 0;
                }
                w.TaskName = GetTask(w.TaskID).ToString();
                w.Continuous = (rdr.GetInt32("Continuous") != 0);
                w.ShowAnswer = (rdr.GetInt32("ShowAnswer") != 0);
                w.Repeatable = (rdr.GetInt32("Repeatable") != 0);
                w.WorkStateID = rdr.GetInt32("WorkState_ID");
                w.WorkState = GetWorkStateVal(w.WorkStateID);
                w.AssignDate = rdr.GetDateTime("AssignDate");
                if (!rdr.IsDBNull(rdr.GetOrdinal("LastChangeStateDate")))
                {
                    w.LastChangeStateDate = rdr.GetDateTime("LastChangeStateDate");
                }
                else
                {
                    w.LastChangeStateDate = null;
                }
                if (!rdr.IsDBNull(rdr.GetOrdinal("BeginDateTime")))
                {
                    w.BeginDateTime = rdr.GetDateTime("BeginDateTime");
                }
                else
                {
                    w.BeginDateTime = null;
                }
                if (!rdr.IsDBNull(rdr.GetOrdinal("EndDateTime")))
                {
                    w.EndDateTime = rdr.GetDateTime("EndDateTime");
                }
                else
                {
                    w.EndDateTime = null;
                }
                res.Add(w);
            }
            con.Close();
            return res.ToArray();
        }
        public static Work[] GetWorks()
        {
            return GetWorks("");
        }
        public static Work[] GetWorks(int UserID)
        {
            return GetWorks("User_ID=" + UserID.ToString());
        }
        public static Work GetWork(int WorkID)
        {
            List<Work> res = new List<Work>();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();

            MySqlCommand comm = new MySqlCommand("SELECT Work_ID,User_ID,UserAssigner_ID,Task_ID,Continuous,ShowAnswer,Repeatable,"+
                "WorkState_ID,AssignDate,LastChangeStateDate,BeginDateTime,EndDateTime FROM work WHERE Work_ID=" + WorkID.ToString(), con);
            MySqlDataReader rdr = comm.ExecuteReader();
            if (rdr.Read())
            {
                Work w = new Work();
                w.WorkID = rdr.GetInt32("Work_ID");
                w.UserID = rdr.GetInt32("User_ID");
                w.UserAssignerID = rdr.GetInt32("UserAssigner_ID");
                if (!rdr.IsDBNull(rdr.GetOrdinal("Task_ID")))
                {
                    w.TaskID = rdr.GetInt32("Task_ID");
                }
                else
                {
                    w.TaskID = 0;
                }
                w.TaskName = GetTask(w.TaskID).ToString();
                w.Continuous = (rdr.GetInt32("Continuous") != 0);
                w.ShowAnswer = (rdr.GetInt32("ShowAnswer") != 0);
                w.Repeatable = (rdr.GetInt32("Repeatable") != 0);
                w.WorkStateID = rdr.GetInt32("WorkState_ID");
                w.WorkState = GetWorkStateVal(w.WorkStateID);
                w.AssignDate = rdr.GetDateTime("AssignDate");
                if (!rdr.IsDBNull(rdr.GetOrdinal("LastChangeStateDate")))
                {
                    w.LastChangeStateDate = rdr.GetDateTime("LastChangeStateDate");
                }
                else
                {
                    w.LastChangeStateDate = null;
                }
                if (!rdr.IsDBNull(rdr.GetOrdinal("BeginDateTime")))
                {
                    w.BeginDateTime = rdr.GetDateTime("BeginDateTime");
                }
                else
                {
                    w.BeginDateTime = null;
                }
                if (!rdr.IsDBNull(rdr.GetOrdinal("EndDateTime")))
                {
                    w.EndDateTime = rdr.GetDateTime("EndDateTime");
                }
                else
                {
                    w.EndDateTime = null;
                }
                con.Close();
                return w;
            }
            else
            {
                con.Close();
                return null;
            }
        }
        private static int GetNextWorkID()
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com = new MySqlCommand("SELECT count(1) FROM work", con);
            int cnt;
            cnt = int.Parse(com.ExecuteScalar().ToString());
            if (cnt > 0)
            {
                com = new MySqlCommand("SELECT max(Work_ID) FROM work", con);
                int wrkID;
                wrkID = ((int)com.ExecuteScalar()) + 1;
                con.Close();
                return wrkID;
            }
            else
            {
                con.Close();
                return 1;
            }
        }
        public static int AddWork(Work NewWork)
        {
            int WorkID = GetNextWorkID();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com = new MySqlCommand("INSERT INTO work (Work_ID,User_ID,UserAssigner_ID,Task_ID,Continuous,ShowAnswer,Repeatable,WorkState_ID,"+
                "AssignDate,LastChangeStateDate,BeginDateTime,EndDateTime) VALUES (@Work_ID,@User_ID,@UserAssigner_ID,@Task_ID,@Continuous,@ShowAnswer,@Repeatable,@WorkState_ID," +
                "@AssignDate,@LastChangeStateDate,@BeginDateTime,@EndDateTime)", con);
            com.Parameters.AddWithValue("@Work_ID", WorkID);
            com.Parameters.AddWithValue("@User_ID", NewWork.UserID);
            com.Parameters.AddWithValue("@UserAssigner_ID", NewWork.UserAssignerID);
            com.Parameters.AddWithValue("@Task_ID", NewWork.TaskID);
            com.Parameters.AddWithValue("@Continuous", NewWork.Continuous ? 1 : 0);
            com.Parameters.AddWithValue("@ShowAnswer", NewWork.ShowAnswer ? 1 : 0);
            com.Parameters.AddWithValue("@Repeatable", NewWork.Repeatable ? 1 : 0);
            com.Parameters.AddWithValue("@WorkState_ID", NewWork.WorkStateID);
            com.Parameters.AddWithValue("@AssignDate", NewWork.AssignDate);
            if (NewWork.LastChangeStateDate != null)
            {
                com.Parameters.AddWithValue("@LastChangeStateDate", NewWork.LastChangeStateDate);
            }
            else
            {
                com.Parameters.AddWithValue("@LastChangeStateDate", DBNull.Value);
            }
            if (NewWork.BeginDateTime != null)
            {
                com.Parameters.AddWithValue("@BeginDateTime", NewWork.BeginDateTime);
            }
            else
            {
                com.Parameters.AddWithValue("@BeginDateTime", DBNull.Value);
            }
            if (NewWork.EndDateTime != null)
            {
                com.Parameters.AddWithValue("@EndDateTime", NewWork.EndDateTime);
            }
            else
            {
                com.Parameters.AddWithValue("@EndDateTime", DBNull.Value);
            }
            com.ExecuteNonQuery();
            con.Close();
            return WorkID;
        }
        public static void UpdateWork(Work NewWork)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com = new MySqlCommand("UPDATE work SET User_ID=@User_ID,UserAssigner_ID=@UserAssigner_ID,Task_ID=@Task_ID,"+
                "Continuous=@Continuous,ShowAnswer=@ShowAnswer,Repeatable=@Repeatable,WorkState_ID=@WorkState_ID,AssignDate=@AssignDate,"+
                "LastChangeStateDate=@LastChangeStateDate,BeginDateTime=@BeginDateTime,EndDateTime=@EndDateTime WHERE Work_ID=@Work_ID", con);
            com.Parameters.AddWithValue("@Work_ID", NewWork.WorkID);
            com.Parameters.AddWithValue("@User_ID", NewWork.UserID);
            com.Parameters.AddWithValue("@UserAssigner_ID", NewWork.UserAssignerID);
            com.Parameters.AddWithValue("@Task_ID", NewWork.TaskID);
            com.Parameters.AddWithValue("@Continuous", NewWork.Continuous ? 1 : 0);
            com.Parameters.AddWithValue("@ShowAnswer", NewWork.ShowAnswer ? 1 : 0);
            com.Parameters.AddWithValue("@Repeatable", NewWork.Repeatable ? 1 : 0);
            com.Parameters.AddWithValue("@WorkState_ID", NewWork.WorkStateID);
            com.Parameters.AddWithValue("@AssignDate", NewWork.AssignDate);
            if (NewWork.LastChangeStateDate != null)
            {
                com.Parameters.AddWithValue("@LastChangeStateDate", NewWork.LastChangeStateDate);
            }
            else
            {
                com.Parameters.AddWithValue("@LastChangeStateDate", DBNull.Value);
            }
            if (NewWork.BeginDateTime != null)
            {
                com.Parameters.AddWithValue("@BeginDateTime", NewWork.BeginDateTime);
            }
            else
            {
                com.Parameters.AddWithValue("@BeginDateTime", DBNull.Value);
            }
            if (NewWork.EndDateTime != null)
            {
                com.Parameters.AddWithValue("@EndDateTime", NewWork.EndDateTime);
            }
            else
            {
                com.Parameters.AddWithValue("@EndDateTime", DBNull.Value);
            }
            com.ExecuteNonQuery();
            con.Close();
        }
        public static void DeleteWork(int WorkID)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com = new MySqlCommand("DELETE FROM work WHERE Work_ID=" + WorkID.ToString(), con);
            com.ExecuteNonQuery();
            con.Close();
        }
        public static void AddWorkResultsFromTaskID(int WorkID, int TaskID)
        {
            int WrkResID = GetNextWorkResultID();
            int[] TaskItemIDs = GetTaskTestItemIDs(TaskID);
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comDel = new MySqlCommand("DELETE FROM workresult WHERE Work_ID=" + WorkID.ToString(), con);
            comDel.ExecuteNonQuery();
            for (int i = 0; i < TaskItemIDs.Count(); i++)
            {
                MySqlCommand com = new MySqlCommand("INSERT INTO workresult(WorkResult_ID,Work_ID,TestItem_ID,OrderNumber," +
                    "Answer,Correct,AnswerTimeStamp) VALUES (@WorkResult_ID,@Work_ID,@TestItem_ID,@OrderNumber," +
                    "@Answer,@Correct,@AnswerTimeStamp)", con);
                com.Parameters.AddWithValue("@WorkResult_ID", WrkResID);
                com.Parameters.AddWithValue("@Work_ID", WorkID);
                com.Parameters.AddWithValue("@TestItem_ID", TaskItemIDs[i]);
                com.Parameters.AddWithValue("@OrderNumber", i + 1);
                com.Parameters.AddWithValue("@Answer","");
                com.Parameters.AddWithValue("@Correct",  0);
                com.Parameters.AddWithValue("@AnswerTimeStamp", DateTime.Now);
                com.ExecuteNonQuery();
                WrkResID++;
            }
            con.Close();
        }
        public static void AddWorkResultsFromWorkID(int WorkID, int OldWorkID)
        {
            int WrkResID = GetNextWorkResultID();
            int[] TaskItemIDs = GetWorkTestItemIDs(OldWorkID);
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comDel = new MySqlCommand("DELETE FROM workresult WHERE Work_ID=" + WorkID.ToString(), con);
            comDel.ExecuteNonQuery();
            for (int i = 0; i < TaskItemIDs.Count(); i++)
            {
                MySqlCommand com = new MySqlCommand("INSERT INTO workresult(WorkResult_ID,Work_ID,TestItem_ID,OrderNumber," +
                    "Answer,Correct,AnswerTimeStamp) VALUES (@WorkResult_ID,@Work_ID,@TestItem_ID,@OrderNumber," +
                    "@Answer,@Correct,@AnswerTimeStamp)", con);
                com.Parameters.AddWithValue("@WorkResult_ID", WrkResID);
                com.Parameters.AddWithValue("@Work_ID", WorkID);
                com.Parameters.AddWithValue("@TestItem_ID", TaskItemIDs[i]);
                com.Parameters.AddWithValue("@OrderNumber", i + 1);
                com.Parameters.AddWithValue("@Answer", "");
                com.Parameters.AddWithValue("@Correct", 0);
                com.Parameters.AddWithValue("@AnswerTimeStamp", DateTime.Now);
                com.ExecuteNonQuery();
                WrkResID++;
            }
            con.Close();
        }
        public static int[] GetWorkTestItemIDs(int WorkID)
        {
            List<int> res = new List<int>();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comm = new MySqlCommand("SELECT TestItem_ID FROM workresult WHERE Work_ID=" + WorkID.ToString(), con);
            MySqlDataReader rdr = comm.ExecuteReader();
            while (rdr.Read())
            {
                res.Add(rdr.GetInt32("TestItem_ID"));
            }
            con.Close();
            return res.ToArray();
        }

        private static int GetNextWorkResultID()
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com = new MySqlCommand("SELECT count(1) FROM workresult", con);
            int cnt;
            cnt = int.Parse(com.ExecuteScalar().ToString());
            if (cnt > 0)
            {
                com = new MySqlCommand("SELECT max(WorkResult_ID) FROM workresult", con);
                int wrkresID;
                wrkresID = ((int)com.ExecuteScalar()) + 1;
                con.Close();
                return wrkresID;
            }
            else
            {
                con.Close();
                return 1;
            }
        }
        public static void SaveWorkResults(WorkResultItem[] WorkResult,int WorkID)
        {
            int WorkResID = GetNextWorkResultID();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand comDel = new MySqlCommand("DELETE FROM workresult WHERE Work_ID=" + WorkID.ToString(), con);
            comDel.ExecuteNonQuery();
            foreach (WorkResultItem wri in WorkResult)
            {
                MySqlCommand com = new MySqlCommand("INSERT INTO workresult(WorkResult_ID,Work_ID,TestItem_ID,OrderNumber," +
                    "Answer,Correct,AnswerTimeStamp) VALUES (@WorkResult_ID,@Work_ID,@TestItem_ID,@OrderNumber," +
                    "@Answer,@Correct,@AnswerTimeStamp)", con);
                com.Parameters.AddWithValue("@WorkResult_ID", WorkResID);
                com.Parameters.AddWithValue("@Work_ID", wri.WorkID);
                com.Parameters.AddWithValue("@TestItem_ID", wri.TestItemID);
                com.Parameters.AddWithValue("@OrderNumber", wri.OrderNumber);
                com.Parameters.AddWithValue("@Answer", wri.Answer);
                com.Parameters.AddWithValue("@Correct", wri.Correct ? 1 : 0);
                com.Parameters.AddWithValue("@AnswerTimeStamp", wri.AnswerTimeStamp);
                com.ExecuteNonQuery();
                WorkResID++;
            }
            con.Close();
            return;
        }
        public static void SetWorkState(int WorkID,int WorkStateID)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com = new MySqlCommand("UPDATE work SET WorkState_ID=" + WorkStateID.ToString() +
                " WHERE Work_ID=" + WorkID.ToString(), con);
            com.ExecuteNonQuery();
            con.Close();
        }
        public static WorkResultItem GetWorkResultItem(int WorkID,int TestItemID)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com = new MySqlCommand("SELECT Answer,Correct,AnswerTimeStamp FROM workresult " +
                "WHERE Work_ID=" + WorkID.ToString() + " AND TestItem_ID=" + TestItemID.ToString(), con);
            MySqlDataReader rdr = com.ExecuteReader();
            if (rdr.Read())
            {
                WorkResultItem res = new WorkResultItem();
                res.Answer = rdr.GetString("Answer");
                res.Correct = (rdr.GetInt32("Correct") != 0);
                res.AnswerTimeStamp = rdr.GetDateTime("AnswerTimeStamp");
                con.Close();
                return res;
            }
            else
            {
                con.Close();
                return null;
            }
        }
        public static WorkResult[] GetWorkResults(int WorkID)
        {
            List<WorkResult> res = new List<WorkResult>();
            MySqlConnection con = new MySqlConnection(ConnectionString);
            con.Open();
            MySqlCommand com = new MySqlCommand("SELECT WorkResult_ID,Work_ID,TestItem_ID,OrderNumber,Answer,Correct,"+
                "AnswerTimeStamp FROM workresult WHERE Work_ID=" + WorkID.ToString(), con);
            MySqlDataReader rdr = com.ExecuteReader();
            while (rdr.Read())
            {
                WorkResult wr = new WorkResult();
                wr.WorkResultID = rdr.GetInt32("WorkResult_ID");
                wr.WorkID = rdr.GetInt32("Work_ID");
                wr.TestItemID = rdr.GetInt32("TestItem_ID");
                wr.OrderNumber = rdr.GetInt32("OrderNumber");
                wr.Answer = rdr.GetString("Answer");
                wr.Correct = (rdr.GetInt32("Correct") != 0);
                wr.AnswerTimeStamp = rdr.GetDateTime("AnswerTimeStamp");
                res.Add(wr);
            }
            con.Close();
            return res.ToArray();
        }
    }
}