﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="Tester.Users" EnableEventValidation="false" %>

<asp:Content ID="contTitle" ContentPlaceHolderID="contTitle" runat="server">
    Пользователи
</asp:Content>
<asp:Content ID="contScript" ContentPlaceHolderID="contScript" runat="server">
    <script type="text/javascript">
        function Confirm(el) {
            Message = $(el).attr("confirmmessage");
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm(Message)) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
        function ChangeUserMouseOver(el) {
            $(el).attr("src", "Buttons/EditMouseOver.png");
        }
        function ChangeUserMouseOut(el) {
            $(el).attr("src", "Buttons/Edit.png");
        }
        function ClearPasswordMouseOver(el) {
            $(el).attr("src", "Buttons/ClearPasswordMouseOver.png");
        }
        function ClearPasswordMouseOut(el) {
            $(el).attr("src", "Buttons/ClearPassword.png");
        }
        function DeleteUserMouseOver(el) {
            $(el).attr("src", "Buttons/DeleteMouseOver.png");
        }
        function DeleteUserMouseOut(el) {
            $(el).attr("src", "Buttons/Delete.png");
        }
    </script>
</asp:Content>
<asp:Content ID="contMenu" ContentPlaceHolderID="contMenu" runat="server">
    <table>
        <tr>
            <td>
                <asp:ImageButton ID="btnMyWork" runat="server" OnClick="btnMyWork_Click" ImageUrl="~/Buttons/MyWork.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnTests" runat="server" OnClick="btnTests_Click" ImageUrl="~/Buttons/Tests.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnTasks" runat="server" OnClick="btnTasks_Click" ImageUrl="~/Buttons/Tasks.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnWorkResults" runat="server" OnClick="btnWorkResults_Click" ImageUrl="~/Buttons/WorkResults.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnUsers" runat="server" Enabled="false" ImageUrl="~/Buttons/UsersActive.png" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="contMain" ContentPlaceHolderID="contMain" runat="server">
    <table border="1">
        <tr>
            <th>
                <asp:LinkButton ID="btnLoginSort" runat="server" Text="Логин"
                    ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                    OnClick="btnLoginSort_Click"></asp:LinkButton>
            </th>
            <th>
                <asp:LinkButton ID="btnFirstNameSort" runat="server" Text="Имя"
                    ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                    OnClick="btnFirstNameSort_Click"></asp:LinkButton>
            </th>
            <th>
                <asp:LinkButton ID="btnSecondNameSort" runat="server" Text="Фамилия"
                    ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                    OnClick="btnSecondNameSort_Click"></asp:LinkButton>
            </th>
            <th>
                <asp:LinkButton ID="btnIsAdminSort" runat="server" Text="Администратор?"
                    ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                    OnClick="btnIsAdminSort_Click"></asp:LinkButton>
            </th>
            <th>
                <asp:LinkButton ID="btnEMailSort" runat="server" Text="E-Mail"
                    ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                    OnClick="btnEMailSort_Click"></asp:LinkButton>
            </th>
            <th>&nbsp;
            </th>
            <th>&nbsp;
            </th>
            <th>&nbsp;
            </th>
        </tr>
        <asp:Repeater ID="rptUserList" runat="server">
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:Label ID="lblLogin" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Login")  %>'
                            ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblFirstName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FirstName")  %>'
                            ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblSecondName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SecondName")  %>'
                            ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    </td>
                    <td style="text-align: center">
                        <asp:Image ID="imgIsAdmin" runat="server"
                            ImageUrl='<%# ((bool)DataBinder.Eval(Container.DataItem, "isAdmin"))?"~/CheckBoxImage/Checked.png":"~/CheckBoxImage/UnChecked.png"  %>' />
                    </td>
                    <td>
                        <asp:Label ID="lblEMail" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EMail")  %>'
                            ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"></asp:Label>
                    </td>
                    <td>
                        <asp:ImageButton ID="btnChangeUser" runat="server"
                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserID")  %>'
                            OnClick="btnChangeUser_Click"
                            ImageUrl="~/Buttons/Edit.png"
                            onmouseover="ChangeUserMouseOver(this)"
                            onmouseout="ChangeUserMouseOut(this)" />
                    </td>
                    <td>
                        <asp:ImageButton ID="btnClearPassword" runat="server"
                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserID")  %>'
                            OnClick="btnClearPassword_Click"
                            ImageUrl="~/Buttons/ClearPassword.png"
                            confirmmessage='<%# "Сбросить пароль для "+DataBinder.Eval(Container.DataItem, "FirstName")+" "+
                                    DataBinder.Eval(Container.DataItem, "SecondName") %>'
                            OnClientClick="Confirm(this);"
                            onmouseover="ClearPasswordMouseOver(this);"
                            onmouseout="ClearPasswordMouseOut(this);" />
                    </td>
                    <td>
                        <asp:ImageButton ID="btnDeleteUser" runat="server"
                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserID")  %>'
                            OnClick="btnDeleteUser_Click"
                            ImageUrl="~/Buttons/Delete.png"
                            confirmmessage='<%# "Удалить пользователя "+DataBinder.Eval(Container.DataItem, "FirstName")+" "+
                                    DataBinder.Eval(Container.DataItem, "SecondName") %>'
                            OnClientClick="Confirm(this);"
                            onmouseover="DeleteUserMouseOver(this);"
                            onmouseout="DeleteUserMouseOut(this);" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <asp:Panel ID="pnUser" runat="server">
        <table>
            <tr>
                <th>
                    <asp:Label ID="Label5" runat="server" Text="Логин"
                        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"></asp:Label>
                </th>
                <th>
                    <asp:Label ID="Label6" runat="server" Text="Имя"
                        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"></asp:Label>
                </th>
                <th>
                    <asp:Label ID="Label7" runat="server" Text="Фамилия"
                        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"></asp:Label>
                </th>
                <th>
                    <asp:Label ID="Label8" runat="server" Text="Администратор?"
                        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"></asp:Label>
                </th>
                <th>
                    <asp:Label ID="Label10" runat="server" Text="E-Mail"
                        ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"></asp:Label>
                </th>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtLogin" runat="server"
                        BackColor="#58FAF4" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"
                        BorderColor="#58FAF4"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtFirstName" runat="server"
                        BackColor="#58FAF4" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"
                        BorderColor="#58FAF4"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtSecondName" runat="server"
                        BackColor="#58FAF4" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"
                        BorderColor="#58FAF4"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkIsAdmin" runat="server" Text="&nbsp;" ForeColor="#0B0B3B"
                        Font-Names="Arial" Font-Size="18px" />
                </td>
                <td>
                    <asp:TextBox ID="txtEMail" runat="server"
                        BackColor="#58FAF4" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"
                        BorderColor="#58FAF4"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="contButtons" ContentPlaceHolderID="contButtons" runat="server">
    <hr />
    <asp:ImageButton ID="btnAddUser" runat="server" OnClick="btnAddUser_Click" ImageUrl="~/Buttons/AddUser.png" />
    <asp:ImageButton ID="btnOK" runat="server" OnClick="btnOK_Click" ImageUrl="~/Buttons/OK.png" />
    <asp:ImageButton ID="btnCancel" runat="server" OnClick="btnCancel_Click" ImageUrl="~/Buttons/Cancel.png" />
</asp:Content>

