﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="WorkResults.aspx.cs" Inherits="Tester.WorkResults" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="contTitle" ContentPlaceHolderID="contTitle" runat="server">
    Результаты тестирования
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="contScript" runat="server">
    <script type="text/javascript">
        function Confirm(el) {
            Message = $(el).attr("confirmmessage");
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm(Message)) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
        function DeleteUserMouseOver(el) {
            $(el).attr("src", "Buttons/DeleteMouseOver.png");
        }
        function DeleteUserMouseOut(el) {
            $(el).attr("src", "Buttons/Delete.png");
        }
    </script>
</asp:Content>
<asp:Content ID="contMenu" ContentPlaceHolderID="contMenu" runat="server">
    <table>
        <tr>
            <td>
                <asp:ImageButton ID="btnMyWork" runat="server" OnClick="btnMyWork_Click" ImageUrl="~/Buttons/MyWork.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnTests" runat="server" OnClick="btnTests_Click" ImageUrl="~/Buttons/Tests.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnTasks" runat="server" OnClick="btnTasks_Click" ImageUrl="~/Buttons/Tasks.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnWorkResults" runat="server" Enabled="false" ImageUrl="~/Buttons/WorkResultsActive.png" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnUsers" runat="server" OnClick="btnUsers_Click" ImageUrl="~/Buttons/Users.png" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="contMain" ContentPlaceHolderID="contMain" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"></asp:ScriptManager>
    <asp:Label ID="Label1" runat="server" Text="Назначено пользователю:" Width="240px" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:DropDownList ID="ddlUserDoer" runat="server" Width="240px" BackColor="#58FAF4" ForeColor="#0B0B3B" 
        Font-Names="Arial" Font-Size="18px" BorderColor="#58FAF4"></asp:DropDownList>
    <br />
    <asp:Label ID="Label2" runat="server" Text="Назначено пользователем:" Width="240px" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:DropDownList ID="ddlUserAssigner" runat="server" Width="240px" BackColor="#58FAF4" ForeColor="#0B0B3B" 
        Font-Names="Arial" Font-Size="18px" BorderColor="#58FAF4"></asp:DropDownList>
    <br />
    <asp:Label ID="Label4" runat="server" Text="Состояние" Width="240px" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:DropDownList ID="ddlState" runat="server" Width="240px" BackColor="#58FAF4" ForeColor="#0B0B3B" 
        Font-Names="Arial" Font-Size="18px" BorderColor="#58FAF4"></asp:DropDownList>
    <br />
    <asp:Label ID="LabelC" runat="server" Text="с" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:TextBox ID="txtFromDate" runat="server" ReadOnly="false" Width="100px" BackColor="#58FAF4" ForeColor="#0B0B3B"
        Font-Names="Arial" Font-Size="18px" BorderColor="#58FAF4"></asp:TextBox>
    <cc1:CalendarExtender ID="ce_txtFromDate" runat="server" TargetControlID="txtFromDate" Format="dd.MM.yyyy" CssClass="cal" />
    <asp:Label ID="Label3" runat="server" Text="по" ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="18px"></asp:Label>
    <asp:TextBox ID="txtToDate" runat="server" ReadOnly="false" Width="100px" BackColor="#58FAF4" ForeColor="#0B0B3B"
        Font-Names="Arial" Font-Size="18px" BorderColor="#58FAF4"></asp:TextBox>
    <cc1:CalendarExtender ID="ce_txtToDate" runat="server" TargetControlID="txtToDate" Format="dd.MM.yyyy" CssClass="cal" />
    <br />
    <asp:ImageButton ID="btnApplyFilter" runat="server" OnClick="btnApplyFilter_Click" ImageUrl="~/Buttons/ApplyFilter.png" />
    <asp:Repeater ID="rptWorkShowResult" runat="server">
        <HeaderTemplate>
            <table border="1">
                <tr>
                    <th>
                        <asp:LinkButton ID="btnTaskNameSort" runat="server" Text="Имя задания"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnTaskNameSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnUserAssignerSort" runat="server" Text="Назначено пользователем"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnUserAssignerSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnUserSort" runat="server" Text="Назначено пользователю"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnUserSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnAssignDateSort" runat="server" Text="Дата назначения"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnAssignDateSort_Click"></asp:LinkButton>
                    </th>
                    <th>
                        <asp:LinkButton ID="btnStateSort" runat="server" Text="Состояние"
                            ForeColor="#0B0B3B" Font-Bold="True" Font-Names="Arial" Font-Size="14px"
                            OnClick="btnStateSort_Click"></asp:LinkButton>
                    </th>
                    <th style="width: 110px" runat="server" id="thDelete">&nbsp;
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:LinkButton ID="btnName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TaskName")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"
                        CommandArgument="<%# Container.ItemIndex %>" OnClick="btnShowResult_Click"></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="btnAssigner" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UserAssignerName")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"
                        CommandArgument="<%# Container.ItemIndex %>" OnClick="btnShowResult_Click"></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="btnCreator" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UserName")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"
                        CommandArgument="<%# Container.ItemIndex %>" OnClick="btnShowResult_Click"></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="btnCreateDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "WorkToShow.AssignDate")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"
                        CommandArgument="<%# Container.ItemIndex %>" OnClick="btnShowResult_Click"></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="btnLastModifyDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "WorkToShow.WorkState")  %>'
                        ForeColor="#0B0B3B" Font-Names="Arial" Font-Size="14px"
                        CommandArgument="<%# Container.ItemIndex %>" OnClick="btnShowResult_Click"></asp:LinkButton>
                </td>
                <td runat="server" id="tdDelete">
                    <asp:ImageButton ID="btnDelete" runat="server"
                        CommandArgument="<%# Container.ItemIndex %>"
                        OnClick="btnDelete_Click"
                        ImageUrl="~/Buttons/Delete.png"
                        confirmmessage='<%# "Удалить задание "+DataBinder.Eval(Container.DataItem, "TaskName")+"?"  %>'
                        OnClientClick="Confirm(this);"
                        onmouseover="DeleteUserMouseOver(this);"
                        onmouseout="DeleteUserMouseOut(this);" />
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="contButtons" ContentPlaceHolderID="contButtons" runat="server">
</asp:Content>

