﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Tester
{
    public partial class MyWork : System.Web.UI.Page
    {
        Work[] myWorks;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SessionToViewState();
            }
            if (ViewState["IsAdmin"]!=null)
            {
                if ((bool)ViewState["IsAdmin"])
                {
                    btnTests.Visible = true;
                    btnTasks.Visible = true;
                    btnUsers.Visible = true;
                }
                else
                {
                    btnTests.Visible = false;
                    btnTasks.Visible = false;
                    btnUsers.Visible = false;
                }
            }
            if (!IsPostBack)
            {
                ShowFilter();
            }
            if (ViewState["CurUser"]!=null)
            {
                UserDB usr = (UserDB)ViewState["CurUser"];
                
                string filter = "User_ID=" + usr.UserID.ToString();
                filter += " AND AssignDate BETWEEN '" + FromDate.Year + "-" + FromDate.Month + "-" + FromDate.Day +
                    " 00:00:00' AND '" + ToDate.Year + "-" + ToDate.Month + "-" + ToDate.Day + " 23:59:59'";
                if (OnlyNoComplete)
                {
                    filter += " AND WorkState_ID IN (1,2)";
                }
                myWorks = AccessDB.GetWorks(filter);
                OrderMyWorks(myWorks);
                rptMyWork.DataSource = myWorks;
                rptMyWork.DataBind();
                SetButtons();
            }
        }
        private void SetButtons()
        {
            btnTests.Attributes.Add("onmouseout", "this.src='Buttons/Tests.png'");
            btnTests.Attributes.Add("onmouseover", "this.src='Buttons/TestsMouseOver.png'");
            btnTasks.Attributes.Add("onmouseout", "this.src='Buttons/Tasks.png'");
            btnTasks.Attributes.Add("onmouseover", "this.src='Buttons/TasksMouseOver.png'");
            btnWorkResults.Attributes.Add("onmouseout", "this.src='Buttons/WorkResults.png'");
            btnWorkResults.Attributes.Add("onmouseover", "this.src='Buttons/WorkResultsMouseOver.png'");
            btnUsers.Attributes.Add("onmouseout", "this.src='Buttons/Users.png'");
            btnUsers.Attributes.Add("onmouseover", "this.src='Buttons/UsersMouseOver.png'");
            btnApplyFilter.Attributes.Add("onmouseout", "this.src='Buttons/ApplyFilter.png'");
            btnApplyFilter.Attributes.Add("onmouseover", "this.src='Buttons/ApplyFilterMouseOver.png'");
        }
        private int CompareWork(Work w1,Work w2)
        {
            if (MyWorkAssignDateSort == "ASC")
            {
                if (w1.AssignDate > w2.AssignDate)
                {
                    return 1;
                }
                else if (w1.AssignDate < w2.AssignDate)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
            if (MyWorkAssignDateSort == "DESC")
            {
                if (w1.AssignDate > w2.AssignDate)
                {
                    return -1;
                }
                else if (w1.AssignDate < w2.AssignDate)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            if (MyWorkTaskNameSort == "ASC")
            {
                return w1.TaskName.CompareTo(w2.TaskName);
            }
            if (MyWorkTaskNameSort == "DESC")
            {
                return w2.TaskName.CompareTo(w1.TaskName);
            }
            if (MyWorkStateSort == "ASC")
            {
                if (w1.WorkStateID > w2.WorkStateID)
                {
                    return 1;
                }
                else if (w1.WorkStateID < w2.WorkStateID)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
            if (MyWorkStateSort == "DESC")
            {
                if (w1.WorkStateID > w2.WorkStateID)
                {
                    return -1;
                }
                else if (w1.WorkStateID < w2.WorkStateID)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            return 0;
        }
        private void OrderMyWorks(Work[] Works)
        {
            if (MyWorkAssignDateSort!="" || MyWorkTaskNameSort!="" || MyWorkStateSort!="")
            {
                Array.Sort(Works, CompareWork);
            }
        }
        private string NextSortDir(string SortDir)
        {
            switch (SortDir)
            {
                case "":
                    return "ASC";
                case "ASC":
                    return "DESC";
                case "DESC":
                    return "";
                default:
                    return "";
            }
        }
        private DateTime FromDate
        {
            get
            {
                if (ViewState["MyWorkFromDate"] != null)
                {
                    return (DateTime)ViewState["MyWorkFromDate"];
                }
                else
                {
                    return DateTime.Now.AddMonths(-3);
                }
            }
            set
            {
                ViewState["MyWorkFromDate"] = value;
            }
        }
        private DateTime ToDate
        {
            get
            {
                if (ViewState["MyWorkToDate"] != null)
                {
                    return (DateTime)ViewState["MyWorkToDate"];
                }
                else
                {
                    return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                }
            }
            set
            {
                ViewState["MyWorkToDate"] = value;
            }
        }
        private bool OnlyNoComplete
        {
            get
            {
                if (ViewState["MyWorkOnlyNoComplete"] != null)
                {
                    return (bool)ViewState["MyWorkOnlyNoComplete"];
                }
                else
                {
                    return true;
                }
            }
            set 
            {
                ViewState["MyWorkOnlyNoComplete"] = value;
            }
        }
        private string MyWorkAssignDateSort
        {
            get
            {
                if (ViewState["MyWorkAssignDateSort"]!=null)
                {
                    return (string)ViewState["MyWorkAssignDateSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["MyWorkAssignDateSort"] = value;
            }
        }
        private string MyWorkTaskNameSort
        {
            get
            {
                if (ViewState["MyWorkTaskNameSort"] != null)
                {
                    return (string)ViewState["MyWorkTaskNameSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["MyWorkTaskNameSort"] = value;
            }
        }
        private string MyWorkStateSort
        {
            get
            {
                if (ViewState["MyWorkStateSort"] != null)
                {
                    return (string)ViewState["MyWorkStateSort"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["MyWorkStateSort"] = value;
            }
        }
        private void ShowFilter()
        {
            chkNoComplete.Checked = OnlyNoComplete;
            txtFromDate.Text = FromDate.ToString("dd.MM.yyyy");
            txtToDate.Text = ToDate.ToString("dd.MM.yyyy");
        }
        protected void btnAssignDateSort_Click(object sender, EventArgs e)
        {
            if (MyWorkTaskNameSort != "" || MyWorkStateSort != "")
            {
                MyWorkStateSort = "";
                MyWorkTaskNameSort = "";
                MyWorkAssignDateSort = "ASC";
            }
            else
            {
                MyWorkAssignDateSort = NextSortDir(MyWorkAssignDateSort);
            }
            ViewStateToSession();
            Response.Redirect("~/MyWork.aspx");
        }
        protected void btnTaskNameSort_Click(object sender, EventArgs e)
        {
            if (MyWorkAssignDateSort != "" || MyWorkStateSort != "")
            {
                MyWorkStateSort = "";
                MyWorkTaskNameSort = "ASC";
                MyWorkAssignDateSort = "";
            }
            else
            {
                MyWorkTaskNameSort = NextSortDir(MyWorkTaskNameSort);
            }
            ViewStateToSession();
            Response.Redirect("~/MyWork.aspx");
        }
        protected void btnStateSort_Click(object sender, EventArgs e)
        {
            if (MyWorkAssignDateSort != "" || MyWorkTaskNameSort != "")
            {
                MyWorkStateSort = "ASC";
                MyWorkTaskNameSort = "";
                MyWorkAssignDateSort = "";
            }
            else
            {
                MyWorkStateSort = NextSortDir(MyWorkStateSort);
            }
            ViewStateToSession();
            Response.Redirect("~/MyWork.aspx");
        }
        protected void btnApplyFilter_Click(object sender, EventArgs e)
        {
            OnlyNoComplete = chkNoComplete.Checked;
            string[] sFrom = txtFromDate.Text.Split(new char[] { '.' });
            string[] sTo = txtToDate.Text.Split(new char[] { '.' });
            FromDate = new DateTime(int.Parse(sFrom[2]), int.Parse(sFrom[1]), int.Parse(sFrom[0]));
            ToDate = new DateTime(int.Parse(sTo[2]), int.Parse(sTo[1]), int.Parse(sTo[0]));
            ViewStateToSession();
            Response.Redirect("~/MyWork.aspx");
        }
        protected void btnTests_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/Tests.aspx");
        }
        protected void btnTasks_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/Tasks.aspx");
        }
        protected void btnWorkResults_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/WorkResults.aspx");
        }
        protected void btnUsers_Click(object sender, EventArgs e)
        {
            ViewStateToSession();
            Response.Redirect("~/Users.aspx");
        }
        protected void btnStart_Click(object sender, EventArgs e)
        {
            int WorkID = int.Parse(((ImageButton)sender).CommandArgument);
            Work wrk = AccessDB.GetWork(WorkID);
            if (wrk.WorkStateID == 3 && wrk.Repeatable)
            {
                int OldWorkID = WorkID;
                wrk.WorkID = 0;
                wrk.WorkStateID = 1;
                wrk.AssignDate = DateTime.Now;
                WorkID = AccessDB.AddWork(wrk);
                AccessDB.AddWorkResultsFromWorkID(WorkID, OldWorkID);
                wrk = AccessDB.GetWork(WorkID);
            }
            if (wrk.WorkStateID == 1)
            {
                DateTime dtNow = DateTime.Now;
                dtNow = new DateTime(dtNow.Ticks - dtNow.Ticks % TimeSpan.TicksPerSecond);
                wrk.BeginDateTime = dtNow;
            }
            ViewState["CurrentWork"] = wrk;
            int[] WorkItems = AccessDB.GetWorkTestItemIDs(WorkID);
            WorkResultItem[] WorkResult = new WorkResultItem[WorkItems.Count()];
            for (int i = 0; i < WorkResult.Count(); i++)
            {
                WorkResult[i] = new WorkResultItem();
                WorkResult[i].OrderNumber = i + 1;
                WorkResult[i].WorkID = WorkID;
                WorkResult[i].TaskID = wrk.TaskID;
                WorkResult[i].TestItemID = WorkItems[i];
                WorkResult[i].QuestionHtml = AccessDB.GetTestItem(WorkResult[i].TestItemID).QuestionHtml;
                WorkResult[i].Answer = "";
                WorkResult[i].ShowCorrectAnswer = wrk.ShowAnswer;
                WorkResult[i].CorrectAnswer = AccessDB.GetTestItem(WorkResult[i].TestItemID).Answer;
                WorkResult[i].Correct = false;
                WorkResult[i].AnswerTimeStamp = DateTime.Now;
                if (wrk.WorkStateID != 1)
                {
                    WorkResultItem wri = AccessDB.GetWorkResultItem(WorkID, WorkItems[i]);
                    if (wri!=null)
                    {
                        WorkResult[i].Answer = wri.Answer;
                        WorkResult[i].Correct = wri.Correct;
                        WorkResult[i].AnswerTimeStamp = wri.AnswerTimeStamp;
                    }
                }
            }
            ViewState["WorkResult"] = WorkResult;
            ViewStateToSession();
            Response.Redirect("~/DoWork.aspx");
        }
        private void SessionToViewState()
        {
            for (int i = 0; i < Session.Count; i++)
            {
                ViewState[Session.Keys[i].ToString()] = Session[Session.Keys[i].ToString()];
            }
        }
        private void ViewStateToSession()
        {
            string[] Keys = new string[ViewState.Count];
            ViewState.Keys.CopyTo(Keys, 0);
            for (int i = 0; i < Keys.Count(); i++)
            {
                Session[Keys[i]] = ViewState[Keys[i]];
            }
        }
    }
}