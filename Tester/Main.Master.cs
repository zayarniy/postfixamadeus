﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Tester
{
    public partial class Main : System.Web.UI.MasterPage
    {
        UserDB CurrentUser;
        bool ChangePasswordFlag;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SessionToViewState();
            }
            string Greeting = "";
            if (ViewState["CurUser"] != null)
            {
                CurrentUser = (UserDB)ViewState["CurUser"];
                Greeting = "Здравствуйте, " + CurrentUser.ToString() + "!";
            }
            lblGreeting.Text = Greeting;
            if (ViewState["ChangePassword"]!=null)
            {
                ChangePasswordFlag = (bool)ViewState["ChangePassword"];
            }
            else
            {
                ChangePasswordFlag = false;
            }
            pnChangePassword.Visible = ChangePasswordFlag;

            SetButtons();
        }
        private void SetButtons()
        {
            btnExit.Attributes.Add("onmouseover", "this.src='Buttons/ExitMouseOver.png'");
            btnExit.Attributes.Add("onmouseout", "this.src='Buttons/Exit.png'");
            btnChange.Attributes.Add("onmouseover", "this.src='Buttons/ChangeMouseOver.png'");
            btnChange.Attributes.Add("onmouseout", "this.src='Buttons/Change.png'");
            btnCancel.Attributes.Add("onmouseover", "this.src='Buttons/CancelMouseOver.png'");
            btnCancel.Attributes.Add("onmouseout", "this.src='Buttons/Cancel.png'");
        }
        protected void btnExit_Click(object sender, EventArgs e)
        {
            Session.Clear();
            ViewState.Clear();
            Response.Redirect("~/Login.aspx");
        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            ViewState["ChangePassword"] = true;
            ViewStateToSession();
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), true);
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ViewState["ChangePassword"] = null;
            ViewStateToSession();
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), true);
        }
        protected void btnChange_Click(object sender, EventArgs e)
        {
            if (CurrentUser.Password != txtOldPassword.Text)
            {
                lblMessage.Text = "Неверный пароль!";
                return;
            }
            if (txtNewPassword.Text != txtNewPassword2.Text)
            {
                lblMessage.Text = "Несовпадают новый пароль и подтверждение";
                return;
            }
            AccessDB.ChangePassword(CurrentUser.UserID, txtNewPassword.Text);
            ViewState["ChangePassword"] = null;
            ViewStateToSession();
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), true);
        }
        private void SessionToViewState()
        {
            for (int i = 0; i < Session.Count; i++)
            {
                ViewState[Session.Keys[i].ToString()] = Session[Session.Keys[i].ToString()];
            }
        }
        private void ViewStateToSession()
        {
            string[] Keys = new string[ViewState.Count];
            ViewState.Keys.CopyTo(Keys, 0);
            for (int i = 0; i < Keys.Count(); i++)
            {
                Session[Keys[i]] = ViewState[Keys[i]];
            }
        }
    }
}